package plugins.ragini.voronoidiagram;

public interface Provider<T> {

	public T next();
	
}
