package plugins.ragini.voronoidiagram;

public class HalfEdge {

	private HalfEdge ELleft, ELRight;
	private Edge ELedge;
	private int ELrefcnt;
	private int ELpm;
	private Site vertex;
	private float ystar;
	private HalfEdge PQNext; 
	private boolean isnull;
	private boolean isdeleted; 
	private int origin; 
	
	public HalfEdge(int i) {
		isnull = true;
		isdeleted = false; 
		ELpm = -1; 
		origin = i; 
	}
	
	public Site vertex() {
		return vertex; 
	}
	
	public Edge eledge() {
		return ELedge;
	}
	
	public int elpm() {
		return ELpm;  
	}
	
	public void setLeft(HalfEdge l) {
		ELleft = l;
	}
	
	public void setRight(HalfEdge r) {
		ELRight = r; 
	}
	
	public void setEdge(Edge e) {
		ELedge = e;                                      
	}
	
	public void setPM(int p) {
		ELpm = p; 
	}
	
	public void setNext(HalfEdge n) {
		PQNext = n; 
	}
	
	public void setVertex(Site s) {
		vertex = s; 
	}
	
	public HalfEdge left() {
		return ELleft;
	}
	
	public HalfEdge right() {
		return ELRight; 
	}
	
	public void setystar(float d) {
		ystar = d; 
	}
	
	public HalfEdge PQnext() {
		return PQNext; 
	}
	
	public float ystar() {
		return ystar; 
	}
	
	public void setPQnext(HalfEdge h) {
		PQNext = h; 
	}
	
	public boolean isNull() {
		return isnull;
	}
	
	public boolean isDeleted() {
		return isdeleted;
	}
	
	public void setNull(boolean b) {
		isnull = b; 
	}
	
	public void setDeleted(boolean b) {
		isdeleted = b; 
	}
	
	public int origin() {
		return origin; 
	}
	
}
