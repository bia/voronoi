package plugins.ragini.voronoidiagram;
 
//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   Face.java

import java.util.ArrayList;
import java.util.Iterator;

//Referenced classes of package delaunay:
//         QuadEdge, DT, Vertex

public class Face
{

 public Face()
 {
     id_f = id;
     id++;
     edges = new QuadEdge[3];
     S = DT.vertex_infty;
     B = DT.vertex_infty;
     setVal(0.0F);
     setInvRadius(0.0D);
     setInvSize(0.0D);
     setMeasure(0.0D);
     counter = 0;
 }

 public Face(QuadEdge a, QuadEdge b, QuadEdge c)
 {
     this();
     setEdges(a, b, c);
     setCenter();
     setBary();
     setInvSize(Math.max(Math.max(Vertex.distance(B, a.orig()), Vertex.distance(B, b.orig())), Vertex.distance(B, c.orig())));
     setInvRadius(Vertex.distance(S, a.orig()));
     if(DT.type == 1)
         setMeasure(getInvRadius());
     else
         setMeasure(getInvSize());
 }

 public Iterator extractFacesVoisines()
 {
     ArrayList f_liste = new ArrayList();
     for(int i = 0; i < 3; i++)
     {
         QuadEdge e = edges[i];
         QuadEdge ee = e;
         int count = 0;
         do
         {
             count++;
             ee = ee.onext();
             Face fff = ee.rightFace();
             if(fff == null)
                 fff = DT.face_null;
             if(!f_liste.contains(fff))
                 f_liste.add(fff);
         } while(ee != e && count < 30);
     }

     return (Iterator)f_liste.listIterator();
 }

 public Iterator extractFacesVoisines2()
 {
     ArrayList f_liste = new ArrayList();
     for(int i = 0; i < 3; i++)
     {
         QuadEdge e = edges[i];
         Face fff = e.sym().rightFace();
         if(fff == null)
             fff = DT.face_null;
         if(!f_liste.contains(fff))
             f_liste.add(fff);
     }

     return (Iterator)f_liste.listIterator();
 }

 public Iterator extractFacesVoisines(double angle, double epsilon)
 {
     if(angle < 0.0D)
         return extractFacesVoisines();
     ArrayList f_liste = new ArrayList();
     for(int i = 0; i < 3; i++)
     {
         QuadEdge e = edges[i];
         QuadEdge ee = e;
         int count = 0;
         do
         {
             count++;
             Face fff = ee.rightFace();
             if(fff == null)
                 fff = DT.face_null;
             if(!f_liste.contains(fff))
                 if(fff != DT.face_null)
                 {
                     if(ecartAngle(fff) < epsilon)
                         f_liste.add(fff);
                 } else
                 {
                     f_liste.add(fff);
                 }
             ee = ee.onext();
         } while(ee != e && count < 30);
     }

     return (Iterator)f_liste.listIterator();
 }

 public Iterator extractFacesVoisines2(double angle, double epsilon)
 {
     if(angle < 0.0D)
         return extractFacesVoisines2();
     ArrayList f_liste = new ArrayList();
     for(int i = 0; i < 3; i++)
     {
         QuadEdge e = edges[i];
         Face fff = e.sym().rightFace();
         if(fff == null)
             fff = DT.face_null;
         if(!f_liste.contains(fff))
             if(fff != DT.face_null)
             {
                 if(ecartAngle(fff) < epsilon)
                     f_liste.add(fff);
             } else
             {
                 f_liste.add(fff);
             }
     }

     return (Iterator)f_liste.listIterator();
 }

 public void setCenter()
 {
     S = Vertex.findCenter(edges[0].orig(), edges[1].orig(), edges[2].orig());
 }

 public void setBary()
 {
     B = Vertex.findBary(edges[0].orig(), edges[1].orig(), edges[2].orig());
 }

 public Vertex getCenter()
 {
     return S;
 }

 public Vertex getBary()
 {
     return B;
 }

 public float getVal()
 {
     return val;
 }

 public float getValAux()
 {
     return val_aux;
 }

 public void setValAux(float val)
 {
     val_aux = val;
 }

 public void setVal(float val)
 {
     this.val = val;
 }

 public QuadEdge[] getEdges()
 {
     return edges;
 }

 public void setEdges(QuadEdge a, QuadEdge b, QuadEdge c)
 {
     edges[0] = a;
     edges[1] = b;
     edges[2] = c;
 }

 public void setInvRadius(double radius)
 {
     if(radius == 0.0D)
         invRadius = DT.MAX_VALUE;
     else
         invRadius = 1.0D / radius;
 }

 public void setInvSize(double size)
 {
     if(size == 0.0D)
         invSize = DT.MAX_VALUE;
     invSize = 1.0D / size;
 }

 public void setMeasure(double measure)
 {
     this.measure = measure;
 }

 public double getInvRadius()
 {
     return invRadius;
 }

 public double getInvSize()
 {
     return invSize;
 }

 public double getMeasure()
 {
     return measure;
 }

 public int contains(Vertex p)
 {
     if(invSize == 0.0D)
         return 0;
     double size = 1.0D / (double)(float)invSize;
     return Vertex.distance(p, B) >= size / 3D ? 0 : 1;
 }

 public void setCounter(int counter)
 {
     this.counter = counter;
 }

 public int getCounter()
 {
     return counter;
 }

 public double ecartAngle(Face f)
 {
     double angle2 = 0.0D;
     Vertex angle = new Vertex(0.0D, 0.0D);
     if(DT.vertexAngle.x == -1D && DT.vertexAngle.y == -1D)
         return 0.0D;
     angle.x = getCenter().x - f.getCenter().x;
     angle.y = getCenter().y - f.getCenter().y;
     double norme = Vertex.distance(getCenter(), f.getCenter());
     if(norme == 0.0D)
     {
         return 0.0D;
     } else
     {
         double cosangle = angle.x * DT.vertexAngle.x + angle.y * DT.vertexAngle.y;
         cosangle /= norme;
         angle2 = Math.acos(cosangle);
         return angle2;
     }
 }

 public String toString()
 {
     if(edges[0] != null)
         return (new StringBuilder()).append(edges[0].orig()).append(" ").append(edges[1].orig()).append(" ").append(edges[2].orig()).toString();
     else
         return null;
 }

 private QuadEdge edges[];
 private int counter;
 private double invRadius;
 private double invSize;
 private double measure;
 private ArrayList e;
 private ArrayList d;
 private Vertex S;
 private Vertex B;
 float val;
 float val_aux;
 float val_aux_angle;
 public int id_f;
 static int id = 0;

}

