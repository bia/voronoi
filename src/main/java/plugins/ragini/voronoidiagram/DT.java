package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   DT.java

//Referenced classes of package delaunay:
//         Vertex, Face

public class DT
{

 public DT()
 {
 }

 public static int nextCoord(int i)
 {
     return i != 0 ? 0 : 1;
 }

 public static final int XAXIS = 0;
 public static final int YAXIS = 1;
 public static final int ZAXIS = 2;
 public static final int ALG_INCODE = 0;
 public static final int ALG_DEWALL = 1;
 public static final int ALG_GUIBAS = 2;
 public static int counter1;
 public static int counter2;
 public static int counter3;
 public static float opacity = 1.0F;
 public static int type = 1;
 public static int step = 40;
 public static double res = 1.0D;
 public static double jitter = 0.01D;
 public static double MAX_VALUE;
 public static double densityParameter = 0.5D;
 public static Vertex vertexAngle = new Vertex(-1D, -1D);
 public static int frame;
 public static Face face_null = new Face();
 public static Vertex vertex_null = new Vertex(0.0D, 0.0D);
 public static Vertex vertex_infty;

 static 
 {
     MAX_VALUE = 100000D;
     vertex_infty = new Vertex(MAX_VALUE, MAX_VALUE);
 }
}
