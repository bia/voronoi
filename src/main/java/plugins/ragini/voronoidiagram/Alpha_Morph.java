package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   Alpha_Morph.java


import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GUI;
import ij.plugin.frame.PlugInFrame;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.PrintStream;
import java.util.Vector;
import javax.swing.*;

//Referenced classes of package delaunay:
//         DelaunayTriangulator, Display, DT, Vertex

public class Alpha_Morph extends PlugInFrame
 implements Runnable
{

 public Alpha_Morph(Neighbor[] data, double _minx, double _miny, double _maxx, double _maxy)
 {
     super("Alpha_Morph");
     minx = _minx;
     miny = _miny;
     maxx = _maxx; 
     maxy = _maxy; 
     load = data; 
     running = false;
     waiting = true;
     counter = 0;
     canAddPoints = true;
     fileImage = null;
     rnd = 30;
     rank = 0;
     rank2 = 0;
     toggle = 0;
     toggle1 = 1;
     toggle2 = 2;
     toggle3 = 3;
     toggleBin = 0;
     angle = -1D;
     epsilon = 0.39269908169872414D;
     myThread = null;
     sites = null;
     sitesBin = null;
     dt = new DelaunayTriangulator[4];
     disp = new Display[4];
     for(int i = 0; i < 4; i++)
     {
         dt[i] = new DelaunayTriangulator();
         disp[i] = new Display(dt[i]);
     }

     slider = new JSlider(0, 360, 0);
     slider_opacity = new JSlider(0, 100, 100);
     b1 = new Button("  Process All  ");
     b2 = new Button("  Seeds ");
     b3 = new Button("   Dual   ");
     bComp = new Button("   Comp  ");
     b6 = new Button("   Binarize  ");
     bE = new Button("   Erode   ");
     tOrdre = new TextField((new StringBuilder(String.valueOf(rank))).toString(), 2);
     tOrdre2 = new TextField((new StringBuilder(String.valueOf(rank2))).toString(), 2);
     bD = new Button("   Dilation   ");
     bDFuzzy = new Button("   Fuzzy Dilation   ");
     b4 = new Button("    Clear    ");
    // b5 = new Button("   Generate   ");
     b7 = new Button("   Load Point Set  ");
    // b8 = new Button("   Save Point Set  ");
     //i7 = new Button("   Load Image  ");
     i8 = new Button("   Save Image  ");
     bC = new Button("Edges/Faces");
    // bConv = new Button("Convert");
     bInter = new Button(" Binary Op.");
     t0 = new TextField("1", 2);
     t1 = new TextField((new StringBuilder(String.valueOf(rnd))).toString(), 2);
     t2 = new TextField((new StringBuilder(String.valueOf(DT.densityParameter))).toString(), 2);
     t3 = new TextField((new StringBuilder(String.valueOf(DT.vertexAngle.x))).toString(), 2);
     t4 = new TextField((new StringBuilder(String.valueOf(DT.jitter))).toString(), 2);
     t8 = new TextField("22.5", 2);
     tvoid = new TextField("", 2);
     log = new TextArea(10, 20);
     p1 = new Panel();
     p2 = new Panel();
     p3 = new Panel();
     p4 = new Panel();
     init();
 }

 public void init()
 {
     p1.setLayout(new FlowLayout());
     p1.add(b1);
     p1.add(b2);
     p1.add(b4);
     //p1.add(b5);
     p1.add(b3);
     p1.add(bComp);
     p1.add(b6);
     p1.add(bE);
     p1.add(tOrdre);
     p1.add(bD);
     p1.add(bDFuzzy);
     p1.add(tOrdre2);
     p2.setLayout(new GridLayout(16, 2));
     p2.add(b7);
    // p2.add(b8);
     //p2.add(i7);
     p2.add(i8);
     p2.add(new Label("Scale Factor "));
     p2.add(t0);
     p2.add(new Label("Random points "));
     p2.add(t1);
     p2.add(new Label("Density parameter "));
     p2.add(t2);
     p2.add(new Label("Jitter "));
     p2.add(t4);
     JRadioButton type1 = new JRadioButton("Size");
     JRadioButton type2 = new JRadioButton("Radius", true);
     ButtonGroup tbb = new ButtonGroup();
     tbb.add(type1);
     tbb.add(type2);
     class _cls1SelectItemListenerTb
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("Radius"))
                     DT.type = 1;
                 else
                 if(sel.getText().equals("Size"))
                     DT.type = 0;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListenerTb()
         {
        	 super();
             this$0 = Alpha_Morph.this;
         }
     }

     type1.addItemListener(new _cls1SelectItemListenerTb());
     type2.addItemListener(new _cls1SelectItemListenerTb());
     JPanel pTb = new JPanel();
     pTb.add(type1);
     pTb.add(type2);
     p2.add(new Label("Criterium: "));
     p2.add(pTb);
     p2.add(bC);
    // p2.add(bConv);
     p2.add(new Label("Opacity "));
     p2.add(slider_opacity);
     p2.add(new Label("Angle morpho "));
     p2.add(slider);
     p2.add(new Label("Angle"));
     p2.add(new Label("Epsilon"));
     p2.add(t3);
     p2.add(t8);
     p2.add(new Label("Binary Operators ..."));
     p2.add(tvoid);
     JRadioButton rbb1 = new JRadioButton("1");
     JRadioButton rbb2 = new JRadioButton("2", true);
     JRadioButton rbb3 = new JRadioButton("3");
     JRadioButton rbb4 = new JRadioButton("4");
     ButtonGroup bgg = new ButtonGroup();
     bgg.add(rbb1);
     bgg.add(rbb2);
     bgg.add(rbb3);
     bgg.add(rbb4);
     class _cls1SelectItemListener1
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("1"))
                     toggle1 = 0;
                 else
                 if(sel.getText().equals("2"))
                     toggle1 = 1;
                 else
                 if(sel.getText().equals("3"))
                     toggle1 = 2;
                 else
                 if(sel.getText().equals("4"))
                     toggle1 = 3;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListener1()
         {
        	 super();
             this$0 = Alpha_Morph.this;
         }
     }

     rbb1.addItemListener(new _cls1SelectItemListener1());
     rbb2.addItemListener(new _cls1SelectItemListener1());
     rbb3.addItemListener(new _cls1SelectItemListener1());
     rbb4.addItemListener(new _cls1SelectItemListener1());
     JPanel p6 = new JPanel();
     p6.add(rbb1);
     p6.add(rbb2);
     p6.add(rbb3);
     p6.add(rbb4);
     p2.add(new Label("Operand 1"));
     p2.add(p6);
     JRadioButton rbbb1 = new JRadioButton("1");
     JRadioButton rbbb2 = new JRadioButton("2");
     JRadioButton rbbb3 = new JRadioButton("3", true);
     JRadioButton rbbb4 = new JRadioButton("4");
     ButtonGroup bggg = new ButtonGroup();
     bggg.add(rbbb1);
     bggg.add(rbbb2);
     bggg.add(rbbb3);
     bggg.add(rbbb4);
     class _cls1SelectItemListener2
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("1"))
                     toggle2 = 0;
                 else
                 if(sel.getText().equals("2"))
                     toggle2 = 1;
                 else
                 if(sel.getText().equals("3"))
                     toggle2 = 2;
                 else
                 if(sel.getText().equals("4"))
                     toggle2 = 3;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListener2()
         {
        	 super();
             this$0 = Alpha_Morph.this;
         }
     }

     rbbb1.addItemListener(new _cls1SelectItemListener2());
     rbbb2.addItemListener(new _cls1SelectItemListener2());
     rbbb3.addItemListener(new _cls1SelectItemListener2());
     rbbb4.addItemListener(new _cls1SelectItemListener2());
     JPanel p7 = new JPanel();
     p7.add(rbbb1);
     p7.add(rbbb2);
     p7.add(rbbb3);
     p7.add(rbbb4);
     p2.add(new Label("Operand 2"));
     p2.add(p7);
     JRadioButton rbbbb1 = new JRadioButton("1");
     JRadioButton rbbbb2 = new JRadioButton("2");
     JRadioButton rbbbb3 = new JRadioButton("3");
     JRadioButton rbbbb4 = new JRadioButton("4", true);
     ButtonGroup bgggg = new ButtonGroup();
     bgggg.add(rbbbb1);
     bgggg.add(rbbbb2);
     bgggg.add(rbbbb3);
     bgggg.add(rbbbb4);
     class _cls1SelectItemListener3
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("1"))
                     toggle3 = 0;
                 else
                 if(sel.getText().equals("2"))
                     toggle3 = 1;
                 else
                 if(sel.getText().equals("3"))
                     toggle3 = 2;
                 else
                 if(sel.getText().equals("4"))
                     toggle3 = 3;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListener3()
         {
        	 super();
             this$0 = Alpha_Morph.this;
         }
     }

     rbbbb1.addItemListener(new _cls1SelectItemListener3());
     rbbbb2.addItemListener(new _cls1SelectItemListener3());
     rbbbb3.addItemListener(new _cls1SelectItemListener3());
     rbbbb4.addItemListener(new _cls1SelectItemListener3());
     JPanel p8 = new JPanel();
     p8.add(rbbbb1);
     p8.add(rbbbb2);
     p8.add(rbbbb3);
     p8.add(rbbbb4);
     p2.add(new Label("Result"));
     p2.add(p8);
     JRadioButton rb1 = new JRadioButton("1", true);
     JRadioButton rb2 = new JRadioButton("2");
     JRadioButton rb3 = new JRadioButton("3");
     JRadioButton rb4 = new JRadioButton("4");
     ButtonGroup bg = new ButtonGroup();
     bg.add(rb1);
     bg.add(rb2);
     bg.add(rb3);
     bg.add(rb4);
     class _cls1SelectItemListener
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("1"))
                     toggle = 0;
                 else
                 if(sel.getText().equals("2"))
                     toggle = 1;
                 else
                 if(sel.getText().equals("3"))
                     toggle = 2;
                 else
                 if(sel.getText().equals("4"))
                     toggle = 3;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListener()
         {
        	 super();
             this$0 = Alpha_Morph.this;
         }
     }

     rb1.addItemListener(new _cls1SelectItemListener());
     rb2.addItemListener(new _cls1SelectItemListener());
     rb3.addItemListener(new _cls1SelectItemListener());
     rb4.addItemListener(new _cls1SelectItemListener());
     JPanel p5 = new JPanel();
     p5.add(rb1);
     p5.add(rb2);
     p5.add(rb3);
     p5.add(rb4);
     p1.add(p5);
     p1.add(bInter);
     JRadioButton rbbin1 = new JRadioButton("Inter", true);
     JRadioButton rbbin2 = new JRadioButton("Union");
     ButtonGroup bgbin = new ButtonGroup();
     bgbin.add(rbbin1);
     bgbin.add(rbbin2);
     class _cls1SelectItemListenerBin
         implements ItemListener
     {

         public void itemStateChanged(ItemEvent e)
         {
             AbstractButton sel = (AbstractButton)e.getItemSelectable();
             if(e.getStateChange() == 1)
                 if(sel.getText().equals("Inter"))
                     toggleBin = 0;
                 else
                 if(sel.getText().equals("Union"))
                     toggleBin = 1;
         }

         final Alpha_Morph this$0;

         _cls1SelectItemListenerBin()
         {
        	 super();
        	 this$0 = Alpha_Morph.this;
         }
     }

     rbbin1.addItemListener(new _cls1SelectItemListenerBin());
     rbbin2.addItemListener(new _cls1SelectItemListenerBin());
     JPanel p9 = new JPanel();
     p9.add(rbbin1);
     p9.add(rbbin2);
     p1.add(p9);
     p3.setLayout(new BorderLayout());
     p3.add("Center", log);
     p3.add("North", p2);
     reset();
     p4.setLayout(new GridLayout(2, 2));
     for(int i = 0; i < 4; i++)
         p4.add(disp[i]);

     add("Center", p4);
     add("East", p3);
     add("South", p1);
     pack();
     GUI.center(this);
     setVisible(true);
     slider.setMajorTickSpacing(20);
     slider.setMinorTickSpacing(10);
     slider.setPaintTicks(false);
     slider.setPaintLabels(false);
     slider_opacity.setMajorTickSpacing(10);
     slider_opacity.setMinorTickSpacing(5);
     slider_opacity.setPaintTicks(false);
     slider_opacity.setPaintLabels(false);
     slider.addMouseListener(new MouseListener() {
    	 {
             //super();
              this$0 = Alpha_Morph.this;
          }

         public void mousePressed(MouseEvent mouseevent)
         {
         }

         public void mouseClicked(MouseEvent mouseevent)
         {
         }

         public void mouseReleased(MouseEvent me)
         {
             angle = (360 * me.getX()) / slider.getWidth();
             if(angle > 360D)
                 angle = 360D;
             if(angle < 0.0D)
                 angle = 0.0D;
             String angleS = (new StringBuilder()).append(angle).toString();
             slider.setValue((int)angle / 1);
             t3.setText(angleS);
             try
             {
                 double angleRadian = ((360D - angle) * 3.1415926535897931D * 2D) / 360D;
                 DT.vertexAngle.x = Math.cos(angleRadian);
                 DT.vertexAngle.y = Math.sin(angleRadian);
             }
             catch(NumberFormatException nfe)
             {
                 DT.vertexAngle.x = -1D;
                 DT.vertexAngle.y = -1D;
             }
         }

         public void mouseEntered(MouseEvent mouseevent)
         {
         }

         public void mouseExited(MouseEvent mouseevent)
         {
         }

         final Alpha_Morph this$0;
 
         
     }
);
     slider_opacity.addMouseListener(new MouseListener() {

         public void mousePressed(MouseEvent mouseevent)
         {
         }

         public void mouseClicked(MouseEvent mouseevent)
         {
         }

         public void mouseReleased(MouseEvent me)
         {
             DT.opacity = (float)me.getX() / (float)slider.getWidth();
             disp[toggle].repaint();
         }

         public void mouseEntered(MouseEvent mouseevent)
         {
         }

         public void mouseExited(MouseEvent mouseevent)
         {
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     t0.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 DT.res = Float.valueOf(s).intValue();
             }
             catch(NumberFormatException nfe)
             {
                 DT.res = 1.0D;
             }
             if(DT.res < 1.0D)
                 DT.res = 1.0D;
             t0.setText((new StringBuilder(String.valueOf(DT.res))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     t1.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 rnd = Integer.valueOf(s).intValue();
             }
             catch(NumberFormatException nfe)
             {
                 rnd = 30;
             }
             if(rnd < 3)
                 rnd = 3;
             t1.setText((new StringBuilder(String.valueOf(rnd))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
           //  super();
         }
     }
);
     t2.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 DT.densityParameter = Double.valueOf(s).doubleValue();
             }
             catch(NumberFormatException nfe)
             {
                 DT.densityParameter = 1.5D;
             }
             if(DT.densityParameter < 0.0D)
                 DT.densityParameter = 1.0D;
             t2.setText((new StringBuilder(String.valueOf(DT.densityParameter))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            /// super();
         }
     }
);
     t3.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             angle = -1D;
             try
             {
                 angle = Double.valueOf(s).doubleValue();
                 if(angle < 0.0D)
                 {
                     DT.vertexAngle.x = -1D;
                     DT.vertexAngle.y = -1D;
                 } else
                 {
                     double angleRadian = ((360D - angle) * 3.1415926535897931D * 2D) / 360D;
                     DT.vertexAngle.x = Math.cos(angleRadian);
                     DT.vertexAngle.y = Math.sin(angleRadian);
                 }
             }
             catch(NumberFormatException nfe)
             {
                 DT.vertexAngle.x = -1D;
                 DT.vertexAngle.y = -1D;
             }
             t3.setText((new StringBuilder(String.valueOf(angle))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
     t4.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 DT.jitter = Double.valueOf(s).doubleValue();
             }
             catch(NumberFormatException nfe)
             {
                 DT.jitter = 0.0D;
             }
             if(DT.jitter > 0.10000000000000001D)
                 DT.jitter = 0.0D;
             if(DT.jitter < -0.10000000000000001D)
                 DT.jitter = 0.0D;
             t4.setText((new StringBuilder(String.valueOf(DT.jitter))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     t8.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             epsilon = 0.39269908169872414D;
             try
             {
                 epsilon = Double.valueOf(s).doubleValue();
                 if(epsilon < 0.0D)
                     epsilon = 0.0D;
                 else
                     epsilon = (epsilon * 3.1415926535897931D) / 180D;
             }
             catch(NumberFormatException nfe)
             {
                 epsilon = 0.39269908169872414D;
             }
             t8.setText((new StringBuilder(String.valueOf((epsilon * 180D) / 3.1415926535897931D))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     tOrdre.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 rank = Integer.valueOf(s).intValue();
             }
             catch(NumberFormatException nfe)
             {
                 rank = 0;
             }
             if(rank < 0)
                 rank = 0;
             tOrdre.setText((new StringBuilder(String.valueOf(rank))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
     tOrdre2.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             String s = e.getActionCommand();
             try
             {
                 rank2 = Integer.valueOf(s).intValue();
             }
             catch(NumberFormatException nfe)
             {
                 rank2 = 0;
             }
             if(rank2 < 0)
                 rank2 = 0;
             tOrdre.setText((new StringBuilder(String.valueOf(rank2))).toString());
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     b1.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             if(waiting)
                 waiting = false;
             for(int i = 0; i < 4; i++)
                 disp[i].run(0);

             b6.setEnabled(true);
             b3.setEnabled(true);
             bE.setEnabled(true);
             bD.setEnabled(true);
             tOrdre.setEnabled(true);
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
     b2.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             disp[toggle].swap(18);
             if(b2.getBackground() == Color.RED)
                 b2.setBackground(Color.LIGHT_GRAY);
             else
                 b2.setBackground(Color.RED);
             if(b6.isEnabled())
                 b6.setEnabled(false);
             else
                 b6.setEnabled(true);
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     b3.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             disp[toggle].swap(13);
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     bComp.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             dt[toggle].constructComp();
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
           //  super();
         }
     }
);
     b4.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             reset();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
    /* b5.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             reset();
             disp[toggle].resetAll(true);
             disp[toggle].clearPoints();
             disp[toggle].enableAdding(true);
             disp[toggle].loadPoints(rnd, minx, miny, maxx, maxy, load);
             disp[toggle].show(10);
             disp[toggle].repaint();
             b1.setEnabled(true);
             b3.setEnabled(false);
             b6.setEnabled(false);
             bE.setEnabled(false);
             bD.setEnabled(false);
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);*/
     b6.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             rank = 0;
             disp[toggle].setRank(rank);
             rank2 = 0;
             disp[toggle].setRank2(rank2);
             tOrdre.setText((new Integer(rank)).toString());
             tOrdre2.setText((new Integer(rank2)).toString());
             dt[toggle].constructBin(sitesBin);
             disp[toggle].hide(11);
             disp[toggle].repaint();
             b6.setEnabled(false);
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
     bE.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             rank++;
             disp[toggle].setRank(rank);
             tOrdre.setText((new Integer(rank)).toString());
             dt[toggle].constructErode();
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
     bD.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             rank2++;
             disp[toggle].setRank2(rank2);
             tOrdre2.setText((new Integer(rank2)).toString());
             dt[toggle].constructDilate(angle, epsilon);
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
           //  super();
         }
     }
);
     bDFuzzy.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             dt[toggle].constructFuzzyDilate(angle, epsilon);
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     bInter.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             disp[toggle].run(0);
             disp[toggle3].hide(11);
             if(toggleBin == 0)
                 dt[toggle3].constructInter(dt[toggle1], dt[toggle2]);
             else
                 dt[toggle3].constructUnion(dt[toggle1], dt[toggle2]);
             disp[toggle3].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
           //  super();
         }
     }
);
     bC.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             disp[toggle].swap(17);
             disp[toggle].repaint();
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
 /*    bConv.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             final JFrame frame = new JFrame();
             frame.setLocationRelativeTo(null);
             frame.setVisible(true);
             final Dialog d = new Dialog(frame, (new StringBuilder("The jitter parameter will be set to ")).append(DT.jitter).toString());
             d.setVisible(true);
             d.setSize(400, 100);
             d.setLocationRelativeTo(frame);
             Button dbtn;
             d.add(dbtn = new Button("OK"), "South");
             dbtn.addActionListener(new ActionListener() {

                 public void actionPerformed(ActionEvent e)
                 {
                     d.dispose();
                     frame.dispose();
                     disp[toggle].convertImage();
                 }

                 final _cls23 this$1;
                 private final Dialog val$d;
                 private final JFrame val$frame;

                 
                 {
                     this$1 = _cls23.this;
                     d = dialog;
                     frame = jframe;
                     super();
                 }
             }
);
         }

         final Alpha_Morph this$0;


         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);*/
     b7.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
        	   reset();
               disp[toggle].resetAll(true);
               disp[toggle].clearPoints();
               disp[toggle].enableAdding(true);
               disp[toggle].loadPoints(rnd, minx, miny, maxx, maxy, load);
               disp[toggle].show(10);
               disp[toggle].repaint();
               b1.setEnabled(true);
               b3.setEnabled(false);
               b6.setEnabled(false);
               bE.setEnabled(false);
               bD.setEnabled(false);
              
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);
   /*  b8.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             JFrame frame = new JFrame("File");
             FileDialog fd = new FileDialog(frame, "Quel fichier ?", 1);
             fd.setVisible(true);
             String selectedItem = fd.getFile();
             if(selectedItem != null)
             {
                 File file = new File((new StringBuilder(String.valueOf(fd.getDirectory()))).append(File.separator).append(fd.getFile()).toString());
                 System.out.println((new StringBuilder("saving file ")).append(fd.getDirectory()).append(File.separator).append(fd.getFile()).toString());
                 disp[toggle].writePoints(file);
             }
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);*/
    /* i7.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             ImagePlus imp = WindowManager.getCurrentImage();
             if(imp == null)
             {
                 JFrame frame = new JFrame();
                 FileDialog fd = new FileDialog(frame, "Quel fichier ?", 0);
                 fd.setVisible(true);
                 String selectedItem = fd.getFile();
                 if(selectedItem != null)
                 {
                     File file = new File((new StringBuilder(String.valueOf(fd.getDirectory()))).append(File.separator).append(fd.getFile()).toString());
                     System.out.println((new StringBuilder("loading file ")).append(fd.getDirectory()).append(File.separator).append(fd.getFile()).toString());
                     fileImage = file;
                     disp[toggle].loadImage(file);
                 }
                 return;
             } else
             {
                 java.awt.Image image = imp.getImage();
                 disp[toggle].loadImage(image);
                 return;
             }
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
             //super();
         }
     }
);*/
     i8.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e)
         {
             JFrame frame = new JFrame("File");
             FileDialog fd = new FileDialog(frame, "Quel fichier ?", 1);
             fd.setVisible(true);
             String selectedItem = fd.getFile();
             if(selectedItem != null)
             {
                 File file = new File((new StringBuilder(String.valueOf(fd.getDirectory()))).append(File.separator).append(fd.getFile()).toString());
                 System.out.println((new StringBuilder("saving file ")).append(fd.getDirectory()).append(File.separator).append(fd.getFile()).toString());
                 disp[toggle].writeImage(file);
             }
         }

         final Alpha_Morph this$0;

         
         {
             this$0 = Alpha_Morph.this;
            // super();
         }
     }
);
     myThread = new Thread(this);
     myThread.start();
 }

 public void reset()
 {
     for(int i = 0; i < 4; i++)
     {
         disp[i].resetAll(true);
         disp[i].clearPoints();
         disp[i].enableAdding(true);
         disp[i].show(10);
         disp[i].hide(17);
         disp[i].hide(11);
         disp[i].hide(18);
         disp[i].repaint();
         dt[i].resetAll();
     }

     running = false;
     sites = null;
     sitesBin = null;
     counter = 0;
     b1.setEnabled(false);
     b2.setEnabled(false);
     b3.setEnabled(false);
     b4.setEnabled(true);
     //b5.setEnabled(true);
     b6.setEnabled(false);
     bE.setEnabled(false);
     bD.setEnabled(false);
     bC.setEnabled(true);
     b2.setBackground(Color.LIGHT_GRAY);
 }

 public void run()
 {
     do
     {
         b4.setEnabled(true);
         //b5.setEnabled(true);
         while(waiting) 
             if(disp[toggle].numPoints() > 2)
             {
                 b1.setEnabled(true);
                 b2.setEnabled(true);
             }
         b4.setEnabled(false);
        // b5.setEnabled(false);
         for(int i = 0; i < 4; i++)
         {
             disp[toggle].resetAll(false);
             dt[toggle].resetAll();
         }

         b2.setBackground(Color.LIGHT_GRAY);
         disp[toggle].enableAdding(true);
         running = true;
         sites = disp[toggle].getPoints();
         disp[toggle].setSites(sites);
         disp[toggle].show(8);
         for(int i = 0; i < 4; i++)
             if(i != toggle)
             {
                 disp[i].setPoints(sites);
                 disp[i].setSites(sites);
                 disp[i].show(8);
                 disp[i].repaint();
             }

         sitesBin = disp[toggle].getPointsBin();
         disp[toggle].show(11);
         disp[toggle].show(5);
         disp[toggle].show(6);
         disp[toggle].show(1);
         startTime = System.currentTimeMillis();
         if(counter == 0)
         {
             for(int i = 0; i < 4; i++)
             {
                 dt[i].inCoDeTotal(disp[i], sites, sitesBin);
                 dt[i].setMeasure();
             }

         } else
         {
             dt[toggle].inCoDeTotal(disp[toggle], sites, sitesBin);
             dt[toggle].setMeasure();
         }
         counter++;
         endTime = System.currentTimeMillis();
         time = endTime - startTime;
         time /= 4D;
         log.append("Algorithm: Incremental construction\n");
         log.append((new StringBuilder("Total vertices: ")).append(disp[toggle].numPoints()).append("\n").toString());
         log.append((new StringBuilder("Total edges: ")).append(dt[toggle].getEdges().size()).append("\n").toString());
         log.append((new StringBuilder("Total faces: ")).append(dt[toggle].getFaces().size()).append("\n").toString());
         int eulerNumber = (dt[toggle].getFaces().size() - dt[toggle].getEdges().size()) + disp[toggle].numPoints();
         log.append((new StringBuilder("Euler number: ")).append(eulerNumber).append("\n").toString());
         log.append((new StringBuilder("No. of Delaunay tests: ")).append(DT.counter2).append("\n").toString());
         log.append((new StringBuilder("Mesure Mean: ")).append(dt[toggle].getMeasureMean()).append(" \n").toString());
         log.append((new StringBuilder("Mesure Median: ")).append(dt[toggle].getMeasureMedian()).append(" \n").toString());
         log.append("\n");
         log.append((new StringBuilder("Elapsed time: ")).append(time).append(" msec\n").toString());
         log.append("\n");
         for(int i = 0; i < 4; i++)
             disp[i].enableAdding(true);

         disp[toggle].repaint();
         running = false;
         waiting = true;
     } while(true);
 }

 boolean running;
 boolean waiting;
 int counter;
 boolean canAddPoints;
 File fileImage;
 int rnd;
 int rank;
 int rank2;
 int toggle;
 int toggle1;
 int toggle2;
 int toggle3;
 int toggleBin;
 long startTime;
 long endTime;
 long time;
 double angle;
 double epsilon;
 private Thread myThread;
 Neighbor[] load; 
 Vertex sites[];
 Vertex sitesBin[];
 DelaunayTriangulator dt[];
 Display disp[];
 JSlider slider;
 JSlider slider_opacity;
 Button b1;
 Button b2;
 Button b3;
 Button bComp;
 Button b6;
 Button bE;
 TextField tOrdre;
 TextField tOrdre2;
 Button bD;
 Button bDFuzzy;
 Button b4;
// Button b5;
 Button b7;
 //Button b8;
 //Button i7;
 Button i8;
 Button bC;
// Button bConv;
 Button bInter;
 TextField t0;
 TextField t1;
 TextField t2;
 TextField t3;
 TextField t4;
 TextField t8;
 TextField tvoid;
 TextArea log;
 Panel p1;
 Panel p2;
 Panel p3;
 Panel p4;
 double minx; 
 double miny;
 double maxx;
 double maxy; 
}
