package plugins.ragini.voronoidiagram;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Site {

	protected PriorityQueue<Neighbor> neighbors; 
	protected Point coord;
	private int sitenbr, refcnt; 
	private ROI2DSite point; 
	
	public Site() {
		NeighborComp comp = new NeighborComp(); 
		neighbors = new PriorityQueue<Neighbor>(5, comp); 
	}
	
	public Point getCoord() {
		return coord; 
	}
	
	public int sitenbr() {
		return sitenbr; 
	}
	
	public void setRefCnt(int r) {
		refcnt = r; 
	}
	
	public void setSiteNbr(int n) {
		sitenbr = n; 
	}
///>>>????
	
	public void setCoord(Point p) {
		coord = p; 
	}
	
	public PriorityQueue<Neighbor> neighbors() {
		return neighbors; 
	}
	
	private class NeighborComp implements Comparator<Neighbor> {

		@Override
		public int compare(Neighbor n1, Neighbor n2) {
			if (distance(coord.X(), coord.Y(), n1.getCoord().X(), n1.getCoord().Y()) > distance(coord.X(), coord.Y(), n2.getCoord().X(), n2.getCoord().Y())) 
				return 1;
			else if (distance(coord.X(), coord.Y(), n1.getCoord().X(), n1.getCoord().Y()) < distance(coord.X(), coord.Y(), n2.getCoord().X(), n2.getCoord().Y())) 
				return -1; 
			else if (n1.getCoord().Y() > n2.getCoord().Y()) {
				n2.same(n1); 
				n1.same(n2); 
				return 1; }
			else if (n1.getCoord().Y() < n2.getCoord().Y()) {
				n1.same(n2); 
				n2.same(n1); 
				return -1; }
			else if (n1.getCoord().X() > n2.getCoord().X()) {
				n2.same(n1); 
				n1.same(n2); 
				return 1; }
			else if (n1.getCoord().X() < n2.getCoord().X()) {
				n1.same(n2); 
				n2.same(n1); 
				return -1; }
			else { 
				System.out.println("here"); 
				return 0;
			}
		}
		
		private double distance(double ax, double ay, double bx, double by) {
			return (Math.sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay)));
		}
		
	}
	
	public void setPoint(ROI2DSite p) {
		point = p; 
	}
	
	public ROI2DSite getPoint() {
		return point; 
	}
	
}
