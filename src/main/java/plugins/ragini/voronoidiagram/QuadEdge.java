package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   QuadEdge.java

import java.io.PrintStream;

//Referenced classes of package delaunay:
//         Vertex, Face

public class QuadEdge
{

 public QuadEdge(QuadEdge Onext, QuadEdge rot, Vertex data, boolean can)
 {
     this.Onext = Onext;
     this.data = data;
     this.rot = rot;
     canonical = can;
     counter = 0;
     alpha = 0.0D;
 }

 public static QuadEdge makeEdge(Vertex orig, Vertex dest)
 {
     QuadEdge q0 = new QuadEdge(null, null, null, true);
     QuadEdge q1 = new QuadEdge(null, null, null, false);
     QuadEdge q2 = new QuadEdge(null, null, null, false);
     QuadEdge q3 = new QuadEdge(null, null, null, true);
     q0.rot = q1;
     q1.rot = q2;
     q2.rot = q3;
     q3.rot = q0;
     q0.Onext = q0;
     q1.Onext = q3;
     q2.Onext = q2;
     q3.Onext = q1;
     q0.setData(orig);
     q0.sym().setData(dest);
     return q0;
 }

 public static void splice(QuadEdge q1, QuadEdge q2)
 {
     QuadEdge alpha = q1.onext().rot();
     QuadEdge beta = q2.onext().rot();
     QuadEdge t1 = q2.onext();
     QuadEdge t2 = q1.onext();
     QuadEdge t3 = beta.onext();
     QuadEdge t4 = alpha.onext();
     q1.setOnext(t1);
     q2.setOnext(t2);
     alpha.setOnext(t3);
     beta.setOnext(t4);
 }

 public void splice(QuadEdge q)
 {
     splice(this, q);
 }

 public static void splice2(QuadEdge q1, QuadEdge q2)
 {
     q2.rot().setOnext(q1.rotSym());
     q1.setOnext(q2);
 }

 public void splice2(QuadEdge q)
 {
     splice2(this, q);
 }

 public static QuadEdge connect(QuadEdge a, QuadEdge b)
 {
     QuadEdge q = makeEdge(a.dest(), b.orig());
     splice(q, a.lnext());
     splice(q.sym(), b);
     return q;
 }

 public static void deleteEdge(QuadEdge q)
 {
     splice(q, q.oprev());
     splice(q.sym(), q.sym().oprev());
 }

 public int getCounter()
 {
     return counter;
 }

 public void setCounter(int c)
 {
     counter = c;
 }

 public void setFace(Face f)
 {
     rightF = f;
 }

 public void setFaces(Face f, Face g)
 {
     rightF = f;
     sym().rightF = g;
 }

 public void setAlpha(double alpha)
 {
     this.alpha = alpha;
 }

 public double getAlpha()
 {
     return alpha;
 }

 public void setLambda(float lbd)
 {
     lambda_val = lbd;
 }

 public float getLambda()
 {
     return lambda_val;
 }

 public void setMu(float lbd)
 {
     mu_val = lbd;
 }

 public float getMu()
 {
     return mu_val;
 }

 public void setOnext(QuadEdge next)
 {
     Onext = next;
 }

 public void setRot(QuadEdge rot)
 {
     this.rot = rot;
 }

 public void setData(Vertex data)
 {
     this.data = data;
 }

 public QuadEdge onext()
 {
     return Onext;
 }

 public QuadEdge rot()
 {
     return rot;
 }

 public QuadEdge sym()
 {
     return rot.rot();
 }

 public Vertex orig()
 {
     return data;
 }

 public Vertex dest()
 {
     return sym().orig();
 }

 public Vertex right()
 {
     return rot.orig();
 }

 public Vertex left()
 {
     return rot.sym().orig();
 }

 public Face rightFace()
 {
     return rightF;
 }

 public QuadEdge rotSym()
 {
     return rot.sym();
 }

 public QuadEdge oprev()
 {
     return rot.onext().rot();
 }

 public QuadEdge lnext()
 {
     return rotSym().onext().rot();
 }

 public QuadEdge lprev()
 {
     return onext().sym();
 }

 public QuadEdge rnext()
 {
     return rot.onext().rotSym();
 }

 public QuadEdge rprev()
 {
     return sym().onext();
 }

 public boolean isCanonical()
 {
     return canonical;
 }

 public boolean rightOf(Vertex s)
 {
     return s.ccw(sym().data, data);
 }

 public static boolean rightOf(QuadEdge q, Vertex s)
 {
     return s.ccw(q.dest(), q.orig());
 }

 public boolean leftOf(Vertex s)
 {
     return s.ccw(data, dest());
 }

 public static boolean leftOf(Vertex s, QuadEdge q)
 {
     return s.ccw(q.orig(), q.dest());
 }

 boolean equals(QuadEdge e)
 {
     return orig() == e.orig() && dest() == e.dest();
 }

 boolean equivalent(QuadEdge e)
 {
     return orig() == e.orig() && dest() == e.dest() || orig() == e.dest() && dest() == e.orig();
 }

 public int type(double med, int axis)
 {
     if(!orig().test(med, axis))
         return dest().test(med, axis) ? 0 : -1;
     return !dest().test(med, axis) ? 0 : 1;
 }

 public String toString()
 {
     return (new StringBuilder()).append(data).append(" ").append(sym().data).toString();
 }

 public void expliciter()
 {
     QuadEdge sym = sym();
     System.out.println((new StringBuilder(" Edge")).append(this).append(" onext()").append(onext()).append(" oprev").append(oprev()).append(" lnext").append(lnext()).append(" rprev").append(rprev()).toString());
     System.out.println((new StringBuilder(" Edge inverse ")).append(sym).append(" onext()").append(sym.onext()).append(" oprev").append(sym.oprev()).append(" lnext").append(sym.lnext()).append(" rprev").append(sym.rprev()).toString());
 }

 private QuadEdge Onext;
 private QuadEdge rot;
 private Vertex data;
 private int counter;
 private Face rightF;
 private double alpha;
 private float lambda_val;
 private float mu_val;
 private boolean canonical;
}

