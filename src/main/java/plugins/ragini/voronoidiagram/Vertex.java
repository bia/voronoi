package plugins.ragini.voronoidiagram;
//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   Vertex.java


import java.io.Serializable;

//Referenced classes of package delaunay:
//         DT

public class Vertex
 implements Serializable
{

 public Vertex(double x, double y)
 {
     visit = false;
     this.x = x;
     this.y = y;
 }

 public Vertex(double x, double y, int indice)
 {
     this(x, y);
     setIndice(indice);
 }

 public void setIndice(int indice)
 {
     this.indice = indice;
 }

 public static double distance(Vertex a, Vertex b)
 {
     if(a == DT.vertex_infty || b == DT.vertex_infty)
         return DT.MAX_VALUE;
     else
         return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
 }

 public static double sqrDistance(Vertex a, Vertex b)
 {
     if(a == DT.vertex_infty || b == DT.vertex_infty)
         return DT.MAX_VALUE;
     else
         return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
 }

 public static boolean colinear(Vertex a, Vertex b, Vertex c)
 {
     return (a.x * (b.y - c.y) - a.y * (b.x - c.x)) + (b.x * c.y - b.y * c.x) == 0.0D;
 }

 public boolean test(double med, int coord)
 {
     if(coord == 0)
         return x >= med;
     return y >= med;
 }

 public static boolean ccw(Vertex a, Vertex b, Vertex c)
 {
     return (a.x * (b.y - c.y) - a.y * (b.x - c.x)) + (b.x * c.y - b.y * c.x) > 0.0D;
 }

 public boolean ccw(Vertex b, Vertex c)
 {
     return (x * (b.y - c.y) - y * (b.x - c.x)) + (b.x * c.y - b.y * c.x) > 0.0D;
 }

 public static Vertex findCenter(Vertex A, Vertex B, Vertex C)
 {
     DT.counter2++;
     double aux = (((-B.x * C.y + B.x * A.y + A.x * C.y) - A.x * B.y) + B.y * C.x) - A.y * C.x;
     if(aux == 0.0D)
     {
         return DT.vertex_infty;
     } else
     {
         double ss = ((((-B.x * A.x - B.x * C.x) + A.x * C.x) - B.y * A.y - B.y * C.y) + A.y * C.y + B.x * B.x + B.y * B.y) / aux;
         return new Vertex(((A.x + C.x) - ss * (C.y - A.y)) / 2D, ((A.y + C.y) - ss * (A.x - C.x)) / 2D);
     }
 }

 public static Vertex findBary(Vertex A, Vertex B, Vertex C)
 {
     return new Vertex((A.x + C.x + B.x) / 3D, (A.y + C.y + B.y) / 3D);
 }

 public static double delaunayDistance(Vertex A, Vertex B, Vertex C)
 {
     Vertex S = findCenter(A, B, C);
     double ss = sqrDistance(S, A);
     if(ccw(A, B, C))
     {
         if(!ccw(A, B, S))
             ss = -ss;
     } else
     if(ccw(A, B, S))
         ss = -ss;
     return ss;
 }

 public boolean inCircle(Vertex a, Vertex b, Vertex c, Vertex d)
 {
     if(a == d || b == d || c == d)
         return false;
     DT.counter2++;
     double d02 = a.x * a.x + a.y * a.y;
     double d12 = b.x * b.x + b.y * b.y;
     double d22 = c.x * c.x + c.y * c.y;
     double d32 = d.x * d.x + d.y * d.y;
     double det = a.x * det3x3(b.y, d12, 1.0D, c.y, d22, 1.0D, d.y, d32, 1.0D);
     det -= a.y * det3x3(b.x, d12, 1.0D, c.x, d22, 1.0D, d.x, d32, 1.0D);
     det += d02 * det3x3(b.x, b.y, 1.0D, c.x, c.y, 1.0D, d.x, d.y, 1.0D);
     det -= det3x3(b.x, b.y, d12, c.x, c.y, d22, d.x, d.y, d32);
     return det > 0.0D;
 }

 public boolean inCircle(Vertex b, Vertex c, Vertex d)
 {
     if(this == d || b == d || c == d)
         return false;
     DT.counter2++;
     double d02 = x * x + y * y;
     double d12 = b.x * b.x + b.y * b.y;
     double d22 = c.x * c.x + c.y * c.y;
     double d32 = d.x * d.x + d.y * d.y;
     double det = x * det3x3(b.y, d12, 1.0D, c.y, d22, 1.0D, d.y, d32, 1.0D);
     det -= y * det3x3(b.x, d12, 1.0D, c.x, d22, 1.0D, d.x, d32, 1.0D);
     det += d02 * det3x3(b.x, b.y, 1.0D, c.x, c.y, 1.0D, d.x, d.y, 1.0D);
     det -= det3x3(b.x, b.y, d12, c.x, c.y, d22, d.x, d.y, d32);
     return det > 0.0D;
 }

 public static double det3x3(double d00, double d01, double d02, double d10, 
         double d11, double d12, double d20, double d21, double d22)
 {
     return (d00 * (d11 * d22 - d12 * d21) - d01 * (d10 * d22 - d12 * d20)) + d02 * (d10 * d21 - d11 * d20);
 }

 public static void mergeSort(Vertex s[])
 {
     mergeSort(s, 0, s.length);
 }

 public static void mergeSort(Vertex s[], int a, int b)
 {
     int size = b - a;
     if(size > 1)
         if(size == 2)
         {
             if(compare(s[a], s[a + 1]))
             {
                 Vertex tmp = s[a];
                 s[a] = s[a + 1];
                 s[a + 1] = tmp;
             }
         } else
         {
             int mid = size / 2;
             mergeSort(s, a, a + mid);
             mergeSort(s, a + mid, b);
             Vertex ascend[] = new Vertex[size];
             int i = a;
             int j = a + mid;
             for(int k = 0; k < size; k++)
                 if(i >= a + mid)
                     ascend[k] = s[j++];
                 else
                 if(j >= b)
                     ascend[k] = s[i++];
                 else
                 if(compare(s[i], s[j]))
                     ascend[k] = s[j++];
                 else
                     ascend[k] = s[i++];

             for(int k = 0; k < size; k++)
                 s[a + k] = ascend[k];

         }
 }

 public void setVisit()
 {
     visit = true;
 }

 public void setVisit(boolean visit)
 {
     this.visit = visit;
 }

 public boolean getVisit()
 {
     return visit;
 }

 public static boolean compare(Vertex s1, Vertex s2)
 {
     if(s1.x != s2.x)
         return s1.x > s2.x;
     return s1.y > s2.y;
 }

 public String toString()
 {
     return (new StringBuilder(" ")).append(indice).append(" ").toString();
 }

 double x;
 double y;
 int indice;
 boolean visit;
}
