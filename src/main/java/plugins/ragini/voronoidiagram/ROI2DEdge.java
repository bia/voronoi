package plugins.ragini.voronoidiagram;

import icy.roi.ROI2DLine;

import java.awt.geom.Line2D;

public class ROI2DEdge extends ROI2DLine {
	
	public ROI2DEdge(Line2D line) {
		super(line.getP1(), line.getP2()); 
	}

}
