package plugins.ragini.voronoidiagram;

import java.util.Arrays;
import java.util.Comparator;

public class Vonoroi
{

    private int siteidx, nvertices;

    HalfEdge PQHash;

    private Constants constants;

    private EdgeList edgelist;
    private VRegion vregion;
    private Geometry events;

    public Vonoroi(EdgeList _edgelist, Constants _constants, VRegion _vregion, Geometry geometry)
    {
        siteidx = 0;
        constants = _constants;
        edgelist = _edgelist;
        vregion = _vregion;
        events = geometry;
    }

    private class GLscomp implements Comparator<Vert>
    {

        @Override
        public int compare(Vert s1, Vert s2)
        {
            if (s1.Y() < s2.Y())
                return -1;
            else if (s1.Y() > s2.Y())
                return 1;
            else if (s1.X() < s2.X())
                return -1;
            else if (s1.X() > s2.X())
                return 1;
            else
                return 0;
        }

    }

    public void sortGLSites()
    {

        float vx, vy;
        int k, sid;
        char a;
        float dx, dy;

        GLscomp comp = new GLscomp();

        Arrays.sort(constants.getGLSites(), comp);

        // excise();

    }

    // private void excise() {
    //
    // int sid = 0;
    // Vert[] GLSites = constants.getGLSites();
    //
    // for(int k = 0; k < constants.nsites(); k++) {
    // if (k ==0 || (GLSites[k-1].Y() != GLSites[k].Y()) || (GLSites[k-1].X() != GLSites[k].X())) {
    //
    // dx = GLSites[k].x * (1.0 / 4096.0);
    // dy = GLSites[k].y * (1.0 / 4096.0);
    //
    // /* GLSites[sid].setX(GLSites[k].X());
    // GLSites[sid].setY(GLSites[k].Y()); */
    // }
    // }
    //
    // if (sid != constants.nsites()) {
    // System.out.printf("voronoi: excised %d coincident sites\n", constants.nsites()-sid);
    // }
    //
    // }

    public void voronoi()
    {
        SiteProvider sp = new SiteProvider(constants);

        Neighbor newsite, temp, p, v;
        Neighbor bot, top;
        Point newintstar = new Point();
        int pm;
        HalfEdge lbnd, rbnd, llbnd, rrbnd, bisector;
        Edge e;

        if (constants.nsites() <= 1)
            return;
        geominit();
        events.PQinitialize();

        constants.setBottomSite(sp.next());
        vregion.outSite(constants.bottomsite()); // print some stuff and draw a circle
        // circle(...s)
        edgelist.ELinitialize();

        newsite = sp.next();

        // System.out.println("initialized");

        // when do values get added to the hash table?
        while (true)
        {
            if (!events.PQempty())
                newintstar = events.PQmin();

            // newsite is above (greater priority) than the minimum event in the queue
            if (newsite != null
                    && (events.PQempty() || (newsite.getCoord().Y() < newintstar.Y()) || (newsite.getCoord().Y() == newintstar
                            .Y() && newsite.getCoord().X() < newintstar.X())))
            {
                // System.out.println("newsite is smallest");
                vregion.outSite(newsite); // print some stuff and draw a circle with pre-defined
                                          // radius
                lbnd = edgelist.ELleftbnd(newsite.getCoord()); // retrieve the halfedge from the
                                                               // hash table
                rbnd = edgelist.ELright(lbnd);
                bot = edgelist.rightReg(lbnd); // ?
                e = edgelist.bisect(bot, newsite);

                bisector = edgelist.HEcreate(e, constants.le(), 0); // create a new half edge from
                                                                    // the bisector?
                edgelist.ELinsert(lbnd, bisector); // insert it into the hash table? or the linear
                                                   // list?
                if ((p = (Neighbor) edgelist.sintersect(lbnd, bisector)) != null)
                {
                    events.PQdelete(lbnd);
                    events.PQinsert(lbnd, p, dist(p, newsite));
                }
                lbnd = bisector;
                bisector = edgelist.HEcreate(e, constants.re(), 0);
                edgelist.ELinsert(lbnd, bisector);
                if ((p = (Neighbor) edgelist.sintersect(bisector, rbnd)) != null)
                {
                    events.PQinsert(bisector, p, dist(p, newsite));
                }
                newsite = sp.next();
            }
            else if (!events.PQempty())
            {
                // System.out.println("newintstar is smallest");
                lbnd = events.PQextractmin();
                /*
                 * if (lbnd.eledge() == null) System.out.println("lbnd: null edge");
                 * else System.out.println("lbnd: " + lbnd.eledge().a() + ", " + lbnd.eledge().b() +
                 * ", " + lbnd.eledge().c());
                 */
                llbnd = edgelist.ELleft(lbnd);
                // if (llbnd.eledge() == null) System.out.println("llbnd: null edge");
                // else System.out.println("llbnd: " + llbnd.eledge().a() + ", " +
                // llbnd.eledge().b() + ", " + llbnd.eledge().c());
                rbnd = edgelist.ELright(lbnd);
                // if (rbnd.eledge() == null) System.out.println("rbnd: null edge");
                // else System.out.println("rbnd: " + rbnd.eledge().a() + ", " + rbnd.eledge().b() +
                // ", " + rbnd.eledge().c());
                rrbnd = edgelist.ELright(rbnd);
                // if (rrbnd.eledge() == null) System.out.println("rrbnd: null edge");
                // else System.out.println("rrbnd: " + rrbnd.eledge().a() + ", " +
                // rrbnd.eledge().b() + ", " + rrbnd.eledge().c());
                bot = edgelist.leftReg(lbnd);
                top = edgelist.rightReg(rbnd);
                // System.out.println("bot: " + bot.getCoord().X() + " " + bot.getCoord().Y());
                // System.out.println("top: " + top.getCoord().X() + " " + top.getCoord().Y());
                vregion.outTriple(bot, top, edgelist.rightReg(lbnd)); // this is where the
                                                                      // triangulate variable is
                                                                      // used, just printing some
                                                                      // stuff
                v = (Neighbor) lbnd.vertex();
                // if (v == null)
                // System.out.println("vertex null");
                // else System.out.println("vertex: " + v.getCoord().X() + " " + v.getCoord().Y());
                makevertex(v);
                // System.out.println("made vertex");
                edgelist.v_endpoint(lbnd.eledge(), 1, lbnd.elpm(), v);
                edgelist.v_endpoint(rbnd.eledge(), 1, rbnd.elpm(), v);
                // System.out.println("called vendpoint");
                edgelist.ELdelete(lbnd);
                //
                events.PQdelete(rbnd);
                edgelist.ELdelete(rbnd);
                /*
                 * System.out.println("printing list");
                 * for(HalfEdge cur = edgelist.leftend(); cur != edgelist.rightend(); cur =
                 * edgelist.ELright(cur)) {
                 * e = cur.eledge();
                 * if (e == null) System.out.println("null edge");
                 * else System.out.println("edge: " + e.a() + " " + e.b() + " " + e.c());
                 * if (cur.right() == edgelist.rightend()) {
                 * System.out.println("reached right end"); e = cur.right().eledge(); if (e != null)
                 * System.out.println("right end: " + e.a() + " " + e.b() + " " + e.c());}
                 * }
                 * System.out.println("done printing list");
                 * if (rbnd.eledge() == null) System.out.println("deleted null edge");
                 * else System.out.println("deleted: " + rbnd.eledge().a() + ", " +
                 * rbnd.eledge().b() + ", " + rbnd.eledge().c());
                 */
                pm = constants.le();

                if ((bot.getCoord().Y() > top.getCoord().Y())
                        || ((bot.getCoord().Y() == top.getCoord().Y()) && (bot.getCoord().X() > top.getCoord().X())))
                {
                    temp = bot;
                    bot = top;
                    top = temp;
                    pm = constants.re();
                }

                //

                e = edgelist.bisect(bot, top);

                bisector = edgelist.HEcreate(e, pm, 1);
                edgelist.ELinsert(llbnd, bisector);
                /*
                 * System.out.println("right : " + bisector.right().eledge().a() + ", " +
                 * bisector.right().eledge().b() + ", " + bisector.right().eledge().c());
                 * if (bisector.right().right().eledge() == null) System.out.println("rright null");
                 * else System.out.println("rright: " + bisector.right().right().eledge().a() + ", "
                 * + bisector.right().right().eledge().b() + ", " +
                 * bisector.right().right().eledge().c());
                 */
                edgelist.v_endpoint(e, 1, constants.re() - pm, v);
                if ((p = (Neighbor) edgelist.sintersect(llbnd, bisector)) != null)
                {
                    events.PQdelete(llbnd);
                    // if (lbnd.eledge() == null) System.out.println("deleted null edge");
                    // else System.out.println("deleted: " + lbnd.eledge().a() + ", " +
                    // lbnd.eledge().b() + ", " + lbnd.eledge().c());
                    events.PQinsert(llbnd, p, dist(p, bot));
                }

                if ((p = (Neighbor) edgelist.sintersect(bisector, rrbnd)) != null)
                {
                    events.PQinsert(bisector, p, dist(p, bot));
                }
            }
            else
            { // System.out.println("break voronoi");
                break;
            }
        }

        for (lbnd = edgelist.ELright(edgelist.leftend()); lbnd != edgelist.rightend(); lbnd = edgelist.ELright(lbnd))
        {
            e = lbnd.eledge();
            // System.out.println("printing edge: " + e.a() + " " + e.b() + " " + e.c());
            vregion.outEP(e, lbnd.origin());// calls clipline, which draws the appropriate line
            // if (lbnd.right() == edgelist.rightend()) System.out.println("reached right end");
        }

    }

    public void makevertex(Site v)
    {
        v.setSiteNbr(nvertices);
        nvertices += 1;
        vregion.outVertex(v); // /print some stuff
    }

    private void geominit()
    {
        nvertices = 0;
        constants.setnedges(0);
        float sn = constants.nsites() + 4;
        constants.setsqrtnsites(Math.sqrt(sn));
        // System.out.println("setting sqrt" + constants.sqrtnsites());
        constants.setvdeltay(constants.vymax() - constants.vymin());
        constants.setvdeltax(constants.vxmax() - constants.vxmin());
    }

    public float dist(Site a, Site b)
    {
        float ax, bx, ay, by;

        ax = a.getCoord().X();
        ay = a.getCoord().Y();

        bx = b.getCoord().X();
        by = b.getCoord().Y();

        return (float) Math.sqrt((bx - ax) * (bx - ax) + (by - ay) * (by - ay));

    }

}
