package plugins.ragini.voronoidiagram;

public class Point {

	protected float x, y; 
	
	public float X(){
		return x;
	}
	
	public float Y() {
		return y; 
	}
	
	public void setX(float _x) {
		x = _x;
	}
	
	public void setY(float _y) {
		y = _y; 
	}
	
}
