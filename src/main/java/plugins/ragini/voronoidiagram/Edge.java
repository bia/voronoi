package plugins.ragini.voronoidiagram;

public class Edge {
	
	private float a, b, c;
	private Site[] ep;
	private Site[] reg;
	private int edgenbr;
	
	public Edge() {
		ep = new Site[2];
		reg = new Site[2]; 
	}
	
	public float a() {
		return a;
	}
	
	public float b() {
		return b; 
	}
	
	public float c() {
		return c; 
	}
	
	public Site[] ep() {
		return ep;
	}
	
	public Site[] reg() {
		return reg; 
	}
	
	public void seta(double d) {
		a = (float) d; 
	}
	
	public void setb(double d) {
		b = (float) d; 
	}
	
	public void setc(double d) {
		c = (float) d; 
	}
	
	public void setedgenbr(int n) {
		edgenbr = n; 
	}
	
}
