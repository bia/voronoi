package plugins.ragini.voronoidiagram;

public class EdgeS {
	
	private float x1, y1, x2, y2;
	private int nbr1, nbr2;
	private float xm, ym; 
	
	public void setx1(float x) {
		x1 = x;
	}
	
	public void sety1(float y) {
		y1 = y;
	}
	
	public void setx2(float x) {
		x2 = x;
	}
	
	public void sety2(float y) {
		y2 = y; 
	}
	
	public void setnbr1(int n) {
		nbr1 = n;
	}
	
	public void setnbr2(int n) {
		nbr2 = n;
	}
	
	public void setxm(float x) {
		xm = x;
	}
	
	public void setym(float y) {
		ym = y; 
	}
	
	public float x1() {
		return x1;
	}
	
	public float y1() {
		return y1;
	}
	
	public float x2() {
		return x2;
	}
	
	public float y2() {
		return y2; 
	}
	
	public int nbr1() {
		return nbr1;
	}
	
	public int nbr2() {
		return nbr2; 
	}

}
