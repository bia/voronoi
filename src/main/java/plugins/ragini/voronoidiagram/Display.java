package plugins.ragini.voronoidiagram;
//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   Display.java


import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

//Referenced classes of package delaunay:
//         Vertex, QuadEdge, Face, DT, 
//         UniformGrid, DelaunayTriangulator, AFL

public class Display extends Canvas
 implements MouseListener
{

 public void dimensionize()
 {
     w = getWidth();
     h = getHeight();
     ss = Math.min((double)w / xsize, (double)h / ysize);
     ss_int = Math.min(w, h);
     setSize(ss_int, ss_int);
 }

 synchronized void run(int mode)
 {
     playmode = mode;
     stopped = false;
 }

 synchronized void stop()
 {
     stopped = true;
 }

 void breakSmall()
 {
     if(playmode == 1)
         stopped = true;
     if(stopped)
     {
         repaint();
         while(stopped) ;
     }
 }

 void breakBig()
 {
     if(playmode == 1 || playmode == 2)
         stopped = true;
     if(stopped)
     {
         repaint();
         while(stopped) ;
     }
 }

 Display(DelaunayTriangulator dt)
 {
     this.dt = null;
     ug = null;
     center = null;
     left = null;
     right = null;
     tri = null;
     triangles = null;
     edges = null;
     S = null;
     A = null;
     B = null;
     C = null;
     sites = null;
     Q = null;
     Q1 = null;
     Q2 = null;
     Q3 = null;
     frame = 1;
     coord = -1;
     rank = 1;
     rank2 = 1;
     loadImage = false;
     pts = new Vector();
     ptsBin = new Vector();
     canAdd = false;
     xoff = 0.0D;
     yoff = 0.0D;
     xsize = 1.0D;
     ysize = 1.0D;
     ss = 1.0D;
     sx = 1.0D;
     ssy = 1.0D;
     x0 = 0;
     y0 = 0;
     w = 400;
     h = 400;
     ss_int = 1;
     stopped = false;
     playmode = 0;
     enabled = new boolean[19];
     this.dt = dt;
     resetAll(true);
     setBackground(Color.white);
     addMouseListener(this);
     dimensionize();
 }

 public void setRank(int rank)
 {
     this.rank = rank;
 }

 public void setRank2(int rank2)
 {
     this.rank2 = rank2;
 }

 public void addPoint(Vertex v)
 {
     if(enabled[18])
         ptsBin.add(v);
     else
         pts.add(v);
 }

 public void clearPoints()
 {
     pts.clear();
     ptsBin.clear();
     image = null;
     loadImage = false;
 }

 public int numPoints()
 {
     return pts.size();
 }

 public Vertex[] getPoints()
 {
     Vertex sites[] = new Vertex[pts.size()];
     pts.copyInto(sites);
     return sites;
 }

 public void setPoints(Vertex sites[])
 {
     pts.clear();
     for(int i = 0; i < sites.length; i++)
         pts.add(sites[i]);

 }

 public Vertex[] getPointsBin()
 {
     Vertex sites[] = new Vertex[ptsBin.size()];
     ptsBin.copyInto(sites);
     return sites;
 }

 public void loadPoints(int n, double oldminx, double oldminy, double oldmaxx, 
         double oldmaxy, Neighbor[] sites)
 {
     pts.clear();
     for(Neighbor s: sites)  
     {
    	 double x = s.getCoord().X(); 
    	 double newx = (((x - oldminx) * (1))/(oldmaxx - oldminx)); 
    	 double y = s.getCoord().Y();
    	 double newy = (((y - oldminy) * (1))/(oldmaxy - oldminy)); 
         pts.add(new Vertex(newx, newy, pts.size()));
     }

 }

 public void enableAdding(boolean flag)
 {
     canAdd = flag;
 }

 public void mouseClicked(MouseEvent mouseevent)
 {
 }

 public void mouseReleased(MouseEvent mouseevent)
 {
 }

 public void mouseEntered(MouseEvent mouseevent)
 {
 }

 public void mouseExited(MouseEvent mouseevent)
 {
 }

 public void mousePressed(MouseEvent e)
 {
     e.consume();
     if(canAdd)
     {
         Vertex v = new Vertex((double)(e.getX() - x0) / ss + xoff, (double)(e.getY() - y0) / ss + yoff, pts.size());
         addPoint(v);
         setSites(getPoints());
         repaint();
     }
 }

 public void show(int flag)
 {
     enabled[flag] = true;
 }

 public void hide(int flag)
 {
     enabled[flag] = false;
 }

 public void swap(int flag)
 {
     if(!enabled[flag])
         enabled[flag] = true;
     else
         enabled[flag] = false;
 }

 public void setRange(double xo, double yo, double xs, double ys)
 {
     xoff = xo;
     yoff = yo;
     xsize = xs;
     ysize = ys;
     ss = Math.min((double)w / xsize, (double)h / ysize);
 }

 public void setWindow(int xleft, int yleft, int width, int height)
 {
     x0 = xleft;
     y0 = yleft;
     w = width;
     h = height;
     ss = Math.min((double)w / xsize, (double)h / ysize);
 }

 int xconv(double x)
 {
     return (int)((x - xoff) * ss) + x0;
 }

 int yconv(double y)
 {
     return (int)((y - yoff) * ss) + y0;
 }

 int dconv(double x)
 {
     return (int)(x * ss);
 }

 public void setCenterAFL(AFL c)
 {
     center = c;
 }

 public void setLeftAFL(AFL l)
 {
     left = l;
 }

 public void setRightAFL(AFL r)
 {
     right = r;
 }

 public void setTriangulation(AFL t)
 {
     tri = t;
 }

 public void setGrid(UniformGrid u)
 {
     ug = u;
 }

 public void setCircle(Vertex c, double r)
 {
     S = c;
     rad = r;
 }

 public void setTriangle(Vertex a, Vertex b, Vertex c)
 {
     A = a;
     B = b;
     C = c;
 }

 public void setSites(Vertex ss[])
 {
     sites = ss;
 }

 public void setSplittingLine(int c, double m)
 {
     coord = c;
     med = m;
 }

 public void setQuadEdge(QuadEdge q)
 {
     Q = q;
 }

 public void setQuadEdge3(QuadEdge q1, QuadEdge q2, QuadEdge q3)
 {
     Q1 = q1;
     Q2 = q2;
     Q3 = q3;
 }

 public void setTriangles(Vector triangles)
 {
     this.triangles = triangles;
 }

 public void setEdges(Vector edges)
 {
     this.edges = edges;
 }

 public void resetAll(boolean flag)
 {
     ug = null;
     center = left = right = null;
     tri = null;
     S = null;
     A = B = C = null;
     sites = null;
     coord = -1;
     Q = null;
     Q1 = Q2 = Q3 = null;
     rank = 1;
     if(flag)
     {
         for(int i = 0; i < 19; i++)
             enabled[i] = false;

     }
 }

 private void dessineMesh(Graphics g)
 {
     float surface = 0.0F;
     if(enabled[17])
     {
         System.out.println("Mode Contour");
         g.setColor(Color.green);
         for(Enumeration listing = edges.elements(); listing.hasMoreElements();)
         {
             QuadEdge e = (QuadEdge)listing.nextElement();
             QuadEdge e_sym = e.sym();
             if(e.getMu() >= 1.0F && e.getLambda() == 0.0F)
             {
                 float grey = (float)Math.min(1.0D, e.getMu());
                 grey = 1.0F - grey;
                 g.setColor(new Color(grey, grey, grey));
                 Graphics2D g2 = (Graphics2D)g;
                 g2.setStroke(new BasicStroke(3F));
                 g2.drawLine(xconv(e.orig().x), yconv(e.orig().y), xconv(e_sym.orig().x), yconv(e_sym.orig().y));
                 g2.setStroke(new BasicStroke(1.0F));
             }
         }

     } else
     {
         System.out.println("Mode Region");
         for(Enumeration listing = triangles.elements(); listing.hasMoreElements();)
         {
             Face f = (Face)listing.nextElement();
             QuadEdge list_edges[] = new QuadEdge[3];
             list_edges = f.getEdges();
             int x[] = new int[3];
             int y[] = new int[3];
             for(int i = 0; i < 3; i++)
             {
                 x[i] = xconv(list_edges[i].orig().x);
                 y[i] = yconv(list_edges[i].orig().y);
             }

             if(f.getVal() > 0.0F && f.getVal() <= 1.0F)
             {
                 float grey = f.getVal();
                 surface = (float)((double)surface + 1.0D / f.getMeasure());
                 g.setColor(new Color(grey, grey, grey, DT.opacity));
                 g.fillPolygon(x, y, 3);
                 g.setColor(Color.BLACK);
                 g.drawPolygon(x, y, 3);
             }
         }

     }
     System.out.println(surface);
 }

 public void paint(Graphics g)
 {
     dimensionize();
     getGraphics().setColor(Color.BLACK);
     getGraphics().drawRect(1, 1, ss_int - 2, ss_int - 2);
     setWindow(0, 0, w, h);
     setVisible(true);
     if(image != null)
         g.drawImage(image, 0, 0, ss_int, ss_int, this);
     
     g.setPaintMode();
     if(triangles != null)
         dessineMesh(g);
     if(ug != null && enabled[0])
     {
         g.setColor(Color.lightGray);
         g.fillRect(xconv(ug.XMin()), yconv(ug.YMin()), dconv(ug.XSize()), dconv(ug.YSize()));
     }
     if(pts != null && enabled[10])
     {
         g.setColor(Color.BLACK);
         for(int i = 0; i < pts.size(); i++)
         {
             Vertex v = (Vertex)pts.elementAt(i);
             g.drawLine(xconv(v.x), yconv(v.y), xconv(v.x), yconv(v.y));
         }

     }
     if(ptsBin != null && (enabled[10] || enabled[8]))
     {
         g.setColor(Color.BLACK);
         for(int i = 0; i < ptsBin.size(); i++)
         {
             Vertex v = (Vertex)ptsBin.elementAt(i);
             g.drawLine(xconv(v.x), yconv(v.y), xconv(v.x), yconv(v.y));
         }

     }
     if(sites != null && enabled[8])
     {
         g.setColor(Color.BLACK);
         for(int i = 0; i < sites.length; i++)
             g.drawLine(xconv(sites[i].x), yconv(sites[i].y), xconv(sites[i].x), yconv(sites[i].y));

     }
     if(coord >= 0 && ug != null && enabled[9])
     {
         g.setColor(Color.blue);
         if(coord == 0)
             g.drawLine(xconv(med), yconv(ug.YMin()), xconv(med), yconv(ug.YMax()));
         else
             g.drawLine(xconv(ug.XMin()), yconv(med), xconv(ug.XMax()), yconv(med));
     }
     if(Q != null && enabled[11])
     {
         g.setColor(Color.BLACK);
         QuadEdge qe;
         for(Enumeration e = dt.extractEdges(Q, frame++); e.hasMoreElements(); g.drawLine(xconv(qe.orig().x), yconv(qe.orig().y), xconv(qe.dest().x), yconv(qe.dest().y)))
             qe = (QuadEdge)e.nextElement();

     }
     if(Q != null && enabled[13])
     {
         g.setColor(Color.red);
         for(Enumeration e = dt.extractEdges(Q, frame++); e.hasMoreElements();)
         {
             QuadEdge qr = (QuadEdge)e.nextElement();
             QuadEdge qe = qr.rot();
             if(qe.orig() != null && qe.dest() != null)
                 g.drawLine(xconv(qe.orig().x), yconv(qe.orig().y), xconv(qe.dest().x), yconv(qe.dest().y));
         }

     }
     if(center != null && enabled[1])
     {
         g.setColor(Color.black);
         for(int i = 0; i < center.size(); i++)
         {
             QuadEdge e = center.get(i);
             g.drawLine(xconv(e.orig().x), yconv(e.orig().y), xconv(e.dest().x), yconv(e.dest().y));
         }

     }
     if(left != null && enabled[2])
     {
         g.setColor(Color.green);
         for(int i = 0; i < left.size(); i++)
         {
             QuadEdge e = left.get(i);
             g.drawLine(xconv(e.orig().x), yconv(e.orig().y), xconv(e.dest().x), yconv(e.dest().y));
         }

     }
     if(right != null && enabled[3])
     {
         g.setColor(Color.green);
         for(int i = 0; i < right.size(); i++)
         {
             QuadEdge e = right.get(i);
             g.drawLine(xconv(e.orig().x), yconv(e.orig().y), xconv(e.dest().x), yconv(e.dest().y));
         }

     }
     if(enabled[12])
     {
         System.out.println("OK EDGE3");
         if(Q1 != null)
         {
             g.setColor(Color.green);
             g.drawLine(xconv(Q1.orig().x), yconv(Q1.orig().y), xconv(Q1.dest().x), yconv(Q1.dest().y));
             g.drawRect(xconv(0.20000000000000001D * Q1.orig().x + 0.80000000000000004D * Q1.dest().x) - 1, yconv(0.20000000000000001D * Q1.orig().y + 0.80000000000000004D * Q1.dest().y) - 1, 3, 3);
         }
         if(Q2 != null)
         {
             g.setColor(Color.blue);
             g.drawLine(xconv(Q2.orig().x), yconv(Q2.orig().y), xconv(Q2.dest().x), yconv(Q2.dest().y));
             g.drawRect(xconv(0.20000000000000001D * Q2.orig().x + 0.80000000000000004D * Q2.dest().x) - 1, yconv(0.20000000000000001D * Q2.orig().y + 0.80000000000000004D * Q2.dest().y) - 1, 3, 3);
         }
         if(Q3 != null)
         {
             g.setColor(Color.gray);
             g.drawLine(xconv(Q3.orig().x), yconv(Q3.orig().y), xconv(Q3.dest().x), yconv(Q3.dest().y));
             g.drawRect(xconv(0.20000000000000001D * Q3.orig().x + 0.80000000000000004D * Q3.dest().x) - 1, yconv(0.20000000000000001D * Q3.orig().y + 0.80000000000000004D * Q3.dest().y) - 1, 3, 3);
         }
     }
     if(A != null && B != null && C != null && enabled[6])
     {
         g.setColor(Color.red);
         g.drawLine(xconv(A.x), yconv(A.y), xconv(B.x), yconv(B.y));
         g.drawString("A", xconv(A.x), yconv(A.y));
         g.setColor(Color.pink);
         g.drawLine(xconv(B.x), yconv(B.y), xconv(C.x), yconv(C.y));
         g.drawLine(xconv(C.x), yconv(C.y), xconv(A.x), yconv(A.y));
     }
     if(S != null && enabled[5])
     {
         g.setColor(Color.red);
         System.out.println((new StringBuilder("rad ")).append(rad).append(" et S ").append(S).toString());
         g.drawOval(xconv(S.x - rad), yconv(S.y - rad), dconv(2D * rad), dconv(2D * rad));
     }
 }

 public void writeImage(File file)
 {
     try
     {
         Rectangle r = getBounds();
         Image image = createImage(r.width, r.height);
         Graphics g = image.getGraphics();
         paint(g);
         ImageIO.write((RenderedImage)image, "png", file);
     }
     catch(IOException ioe)
     {
         ioe.printStackTrace();
     }
 }

 public void writePoints(File file)
 {
     String filename = file.toString();
     String ext = filename.substring(filename.lastIndexOf('.') + 1, filename.length());
     try
     {
         if(ext.equals("txt"))
         {
             FileWriter fwriter = new FileWriter(filename);
             BufferedWriter bw = new BufferedWriter(fwriter);
             for(int i = 0; i < pts.size(); i++)
             {
                 bw.write((new StringBuilder(String.valueOf(sites[i].x))).append("\t").append(sites[i].y).toString());
                 bw.newLine();
             }

             bw.close();
         } else
         {
             ObjectOutputStream obj_out = new ObjectOutputStream(new FileOutputStream(file));
             try
             {
                 obj_out.writeObject(pts);
                 obj_out.close();
             }
             catch(IOException iox)
             {
                 System.out.println("File saving error...");
                 iox.printStackTrace();
             }
         }
     }
     catch(FileNotFoundException fnf)
     {
         System.out.println("File not found...");
         fnf.printStackTrace();
     }
     catch(Exception fnf)
     {
         System.out.println("Exception...");
         fnf.printStackTrace();
     }
 }

 public void loadImage(File file)
 {
     String name = file.getAbsolutePath();
     image = Toolkit.getDefaultToolkit().getImage(name);
     loadImage = true;
     repaint();
 }

 public void loadImage(Image imp)
 {
     image = imp;
     loadImage = true;
     repaint();
 }

 public void convertImage()
 {
     Random rand = new Random();
     if(image != null)
     {
         BufferedImage bimage = toBufferedImage(image);
         int rgbs[] = (int[])null;
         int w = bimage.getWidth();
         int h = bimage.getHeight();
         System.out.println((new StringBuilder("w ")).append(w).append(" h ").append(h).toString());
         rgbs = new int[w * h];
         bimage.getRGB(0, 0, w, h, rgbs, 0, w);
         int mask = 255;
         int count = 0;
         for(int i = 0; i < h; i++)
         {
             for(int j = 0; j < w; j++)
             {
                 int rgb = bimage.getRGB(j, i);
                 int rouge = rgb >> 16 & mask;
                 int vert = rgb >> 8 & mask;
                 int bleu = rgb & mask;
                 if(rouge == 255 && bleu == 0 && vert == 0)
                 {
                     int x = (j * this.w) / w;
                     int y = (i * this.h) / h;
                     if(canAdd)
                     {
                         count++;
                         Vertex v = new Vertex((double)(x - x0) / ss + xoff + DT.jitter * rand.nextDouble(), (double)(y - y0) / ss + yoff + DT.jitter * rand.nextDouble(), pts.size());
                         pts.add(v);
                         setSites(getPoints());
                         repaint();
                     }
                 }
             }

         }

         System.out.println((new StringBuilder(String.valueOf(count))).append(" points.").toString());
     } else
     {
         JOptionPane.showMessageDialog(new JFrame(), "No loaded image. Please use Load Image", "Dialog", 0);
     }
 }

 public void loadPoints(Neighbor[] sites)
 {
     //resetAll(true);
     //clearPoints();
     pts.clear(); 
     
             for(Neighbor s: sites)  
             {
            	 if (s == null) 
            		 System.out.println("null");
            	 else System.out.println("not null");
                 pts.add(new Vertex(s.getCoord().X(), s.getCoord().Y(), pts.size()));
             }
             //setSites(getPoints());
             //show(8);
           
            // repaint(); 
        
   //  enableAdding(true);
 }

 private BufferedImage toBufferedImage(Image image)
 {
     if(image instanceof BufferedImage)
         return (BufferedImage)image;
     BufferedImage bimage = null;
     GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
     try
     {
         int transparency = 1;
         GraphicsDevice gs = ge.getDefaultScreenDevice();
         GraphicsConfiguration gc = gs.getDefaultConfiguration();
         bimage = gc.createCompatibleImage(image.getWidth(null), image.getHeight(null), transparency);
     }
     catch(HeadlessException headlessexception) { }
     if(bimage == null)
     {
         int type = 1;
         bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);
     }
     Graphics g = bimage.createGraphics();
     g.drawImage(image, 0, 0, null);
     g.dispose();
     return bimage;
 }

 Image image;
 Graphics2D graphic;
 DelaunayTriangulator dt;
 UniformGrid ug;
 AFL center;
 AFL left;
 AFL right;
 AFL tri;
 Vector triangles;
 Vector edges;
 Vertex S;
 Vertex A;
 Vertex B;
 Vertex C;
 Vertex sites[];
 QuadEdge Q;
 QuadEdge Q1;
 QuadEdge Q2;
 QuadEdge Q3;
 int frame;
 double rad;
 double med;
 int coord;
 int rank;
 int rank2;
 boolean loadImage;
 Vector pts;
 Vector ptsBin;
 boolean canAdd;
 double xoff;
 double yoff;
 double xsize;
 double ysize;
 double ss;
 double sx;
 double ssy;
 int x0;
 int y0;
 int w;
 int h;
 int ss_int;
 boolean stopped;
 int playmode;
 public static final int CONTROL_PLAY = 0;
 public static final int CONTROL_SHORTSTEP = 1;
 public static final int CONTROL_LONGSTEP = 2;
 public static final int GRID = 0;
 public static final int AFLCENTER = 1;
 public static final int AFLLEFT = 2;
 public static final int AFLRIGHT = 3;
 public static final int TRIANGULATION = 4;
 public static final int CIRCLE = 5;
 public static final int TRIANGLE = 6;
 public static final int EDGE1 = 7;
 public static final int SITES = 8;
 public static final int SPLITLINE = 9;
 public static final int POINTVEC = 10;
 public static final int QUADEDGE = 11;
 public static final int QUADEDGE3 = 12;
 public static final int ROT = 13;
 public static final int ALPHA = 14;
 public static final int ERODE = 15;
 public static final int DILATE = 16;
 public static final int CONTOUR = 17;
 public static final int BINARIZE = 18;
 public static final int DUMMY = 19;
 boolean enabled[];
}
