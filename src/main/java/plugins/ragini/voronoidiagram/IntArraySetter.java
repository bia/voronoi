package plugins.ragini.voronoidiagram;

public class IntArraySetter implements Setter<Integer[][]> {

	private Integer[][] var;
	
	@Override
	public void set(Integer[][] input) {
		var = input; 
	}

	@Override
	public Integer[][] get() {
		return var; 
	}

}
