package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   AFL.java

import java.io.PrintStream;
import java.util.Vector;

//Referenced classes of package delaunay:
//         QuadEdge

public class AFL
{

 public AFL()
 {
     list = new Vector();
 }

 public QuadEdge putTest(QuadEdge e)
 {
     int index = contains(e);
     if(index >= 0)
     {
         return (QuadEdge)list.remove(index);
     } else
     {
         list.add(e);
         return null;
     }
 }

 public void put(QuadEdge e)
 {
     list.add(e);
 }

 public void remove(QuadEdge e)
 {
     list.remove(e);
 }

 public int contains(QuadEdge e1)
 {
     int index;
     for(index = 0; index < list.size(); index++)
     {
         QuadEdge e2 = get(index);
         if(e1.equals(e2) || e1.equals(e2.sym()))
             break;
     }

     if(index < list.size())
         return index;
     else
         return -1;
 }

 public boolean isEmpty()
 {
     return list.isEmpty();
 }

 public QuadEdge extract()
 {
     return (QuadEdge)list.remove(0);
 }

 public int size()
 {
     return list.size();
 }

 QuadEdge get(int index)
 {
     return (QuadEdge)list.elementAt(index);
 }

 public void print()
 {
     for(int i = 0; i < list.size(); i++)
         System.out.println((new StringBuilder("[")).append(get(i).orig()).append(",").append(get(i).dest()).append("]").toString());

 }

 private Vector list;
}
