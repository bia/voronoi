package plugins.ragini.voronoidiagram;

public class ArraySetter implements Setter<NVert []> {

	private NVert [] var; 
	
	@Override
	public void set(NVert [] input) {
		var = input; 
	}

	@Override
	public NVert [] get() {
		return var; 
	}

}
