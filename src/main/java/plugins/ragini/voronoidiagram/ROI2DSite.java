package plugins.ragini.voronoidiagram;
import java.awt.geom.Point2D;

import icy.roi.ROI2DPoint;

public class ROI2DSite extends ROI2DPoint {
	
	private Site site; 
	
	public ROI2DSite(Point2D dp) {
		super(dp); 
	}

	public void setSite(Site s) {
		site = s;
	}
	
	public Site getSite() {
		return site; 
	}
	
}
