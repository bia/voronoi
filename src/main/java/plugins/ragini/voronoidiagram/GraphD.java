package plugins.ragini.voronoidiagram;

public class GraphD {

	private Point pt;
	private Arcs arcs;
	private short nbarcs; 
	
	public void setArcs(Arcs a) {
		arcs = a;
	}
	
	public void setNbArcs(short s) {
		nbarcs = s; 
	}
	
	public Point pt() {
		return pt; 
	}
	
	public Arcs arcs() {
		return arcs; 
	}
	
	public short nbarcs() {
		return nbarcs; 
	}
	
}
