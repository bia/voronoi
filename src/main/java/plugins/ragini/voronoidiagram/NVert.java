package plugins.ragini.voronoidiagram;

public class NVert {

	private int nbr1, nbr2, onhull, vpid; 
	
	public int onhull() {
		return onhull; 
	}
	
	public void setOnHull(int o) {
		onhull = o; 
	}
	
	public int nbr1() {
		return nbr1;
	}
	
	public int nbr2() {
		return nbr2; 
	}
	
	public void setNbr1(int n) {
		nbr1 = n;
	}
	
	public void setNbr2(int n) {
		nbr2 = n;
	}
	
	public void setVPid(int v) {
		vpid = v; 
	}
	
	public int vpid() {
		return vpid;
	}
	
}
