package plugins.ragini.voronoidiagram;

public class IntSetter implements Setter<Integer> {

	Integer n; 
	
	public IntSetter (int i) {
		n = new Integer(i); 
	}
	
	@Override
	public void set(Integer input) {
		n = input; 
	}

	@Override
	public Integer get() {
		return n; 
	}

}
