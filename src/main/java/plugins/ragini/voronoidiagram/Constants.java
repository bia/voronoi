package plugins.ragini.voronoidiagram;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class Constants {
	
	private int maxSites;
	private int le;
	private int re; 
	private int maxVerts;
	private int maxEdges;
	private int maxTris; 
	private int triangulate; 
	private int rng; 
	private int vrdebug; 
	
	private float vxmin, vxmax, vymin, vymax, vdeltax, vdeltay;

	private Vert[] GLsites, USites, verts;
	private EdgeS[] vedges;
	private Tri[] tris;
	private Tri[] dtris; 
	
	private NVert[] chverts, uchverts;
	private Neighbor [] _sites;
	private int nsites;
	private Point2D [][] lines; 
	private int lineindex; 
	
	private int lbnd;       
	private int bbnd;
	private int rbnd;
	private int tbnd;
	private int PQhashsize;
	private int nedges;
	
	private Site bottomsite;
	private double sqrt_nsites; 
	
	private int edgex;
	private int edgey; 
	private int maxbinx;
	private int maxbiny; 
	
	private ArrayList<EdgeRNG> relativeneighbors; 
	
	public Constants() {
		maxSites = 2*4096; 
		le = 0; 
		re = 1; 
		maxVerts = 3*maxSites;
		maxEdges = 2*maxSites;
		maxTris = maxEdges; 
		verts = new Vert[maxVerts]; 
		vedges = new EdgeS[maxEdges];
		tris = new Tri[maxTris];
		chverts = new NVert[maxVerts];
		uchverts = new NVert[maxVerts]; 
		lbnd = 0;
		bbnd = 1;
		rbnd = 2;
		tbnd = 3; 
		triangulate = 0;
		rng = 0; 
		vrdebug = 0;  
		lineindex = 0; 
	}

	public Vert[] getGLSites() {
		//System.out.println("retrieving GLsites");
		return GLsites; 
	}
	
	public int le() {
		//System.out.println("retrieving le"); 
		return le; 
	}
	
	public int re() {
		//System.out.println("retrieving re"); 
		return re; 
	}
	
	public int nsites() {
		//System.out.println("retrieving nsites"); 
		return nsites; 
	}
	
	public void setNSites(int n) {
		//System.out.println("setting nsites"); 
		nsites = n; 
		lines = new Point2D[n*3][2]; 
	}
	
	public NVert[] chverts() {
		//System.out.println("retrieving chverts"); 
		return chverts; 
	}
	
	public int triangulate() {
		//System.out.println("retrieving triangulate"); 
		return triangulate; 
	}
	
	public EdgeS[] vedges() {
		//System.out.println("retrieving vedges"); 
		return vedges; 
	}
	
	public int vrdebug() {
		//System.out.println("retrieving vrdebug"); 
		return vrdebug; 
	}
	
	public int maxEdges() {
		//System.out.println("retrieving maxedges"); 
		return maxEdges; 
	}
	
	public Vert[] verts() {
		//System.out.println("retrieving verts"); 
		return verts; 
	}
	
	public int maxVerts() {
		//System.out.println("retrieving maxverts"); 
		return maxVerts; 
	}
	
	public int maxTris() {
		//System.out.println("retrieving maxtris"); 
		return maxTris; 
	}
	
	public Tri[] tris() {
		//System.out.println("retrieving tris"); 
		return tris; 
	}
	
	public Tri[] dtris() {
		//System.out.println("retrieving dtris"); 
		return dtris; 
	}
	
	public void setdtris(Tri [] _dtris) {
		//System.out.println("setting dtris"); 
		dtris = _dtris; 
	}
	
	public Neighbor[] sites() {
		//System.out.println("retrieving sites"); 
		return _sites; 
	}
	
	public int maxSites() {
		//System.out.println("retrieving maxsites"); 
		return maxSites;
	}
	
	public Vert[] USites() {
		//System.out.println("retrieving usites"); 
		return USites; 
	}
	
	public NVert[] uchverts() {
		//System.out.println("retrieving uchverts"); 
		return uchverts; 
	}
	
	public void setTriangulate(int n) {
		//System.out.println("setting triangulate"); 
		triangulate = n; 
	}
	
	public int lbnd() {
		//System.out.println("retrieving lbnd"); 
		return lbnd;
	}
	
	public int bbnd() {
		//System.out.println("retrieving bbnd"); 
		return bbnd; 
	}
	
	public int rbnd() {
		//System.out.println("retrieving rbnd"); 
		return rbnd; 
	}
	
	public int tbnd() {
		//System.out.println("retrieving tbnd"); 
		return tbnd; 
	}
	
	public void setvxmin(float m) {
		//System.out.println("setting vxmin"); 
		vxmin = m;
	}
	
	public void setvxmax(float m) {
		//System.out.println("setting vxmax"); 
		vxmax = m;
	}
	
	public void setvymin(float m) {
		//System.out.println("setting vymin"); 
		vymin = m;
	}
	
	public void setvymax(float m) {
		//System.out.println("setting vymax"); 
		vymax = m; 
	}
	
	public float vxmin() {
		//System.out.println("retrieving vxmin"); 
		return vxmin;
	}
	
	public float vxmax() {
		//System.out.println("retrieving vxmax"); 
		return vxmax;
	}
	
	public float vymin() {
		//System.out.println("retrieving vymin"); 
		return vymin;
	}
	
	public float vymax() {
		//System.out.println("retrieving vymax"); 
		return vymax; 
	}
	
	public float vdeltax() {
		//System.out.println("retrieving vdeltax"); 
		return vdeltax; 
	}
	
	public void setBottomSite(Site s) {
		//System.out.println("setting bottomsite"); 
		bottomsite = s; 
	}
	
	public Site bottomsite() {
		//System.out.println("retrieving bottomsite"); 
		return bottomsite; 
	}
	
	public void setTris(Tri[] tri) {
		//System.out.println("setting tris"); 
		tris = tri; 
	}
	
	public void setvdeltax(float d) {
		//System.out.println("setting vdeltax"); 
		vdeltax = d;
	}
	
	public void setvdeltay(float d) {
		//System.out.println("setting vdeltay"); 
		vdeltay = d; 
	}
	
	public int nedges() {
		//System.out.println("retrieving nedges"); 
		return nedges;
	}
	
	public void setnedges(int n) {
		//System.out.println("setting nedges"); 
		nedges = n; 
	}
	
	public int PQhashsize() {
		//System.out.println("retrieving pqhashsize"); 
		return PQhashsize; 
	}
	
	public float vdeltay() {
		//System.out.println("retrieving vdeltay"); 
		return vdeltay; 
	}
	
	public void setsqrtnsites(double n) {
		//System.out.println("setting sqrtnsites"); 
		sqrt_nsites = n; 
	}
	
	public double sqrtnsites() {
		//System.out.println("retrieving sqrtnsites"); 
		return sqrt_nsites; 
	}
	
	public void setPQhashsize(int n) {
		PQhashsize = n; 
	}
	
	public void setchverts (NVert[] varray) {
		chverts = varray; 
	}
	
	public void setUSites(Vert[] sites) {
		USites = sites; 
	}
	
	public void setGLsites(Vert[] sites) {
		GLsites = sites; 
	}
	
	public void setSites(Neighbor[] sites){
		_sites = sites; 
	}
	
	public Point2D[][] lines() {
		return lines; 
	}
	
	public void setlineindex(int n) {
		lineindex = n; 
	}
	
	public int lineindex() {
		return lineindex; 
	}
	
	public int rng() {
		return rng; 
	}
	
	public void setrng(int r) {
		rng = r; 
	}
	
	public int getedgex() {
		return edgex; 
	}
	
	public int getedgey() {
		return edgey; 
	}
	
	public void setedgex(int n) {
		edgex = n;
	}
	
	public void setedgey(int n) {
		edgey = n; 
	}
	
	public int maxbinx() {
		return maxbinx;
	}
	
	public int maxbiny() {
		return maxbiny; 
	}
	
	public void setmaxbinx(int b) {
		maxbinx = b; 
	}
	
	public void setmaxbiny(int b) {
		maxbiny = b; 
	}
	
	public ArrayList<EdgeRNG> relativeneighbors() {
		return relativeneighbors; 
	}
	
	public void setrelativeneighbors(ArrayList<EdgeRNG> l) {
		relativeneighbors = l; 
	}
}


