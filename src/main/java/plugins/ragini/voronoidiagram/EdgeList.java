package plugins.ragini.voronoidiagram;

public class EdgeList {
	
	private HalfEdge ELleftend, ELrightend;
	private int ELhashsize;
	private HalfEdge[] ELhash;
	
	private Constants constants; 
	private VRegion vregion; 
	
	public EdgeList(Constants _constants, VRegion reg) {
		constants = _constants; 
		vregion = reg; 
	}
	
	public HalfEdge leftend() {
		return ELleftend;
	}
	
	public HalfEdge rightend() {
		return ELrightend; 
	}
	
	public void ELinitialize() {
		//System.out.println("initializing edge list"); 
		int i; 
		ELhashsize = (int) (2 * constants.sqrtnsites()); 
		ELhash = new HalfEdge[ELhashsize]; 
		//System.out.println("hash size: " + ELhashsize); 
		for (i = 0; i < ELhashsize; i ++) {
			ELhash[i] = new HalfEdge(-1); 
		}
		
		ELleftend = HEcreate(null, 0, -1);
		ELrightend = HEcreate(null, 0, -1);
		ELleftend.setLeft(null);
		ELleftend.setRight(ELrightend);
		ELrightend.setLeft(ELleftend);
		ELrightend.setRight(null); 
		ELhash[0] = ELleftend;
		ELhash[ELhashsize - 1] = ELrightend; 
	}
	
	public HalfEdge HEcreate(Edge e, int pm, int origin) {
		//System.out.println("creating new halfedge"); 
		HalfEdge answer;
		
		answer = new HalfEdge(origin); 
		answer.setEdge(e);
		answer.setPM(pm); 
		answer.setNext(null);
		answer.setVertex(null); 
		answer.setNull(false);  
		return answer; 
	}
	
	public void ELinsert(HalfEdge lb, HalfEdge newhe) {
		//if(lb == null) System.out.println("lb null"); 
		//else if (newhe == null) System.out.println("newhe null"); 
		//else if (lb.eledge() == null) {System.out.println("lb edge null"); System.out.println("newhe: " + newhe.eledge().a() + ", " + newhe.eledge().b() + ", " + newhe.eledge().c()); }
		//else System.out.println("inserting: " + newhe.eledge().a() + ", " + newhe.eledge().b() + ", " + newhe.eledge().c() + ", " + "after " + lb.eledge().a() + ", " + lb.eledge().b() + ", " + lb.eledge().c()); 
		//System.out.println("inserting halfedge"); 
		newhe.setLeft(lb);
		newhe.setRight(lb.right());
		//if (lb.right().eledge() == null) System.out.println("the right of lb is null"); 
		//else System.out.println(newhe.right().eledge().a() + ", " + newhe.right().eledge().b() + ", " + newhe.right().eledge().c() + " is to the right of: " + newhe.eledge().a() + ", " + newhe.eledge().b() + ", " + newhe.eledge().c());
		lb.right().setLeft(newhe);
		lb.setRight(newhe); 
		if (lb == ELrightend) {ELrightend = newhe;}
	}
	
	public HalfEdge ELgethash(int b) {
		HalfEdge he; 
		
		if(b < 0 || b >= ELhashsize) {
			return null;
		}
		
		he = ELhash[b]; 
		//System.out.println("trying bucket: " + b); 
		//if (he == null) System.out.println("null at bucket: " + b); 
		if (he.isNull() || !he.isDeleted()) { 
			return he; 
		}
		/* Hash table points to deleted half edge.  Patch as necessary. */
		ELhash[b] = new HalfEdge(-1); 
		//some memory stuff, may return null 
		return null; 
	}
	
	public HalfEdge ELleftbnd(Point p) { 
		 int i, bucket = 0;
		 HalfEdge he; 
		 
		 bucket = (int) ((p.X() - constants.vxmin())/constants.vdeltax() * ELhashsize); 
		 //System.out.println("bucket: " + bucket); 
		 if (bucket < 0)
			 bucket = 0;
		 
		 if (bucket >= ELhashsize) 
			 bucket = ELhashsize -1; 
		 
		 he = ELgethash(bucket); 
		 if (he.isNull()) {
			 for (i = 1; true; i ++) {
				 //fix this for loop 
				 if (ELgethash(bucket - i) != null) {
				 if (!(he = ELgethash(bucket - i)).isNull()) {
					 break;
				 }}
				 if (ELgethash(bucket + i) != null) {
				 if (!(he = ELgethash(bucket + 1)).isNull()) {
					 break; 
				 }}
			 }
			 
		 }
		 
		 if (he == ELleftend) {
			 he = he.right(); 
			 while (he != ELrightend && right_of(he, p)) {
				 he = he.right(); 
			 }
			 he = he.left(); 
		 } 
		 
		 else if (he != ELrightend ) {
			 if (right_of(he, p)) {
				 he = he.right(); 
			 while (he != ELrightend && right_of(he, p)) {
				 he = he.right(); 
			 }
			 he = he.left(); }
		 }
		 
		 else {
			 he = he.left(); 
			 while (he != ELleftend && !right_of(he,p)) {
				 he = he.left(); 
			 }
		 }
		 
		 if (bucket > 0 && bucket < ELhashsize - 1) {
			 if (!ELhash[bucket].isNull())
				 //System.out.println("valid bucket"); 
			 ELhash[bucket] = he;
		 }
		 return he; 
		 
	}
	
	public boolean right_of(HalfEdge el, Point p) {
		//returns 1 if p is to the right of halfedge e
		Edge e;
		Site topsite;
		int right_of_site; 
		boolean above, fast;
		float dxp, dyp, dxs, t1, t2, t3, y1; 
		
		e = el.eledge();
		topsite = e.reg()[1];
		right_of_site = (p.X() > topsite.getCoord().X()) ? 1: 0; 
		if (right_of_site == 1 && el.elpm() == constants.le())
			return true;
		else if (right_of_site == 0 && el.elpm() == constants.re())
			return false;
		
		if (e.a() == 1) {
			dyp = p.Y() - topsite.getCoord().Y();
			dxp = p.X() - topsite.getCoord().X();
			fast = false;
			if ((right_of_site == 0 & e.b()< 0) | (right_of_site == 1 & e.b()>=0)) {
				above = (dyp >= e.b()*dxp); 
				fast = above; 
			}
			else {
				above = (p.X() + p.Y()*e.b() > e.c()); 
				if (e.b() < 0) 
					above = !above; 
				if (!above) 
					fast = true; 
    			}
			if (!fast) {
				dxs = topsite.getCoord().X() - (e.reg()[0]).getCoord().X();
				above = ((e.b() * (dxp * dxp - dyp * dyp)) < (dxs * dyp * (1 + 2*dxp/dxs + e.b()*e.b()))); 
				if (e.b() < 0) 
					above = !above;
			}
		}
		else { //e.b() == 1
			y1 = e.c() - e.a()*p.X();
			t1 = p.Y() - y1; 
			t2 = p.X() - topsite.getCoord().X();
			t3 = y1 - topsite.getCoord().Y();
			above = (t1*t1 > (t2*t2 + t3*t3)); 
		}
		return (el.elpm() == constants.le() ? above : !above);
	}

	public void ELdelete(HalfEdge he) {
		he.left().setRight(he.right()); 
		he.right().setLeft(he.left()); 
		he.setEdge(null); 
		he.setDeleted(true); 
	}
	
	public HalfEdge ELright(HalfEdge he) {
		return he.right();
	}
	
	public HalfEdge ELleft(HalfEdge he) {
		return he.left(); 
	}
	
	public Neighbor leftReg(HalfEdge he) {
		if (he.eledge() == null)
			return (Neighbor) constants.bottomsite(); 
		return (Neighbor) (he.elpm() == constants.le() ? he.eledge().reg()[constants.le()] : he.eledge().reg()[constants.re()]); 
	}
	
	public Neighbor rightReg(HalfEdge he) {
		if (he.eledge() == null) 
			return (Neighbor) constants.bottomsite();
		return (Neighbor) (he.elpm() == constants.le() ? he.eledge().reg()[constants.re()] : he.eledge().reg()[constants.le()]); 
	}
	
	public Edge bisect(Site s1, Site s2) {
		float dx, dy, adx, ady;
		Edge newedge; 
		//System.out.println("bisecting:"); 
		//System.out.println("s1: " + s1.getCoord().X() + " " + s1.getCoord().Y());
		//System.out.println("s2: " + s2.getCoord().X() + " " + s2.getCoord().Y()); 
		
		newedge = new Edge(); 
		newedge.reg()[0] = s1;
		newedge.reg()[1] = s2; 
		newedge.ep()[0] = null;
		newedge.ep()[1] = null; 
		
		dx = s2.getCoord().X() - s1.getCoord().X(); //System.out.println("dx: " + dx);
		dy = s2.getCoord().Y() - s1.getCoord().Y(); //System.out.println("dy: " + dy); 
		adx = dx > 0 ? dx : -dx; 
		ady = dy > 0 ? dy : -dy; 
		
		newedge.setc(s1.getCoord().X() * dx + s1.getCoord().Y() * dy +(dx*dx + dy*dy)*0.5); 
		//newedge.setc((dx*dx + dy*dy)*0.5); 
		if (adx > ady) {
			newedge.seta(1); //System.out.println("a: " + newedge.a()); 
			newedge.setb(dy/dx); //System.out.println("b: " + newedge.b());
			if (dx == 0) System.out.println("dividing by zero"); 
			newedge.setc(newedge.c()/dx); //System.out.println("c: " + newedge.c()); 
		}
		else {
			newedge.setb(1); //System.out.println("b: " + newedge.b()); 
			newedge.seta(dx/dy); //System.out.println("a: " + newedge.a()); 
			if (dy == 0) System.out.println("dividing by zero"); 
			newedge.setc(newedge.c()/dy); //System.out.println("c: " + newedge.c()); 
		}
		newedge.setedgenbr(constants.nedges()); 
		vregion.outBisector(newedge);
		constants.setnedges(constants.nedges() +1); 
		return newedge; 
	}

	public void v_endpoint(Edge e, int origin, int lr, Site s) {
		e.ep()[lr] = s; 
		if (e.ep()[constants.re() - lr] == null) 
			return;
		vregion.outEP(e, origin); 
	}

	public Site sintersect(HalfEdge el1, HalfEdge el2) {	
		Edge e1, e2, e;
		HalfEdge el;
		
		float d, xint, yint;
		int right_of_site;
		Neighbor v; 
		
		e1 = el1.eledge();
		e2 = el2.eledge(); 
		
		if (e1 == null || e2 == null)
			return null;
		
		if (e1.reg()[1] == e2.reg()[1]) 
			return null; 
		
		d = e1.a() * e2.b() - e1.b()*e2.a(); 
		if (d==0) return null; 
		if ((-1 * (10 ^ -10)) < d && d < (10 ^ 10))
			return null; 
		
		xint = (e1.c() * e2.b() - e2.c() * e1.b())/d;
		yint = (e2.c() * e1.a() - e1.c() * e2.a())/d; 
		
		if ((e1.reg()[1].getCoord().Y() < e2.reg()[1].getCoord().Y()) || (e1.reg()[1].getCoord().Y() == e2.reg()[1].getCoord().Y() && e1.reg()[1].getCoord().X() < e2.reg()[1].getCoord().X())) {
			el = el1; 
			e = e1; 
		}
		else {
			el = el2;
			e = e2; 
		}
		right_of_site = (xint >= e.reg()[1].getCoord().X()) ? 1 : 0; 
		if ((right_of_site == 1 && el.elpm() == constants.le()) || right_of_site == 0 && el.elpm() == constants.re())
			return null; 
		
		v = new Neighbor(); 
		v.setRefCnt(0);
		v.setCoord(new Point()); 
		v.getCoord().setX(xint);
		v.getCoord().setY(yint); 
		return v; 
	}

}
