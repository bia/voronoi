package plugins.ragini.voronoidiagram;

public class Tri {

	private Vert v1, v2, v3; 
	
	public void setV1(Vert v) {
		v1 = v;
	}
	
	public void setV2(Vert v) {
		v2 = v;
	}
	
	public void setV3(Vert v) {
		v3 = v; 
	}
	
	public Vert v1() {
		return v1;
	}
	
	public Vert v2() {
		return v2;
	}
	
	public Vert v3() {
		return v3; 
	}
	
}
