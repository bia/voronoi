package plugins.ragini.voronoidiagram;

public class Geometry {
	
	private int PQcount, PQmin; 
	private HalfEdge[] PQhash; 
	private Constants constants; 
	
	public Geometry(Constants _constants) {
		constants = _constants;
	}
	
	public void PQinsert(HalfEdge he, Site v, double d) {
		
		 
		
		HalfEdge last, next; 
		
		he.setVertex(v);
		he.setystar((float) (v.getCoord().Y() + d)); 
		last = PQhash[PQbucket(he)]; 
		while((next = last.PQnext()) != null && (he.ystar()>next.ystar() || (he.ystar() == next.ystar() && v.getCoord().X() > next.vertex().getCoord().X()))) {
			last = next; 
		}
		he.setPQnext(last.PQnext());
		last.setPQnext(he);
		PQcount +=1; 
		//System.out.println("inserting halfedge: " + he.vertex().getCoord().X() + ", " + he.ystar());
	}
	
	public void PQdelete(HalfEdge he) {
		HalfEdge last;
		if (he.vertex() != null && he.vertex().getCoord() != null) 
		//System.out.println("removing halfedge: " + he.vertex().getCoord().X() + ", " + he.ystar()); 
		if (he.vertex() != null) {
			last = PQhash[PQbucket(he)];                                                                                                             
			while (last.PQnext() != he) {
				last = last.PQnext(); 
			}
			last.setPQnext(he.PQnext());
			PQcount -=1; 
			he.setVertex(null); 
		}
	}
	
	public int PQbucket(HalfEdge he) {
		int bucket;
		
		bucket = (int) ((he.ystar() - constants.vymin())/constants.vdeltay() * constants.PQhashsize()); ///////////will this cast work? 
		if (bucket < 0) bucket = 0;
		if (bucket >= constants.PQhashsize()) {
			bucket = constants.PQhashsize() - 1; 
		}
		if (bucket < PQmin) {
			PQmin = bucket;
		}
		return bucket; 
	}
	
	public boolean PQempty() {
		return (PQcount == 0); 
	}
	
	public Point PQmin() {
		Point answer = new Point();
		
		while(PQhash[PQmin].PQnext() == null) {
			PQmin++; 
		}
		answer.setX(PQhash[PQmin].PQnext().vertex().getCoord().X()); 
		answer.setY(PQhash[PQmin].PQnext().ystar()); 
		return answer; 
	}
	
	public HalfEdge PQextractmin() {
		HalfEdge curr; 
		
		curr = PQhash[PQmin].PQnext();
		PQhash[PQmin].setPQnext(curr.PQnext()); 
		PQcount--; 
		return curr; 
	}
	
	public void PQinitialize() {
		int i;
		Point s; 
		
		PQcount = 0;
		PQmin = 0;
		constants.setPQhashsize((int) (4 * constants.sqrtnsites())); 
		PQhash = new HalfEdge[constants.PQhashsize()]; 
		for (i = 0; i < constants.PQhashsize(); i ++) {
			PQhash[i] = new HalfEdge(-1); 
			PQhash[i].setPQnext(null); 
		}
	}
	
}
