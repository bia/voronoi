package plugins.ragini.voronoidiagram;

public class VertTheta {
	
	private float x, y, theta;
	private int e1, e2; 

	public VertTheta(float _x, float _y) {
		x = _x;
		y = _y; 
	}
	
	public float theta() {
		return theta; 
	}
	
	public void setTheta(float t) {
		theta = t; 
	}
	
	public float Y() {
		return y;
	}

	public float X() {
		return x; 
	}
	
	public int e1() {
		return e1;
	}
	
	public int e2() {
		return e2; 
	}
	
	public void setE1(int e) {
		e1 = e;
	}
	
	public void setE2(int e) {
		e2 = e; 
	}
	
	public void setX(float _x) {
		x = _x;
	}
	
	public void setY(float _y) {
		y = _y; 
	}
	
}
