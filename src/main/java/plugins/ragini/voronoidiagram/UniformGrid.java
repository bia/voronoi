package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   UniformGrid.java

import java.util.Iterator;
import java.util.NoSuchElementException;

//Referenced classes of package delaunay:
//         Vertex, DT, QuadEdge

public class UniformGrid
{
 class GridCell
 {

     PointItem points;
     int mark;
     final UniformGrid this$0;

     GridCell()
     {
         super();
         this$0 = UniformGrid.this;
         points = null;
         mark = -1;
     }
 }

 class GridInterval
 {

     void set()
     {
         x1 = y1 = 0;
         x2 = xsize;
         y2 = ysize;
     }

     void set(int X1, int Y1, int X2, int Y2)
     {
         x1 = X1 < 0 ? 0 : X1;
         y1 = Y1 < 0 ? 0 : Y1;
         x2 = X2 > xsize ? xsize : X2;
         y2 = Y2 > ysize ? ysize : Y2;
     }

     void set(Vertex v, double radius)
     {
         int X1 = (int)((v.x - radius - xoffset) / cellsize);
         int Y1 = (int)((v.y - radius - yoffset) / cellsize);
         int X2 = (int)(((v.x + radius) - xoffset) / cellsize) + 1;
         int Y2 = (int)(((v.y + radius) - yoffset) / cellsize) + 1;
         set(X1, Y1, X2, Y2);
     }

     public String toString()
     {
         if(x1 < x2 && y1 < y2)
             return (new StringBuilder("[")).append(x1).append("..").append(x2 - 1).append("][").append(y1).append("..").append(y2 - 1).append("]").toString();
         else
             return "[empty]";
     }

     int x1;
     int y1;
     int x2;
     int y2;
     final UniformGrid this$0;

     GridInterval(int X1, int Y1, int X2, int Y2)
     {
         super();
         this$0 = UniformGrid.this;
         set(X1, Y1, X2, Y2);
     }

     GridInterval(Vertex v, double radius)
     {
         super();
         this$0 = UniformGrid.this;
         set(v, radius);
     }
 }

 class GridPosition
 {

     int x;
     int y;
     final UniformGrid this$0;

     GridPosition(Vertex v)
     {
         super();
         this$0 = UniformGrid.this;
         x = (int)((v.x - xoffset) / cellsize);
         y = (int)((v.y - yoffset) / cellsize);
         if(x < 0)
             x = 0;
         if(x >= xsize)
             x = xsize - 1;
         if(y < 0)
             y = 0;
         if(y >= ysize)
             y = ysize - 1;
     }
 }

 class PointItem
 {

     Vertex point;
     PointItem next;
     final UniformGrid this$0;

     PointItem()
     {
    	 super();
         this$0 = UniformGrid.this;
         point = null;
         next = null;
     }
 }

 class PointIterator
     implements Iterator
 {

     public boolean hasNext()
     {
         return ptr != null;
     }

     public Object next()
     {
         if(ptr == null)
         {
             throw new NoSuchElementException();
         } else
         {
             Vertex res = ptr.point;
             ptr = ptr.next;
             return res;
         }
     }

     public void remove()
     {
         throw new UnsupportedOperationException();
     }

     PointItem ptr;
     final UniformGrid this$0;

     PointIterator(int x, int y)
     {
    	 super();
         this$0 = UniformGrid.this;
         ptr = null;
         ptr = grid[x][y].points;
     }
 }


 UniformGrid(Vertex verts[], int mincells)
 {
     grid = null;
     array = null;
     xsize = 0;
     ysize = 0;
     xoffset = 0.0D;
     yoffset = 0.0D;
     cellsize = 0.0D;
     currentMark = 0;
     array = new PointItem[verts.length];
     for(int i = 0; i < array.length; i++)
         array[i] = new PointItem();

     double xmax;
     double xmin = xmax = verts[0].x;
     double ymax;
     double ymin = ymax = verts[0].y;
     for(int i = 1; i < verts.length; i++)
     {
         if(verts[i].x < xmin)
             xmin = verts[i].x;
         if(verts[i].x > xmax)
             xmax = verts[i].x;
         if(verts[i].y < ymin)
             ymin = verts[i].y;
         if(verts[i].y > ymax)
             ymax = verts[i].y;
     }

     cellsize = Math.sqrt(((xmax - xmin) * (ymax - ymin)) / (double)mincells);
     xsize = (int)Math.ceil((xmax - xmin) / cellsize);
     ysize = (int)Math.ceil((ymax - ymin) / cellsize);
     xoffset = xmin - (cellsize * (double)xsize - (xmax - xmin)) / 2D;
     yoffset = ymin - (cellsize * (double)ysize - (ymax - ymin)) / 2D;
     grid = new GridCell[xsize][ysize];
     for(int i = 0; i < xsize; i++)
     {
         for(int j = 0; j < ysize; j++)
             grid[i][j] = new GridCell();

     }

     for(int i = 0; i < verts.length; i++)
     {
         int x = (int)((verts[i].x - xoffset) / cellsize);
         int y = (int)((verts[i].y - yoffset) / cellsize);
         if(x < 0)
             x = 0;
         if(x >= xsize)
             x = xsize - 1;
         if(y < 0)
             y = 0;
         if(y >= ysize)
             y = ysize - 1;
         array[i].point = verts[i];
         array[i].next = null;
         if(grid[x][y].points == null)
         {
             grid[x][y].points = array[i];
         } else
         {
             PointItem last;
             for(last = grid[x][y].points; last.next != null; last = last.next);
             last.next = array[i];
         }
     }

 }

 public int XCells()
 {
     return xsize;
 }

 public int YCells()
 {
     return ysize;
 }

 public double XMin()
 {
     return xoffset;
 }

 public double YMin()
 {
     return yoffset;
 }

 public double XMax()
 {
     return xoffset + (double)xsize * cellsize;
 }

 public double YMax()
 {
     return yoffset + (double)ysize * cellsize;
 }

 public double XSize()
 {
     return (double)xsize * cellsize;
 }

 public double YSize()
 {
     return (double)ysize * cellsize;
 }

 public double CellSize()
 {
     return cellsize;
 }

 public PointIterator cellIterator(int x, int y)
 {
     return new PointIterator(x, y);
 }

 void resetAllMarks()
 {
     currentMark++;
 }

 void setMark(int x, int y)
 {
     grid[x][y].mark = currentMark;
 }

 void unsetMark(int x, int y)
 {
     grid[x][y].mark = currentMark - 1;
 }

 boolean isMarked(int x, int y)
 {
     return grid[x][y].mark == currentMark;
 }

 public int numPoints(int x1, int y1, int x2, int y2)
 {
     int cnt = 0;
     for(int i = x1; i < x2; i++)
     {
         for(int j = y1; j < y2; j++)
         {
             for(PointIterator pi = cellIterator(i, j); pi.hasNext();)
             {
                 pi.next();
                 cnt++;
             }

         }

     }

     return cnt;
 }

 public double pseudoMedian(int axis)
 {
     int cfirst = 0;
     int clast = 0;
     int cnt = 0;
     double med = 0.0D;
     switch(axis)
     {
     default:
         break;

     case 0: // '\0'
     {
         int pfirst = 0;
         for(int plast = xsize - 1; pfirst < plast;)
             if(cfirst <= clast)
             {
                 cfirst += numPoints(pfirst, 0, pfirst + 1, ysize);
                 pfirst++;
             } else
             {
                 clast += numPoints(plast, 0, plast + 1, ysize);
                 plast--;
             }

         for(int i = 0; i < ysize; i++)
         {
             for(PointIterator pi = cellIterator(pfirst, i); pi.hasNext();)
             {
                 med += ((Vertex)pi.next()).x;
                 cnt++;
             }

         }

         if(cnt > 0)
             med /= cnt;
         else
             med = (double)pfirst * cellsize + xoffset;
         break;
     }

     case 1: // '\001'
     {
         int pfirst = 0;
         for(int plast = ysize - 1; pfirst < plast;)
             if(cfirst <= clast)
             {
                 cfirst += numPoints(0, pfirst, xsize, pfirst + 1);
                 pfirst++;
             } else
             {
                 clast += numPoints(0, plast, xsize, plast + 1);
                 plast--;
             }

         for(int i = 0; i < xsize; i++)
         {
             for(PointIterator pi = cellIterator(i, pfirst); pi.hasNext();)
             {
                 med += ((Vertex)pi.next()).y;
                 cnt++;
             }

         }

         if(cnt > 0)
             med /= cnt;
         else
             med = (double)pfirst * cellsize + yoffset;
         break;
     }
     }
     return med;
 }

 public Vertex findNearestPointLinear(Vertex v)
 {
     double dist = 0.0D;
     Vertex found = null;
     for(int i = 0; i < array.length; i++)
         if(array[i].point != v)
         {
             double pom = Vertex.sqrDistance(v, array[i].point);
             if(found == null || pom < dist)
             {
                 found = array[i].point;
                 dist = pom;
             }
         }

     return found;
 }

 public Vertex findDelaunayPointLinear(Vertex a, Vertex b)
 {
     double dist = 0.0D;
     Vertex found = null;
     for(int i = 0; i < array.length; i++)
         if(array[i].point != a && array[i].point != b && Vertex.ccw(a, b, array[i].point))
         {
             double pom = Vertex.delaunayDistance(a, b, array[i].point);
             if(found == null || pom < dist)
             {
                 found = array[i].point;
                 dist = pom;
             }
         }

     return found;
 }

 public Vertex findFirst(Vertex v)
 {
     resetAllMarks();
     Vertex found = null;
     GridPosition gp = new GridPosition(v);
     GridInterval gr = new GridInterval(gp.x, gp.y, gp.x + 1, gp.y + 1);
label0:
     while(found == null) 
     {
         for(int i = gr.x1; i < gr.x2; i++)
         {
             for(int j = gr.y1; j < gr.y2; j++)
             {
                 if(isMarked(i, j))
                     continue;
                 for(PointIterator pi = cellIterator(i, j); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     if(pom != v)
                     {
                         found = pom;
                         break label0;
                     }
                 }

                 setMark(i, j);
             }

         }

         if(gr.x1 == 0 && gr.y1 == 0 && gr.x2 == xsize && gr.y2 == ysize)
             break;
         gr.set(gr.x1 - 1, gr.y1 - 1, gr.x2 + 1, gr.y2 + 1);
     }
     return found;
 }

 public Vertex findNearestPoint(Vertex v)
 {
     Vertex found = findFirst(v);
     if(found == null)
         return null;
     double dist = Vertex.distance(v, found);
     GridInterval gr = new GridInterval(v, dist);
     for(int i = gr.x1; i < gr.x2; i++)
     {
         for(int j = gr.y1; j < gr.y2; j++)
             if(!isMarked(i, j))
             {
                 for(PointIterator pi = cellIterator(i, j); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     if(pom != v)
                     {
                         double dd = Vertex.distance(pom, v);
                         if(dd < dist)
                         {
                             dist = dd;
                             found = pom;
                             gr.set(v, dist);
                         }
                     }
                 }

                 setMark(i, j);
             }

     }

     return found;
 }

 public Vertex findDelaunayPoint(Vertex a, Vertex b)
 {
     resetAllMarks();
     Vertex found = null;
     GridPosition gp = new GridPosition(new Vertex((a.x + b.x) / 2D, (a.y + b.y) / 2D));
     GridInterval gr = new GridInterval(gp.x, gp.y, gp.x + 1, gp.y + 1);
     Vertex S;
label0:
     while(found == null) 
     {
         for(int i = gr.x1; i < gr.x2; i++)
         {
             for(int j = gr.y1; j < gr.y2; j++)
             {
                 if(isMarked(i, j))
                     continue;
                 for(PointIterator pi = cellIterator(i, j); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     S = Vertex.findCenter(a, b, pom);
                     double rad = Vertex.distance(S, a);
                     if(pom != a && pom != b && Vertex.ccw(a, b, pom) && rad < DT.MAX_VALUE)
                     {
                         found = pom;
                         break label0;
                     }
                 }

                 setMark(i, j);
             }

         }

         if(gr.x1 == 0 && gr.y1 == 0 && gr.x2 == xsize && gr.y2 == ysize)
             break;
         gr.set(gr.x1 - 1, gr.y1 - 1, gr.x2 + 1, gr.y2 + 1);
     }
     if(found == null)
         return null;
     S = Vertex.findCenter(a, b, found);
     double dist = Vertex.distance(S, a);
     if(!Vertex.ccw(a, b, S))
         dist = -dist;
     gr.set(S, Math.abs(dist));
     for(int i = gr.x1; i < gr.x2; i++)
     {
label1:
         for(int j = gr.y1; j < gr.y2; j++)
         {
             if(isMarked(i, j))
                 continue;
             for(PointIterator pi = cellIterator(i, j); pi.hasNext();)
             {
                 Vertex pom = (Vertex)pi.next();
                 if(pom != a && pom != b && Vertex.ccw(a, b, pom))
                 {
                     S = Vertex.findCenter(a, b, pom);
                     double dd = Vertex.distance(S, a);
                     if(!Vertex.ccw(a, b, S))
                         dd = -dd;
                     if(dd < dist)
                     {
                         dist = dd;
                         found = pom;
                         int x1 = gr.x1;
                         int y1 = gr.y1;
                         int y2 = gr.y2;
                         gr.set(S, Math.abs(dist));
                         if(gr.x1 < x1 || gr.y1 < y1 || gr.y2 > y2)
                         {
                             i = gr.x1 - 1;
                             break label1;
                         }
                     }
                 }
             }

             setMark(i, j);
         }

     }

     return found;
 }

 public QuadEdge findShortestCrossEdge(double med, int coord)
 {
     Vertex a = null;
     Vertex b = null;
     double d = 0.0D;
     switch(coord)
     {
     case 0: // '\0'
         for(int x = (int)((med - xoffset) / cellsize); a == null && x >= 0; x--)
         {
             for(int i = 0; i < ysize; i++)
             {
                 for(PointIterator pi = cellIterator(x, i); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     double dd = Math.abs(pom.x - med);
                     if(pom.x < med && (a == null || dd < d))
                     {
                         a = pom;
                         d = dd;
                     }
                 }

             }

         }

         if(a == null)
             return null;
         int xmax = xsize;
         for(int x = (int)((med - xoffset) / cellsize); b == null || x < xmax; x++)
         {
             for(int i = 0; i < ysize; i++)
             {
                 for(PointIterator pi = cellIterator(x, i); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     double dd = Vertex.distance(a, pom);
                     if(pom.x >= med && (b == null || dd < d))
                     {
                         b = pom;
                         d = dd;
                         xmax = (int)(((med + d) - xoffset) / cellsize) + 1;
                         if(xmax > xsize)
                             xmax = xsize;
                     }
                 }

             }

         }

         break;

     case 1: // '\001'
         for(int y = (int)((med - yoffset) / cellsize); a == null && y >= 0; y--)
         {
             for(int i = 0; i < xsize; i++)
             {
                 for(PointIterator pi = cellIterator(i, y); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     double dd = Math.abs(pom.y - med);
                     if(pom.y < med && (a == null || dd < d))
                     {
                         a = pom;
                         d = dd;
                     }
                 }

             }

         }

         if(a == null)
             return null;
         int ymax = ysize;
         for(int y = (int)((med - yoffset) / cellsize); b == null || y < ymax; y++)
         {
             for(int i = 0; i < xsize; i++)
             {
                 for(PointIterator pi = cellIterator(i, y); pi.hasNext();)
                 {
                     Vertex pom = (Vertex)pi.next();
                     double dd = Vertex.distance(a, pom);
                     if(pom.y >= med && (b == null || dd < d))
                     {
                         b = pom;
                         d = dd;
                         ymax = (int)(((med + d) - yoffset) / cellsize) + 1;
                         if(ymax > ysize)
                             ymax = ysize;
                     }
                 }

             }

         }

         break;
     }
     if(b == null)
         return null;
     else
         return QuadEdge.makeEdge(a, b);
 }

 private GridCell grid[][];
 private PointItem array[];
 private int xsize;
 private int ysize;
 private double xoffset;
 private double yoffset;
 private double cellsize;
 private int currentMark;






}

