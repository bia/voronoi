package plugins.ragini.voronoidiagram;

public interface Setter<T> {
	
	public void set(T input); 
	
	public T get(); 

}
