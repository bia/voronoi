package plugins.ragini.voronoidiagram;

public class Vert extends Point {
	
	private int vpid; 
	
	public Vert(float _x, float _y) {
		x = _x; 
		y = _y; 
	}
	
	public Vert() {
	}
	
	public int vpid() {
		return vpid; 
	}

}
