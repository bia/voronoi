package plugins.ragini.voronoidiagram;

public class SiteProvider implements Provider<Neighbor> {

	private int index; 
	private int lim; 
	private Constants cons; 
	
	public SiteProvider (Constants _cons) {
		index = 0; 
		cons = _cons; 
		lim = cons.sites().length; 
	}
	
	@Override
	public Neighbor next() {
		Neighbor out = null; 
		if (index < lim) {
			out = cons.sites()[index];
			index++;
		}
		//System.out.println("newsite: " + out.getCoord().X() + " " + out.getCoord().Y()); 
		return out; 
	}

}
