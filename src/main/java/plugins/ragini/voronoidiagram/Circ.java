package plugins.ragini.voronoidiagram;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class Circ {
	
	private Point2D center;
	private double radius; 
	private Point2D corner; 
	private double width; 
	private Rectangle2D rec; 
	
	public Circ(Point2D c, double r) {
		center = c;
		radius = r; 
		width = r*2; 
		corner = new Point2D.Double(center.getX()-r, center.getY()-r); 
		rec = new Rectangle2D.Double(corner.getX(), corner.getY(), width, width); 
	}
	
	public double r() {
		return radius; 
	}
	
	public Rectangle2D rectangle() {
		return rec; 
	}
	
	//private float cx, cy, r; 
	//private int nbr1, nbr2, nbr3;

}
