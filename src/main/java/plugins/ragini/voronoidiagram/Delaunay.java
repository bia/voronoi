package plugins.ragini.voronoidiagram;

public class Delaunay {
	
	public Arcs InitArcD() {
		Arcs item; 
		
		item = new Arcs(); 
		item.setNext(null);
		item.setID(-1);
		item.setDist(-1); 
		return item; 
		
	}
	
	public void AddArc(Arcs IVert, int v2, float dist, IntSetter nset) {
		Arcs item = InitArcD();
		Arcs courant; //does n always have to get set only to 1? 
		
		item.setID(v2);
		item.setDist(dist);
		
		if (IVert.next() == null) {
			IVert.setNext(item);
			nset.set(1);
		}
		else {
			courant = IVert.next(); 
			if (courant.dist() > dist) {
				IVert.setNext(item);
				item.setNext(courant); 
				nset.set(1); 
			}
			else {
				while ((courant.next()!=null) && (courant.next().dist() <= dist) && (courant.id() != v2)) {
					courant = courant.next();
				}
				if (courant.id() != v2) {
					item.setNext(courant.next()); 
					courant.setNext(item); 
					nset.set(1); 
				}
			}
		}
	}
	
	public GraphD [] InitGDelaunay(int n) {
		GraphD [] item;
		int i; 
		
		item = new GraphD [n]; 
		for(i = 0; i < n; i++) {
			item[i].setArcs(null);
			item[i].setNbArcs((short) 0); 
		}
		return item; 
	}
	
	public void InsertArc(GraphD[] Graph, int Vertice1, int Vertice2) {
		float dist;
		float min;
		Arcs IVert, Courant, Item; 
		int n = 0;  
		IntSetter nset = new IntSetter(n); 
		
		dist = (float) Math.sqrt((Graph[Vertice1].pt().X() - Graph[Vertice2].pt().X()) * (Graph[Vertice1].pt().X()-Graph[Vertice2].pt().X()) + (Graph[Vertice1].pt().Y() - Graph[Vertice2].pt().Y()) * (Graph[Vertice1].pt().Y()- Graph[Vertice2].pt().Y())); 
		if (Graph[Vertice1].arcs() == null) {
			IVert = InitArcD();
			
			AddArc(IVert, Vertice2, dist, nset);
			n = nset.get(); 
			Graph[Vertice1].setArcs(IVert); 
			Graph[Vertice1].setNbArcs((short) (Graph[Vertice1].nbarcs() + n)); //does this cast work?  
		}
		else {
			IVert = Graph[Vertice1].arcs();
			
			if (IVert.next().dist() > dist) {
				Item = InitArcD();
				Item.setID(Vertice2); 
				Item.setDist(dist);
				Item.setNext(IVert.next()); 
				Graph[Vertice1].setNbArcs((short) (Graph[Vertice1].nbarcs() + 1)); 
				Graph[Vertice1].arcs().setNext(Item); 
			}
			else {
				AddArc(IVert, Vertice2, dist, nset); //or, maybe could have it return the number of new arcs? 
				n = nset.get();
				Graph[Vertice1].setNbArcs((short) (Graph[Vertice1].nbarcs() + n)); 
			}
		}
		
		if(Graph[Vertice2].arcs() == null) {
			IVert = InitArcD();
			AddArc(IVert, Vertice1, dist, nset);
			n = nset.get();
			Graph[Vertice2].setNbArcs((short) (Graph[Vertice1].nbarcs() + n)); 
			Graph[Vertice2].setArcs(IVert); 
		}
		else {
			IVert = Graph[Vertice2].arcs();
			
			if (IVert.next().dist() > dist) { 
				Courant = IVert;
				Item = InitArcD();
				Item.setID(Vertice1);
				Item.setDist(dist);
				Item.setNext(IVert.next()); 
				Graph[Vertice2].setNbArcs((short) (Graph[Vertice1].nbarcs() + 1));
				Graph[Vertice2].arcs().setNext(Item);
			}
			else {
				AddArc(IVert, Vertice1, dist, nset);
				n = nset.get();
				Graph[Vertice2].setNbArcs((short) (Graph[Vertice1].nbarcs() + n)); 
			}
		}
		
	}

}
