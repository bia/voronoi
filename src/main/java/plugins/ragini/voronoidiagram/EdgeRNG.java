package plugins.ragini.voronoidiagram;

import java.awt.geom.Line2D;
import java.util.ArrayList;

public class EdgeRNG extends Line2D.Double {
	
	private Neighbor n1, n2; 
	private ArrayList<Neighbor> endpoints; 
	private double length; 
	
	public EdgeRNG(Neighbor g1, Neighbor g2) {
		super(g1.getCoord().X(), g1.getCoord().Y(), g2.getCoord().X(), g2.getCoord().Y()); 
		n1 = g1; 
		n2 = g2; 
		endpoints = new ArrayList<Neighbor>(2); 
		endpoints.add(n1);
		endpoints.add(n2);
		length = distance(g1.getCoord().X(), g1.getCoord().Y(), g2.getCoord().X(), g2.getCoord().Y()); 
	}
	
	public ArrayList<Neighbor> endpoints() {
		return endpoints; 
	}
	
	private double distance(double ax, double ay, double bx, double by) {
		return (Math.sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay)));
	}
	
	public double length() {
		return length; 
	}

}
