package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   Image_Extract.java

import ij.ImagePlus;
import ij.WindowManager;
import ij.plugin.filter.MaximumFinder;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import java.awt.Rectangle;
import javax.swing.JOptionPane;

public class Image_Extract
 implements PlugInFilter
{

 public Image_Extract()
 {
 }

 public int setup(String arg, ImagePlus imp)
 {
     return 127;
 }

 public void run(ImageProcessor ip)
 {
     imp = WindowManager.getCurrentImage();
     ImageProcessor ip2 = imp.getProcessor().resize(imp.getWidth(), imp.getHeight());
     (new ImagePlus("Init", ip2)).show();
     ip = ip.convertToByte(true);
     double tolerance = 30D;
     double input = -1D;
     String s = "30";
     s = JOptionPane.showInputDialog("Input the tolerance parameter (30 by default):", "30");
     input = Double.valueOf(s).doubleValue();
     if(input > 0.0D)
         tolerance = input;
     Rectangle r = ip.getRoi();
     ip.invert();
     ByteProcessor bp = (new MaximumFinder()).findMaxima(ip, tolerance, -808080D, 0, true, false);
     ip.setPixels(bp.getPixels());
     ip.invertLut();
     ip = ip.convertToRGB();
     imp.setProcessor("RGB", ip);
     imp.repaintWindow();
     int mask = 255;
     for(int y = r.y; y < r.y + r.height; y++)
     {
         for(int x = r.x; x < r.x + r.width; x++)
         {
             int rgb = ip.get(x, y);
             int rouge = rgb >> 16 & mask;
             int vert = rgb >> 8 & mask;
             int bleu = rgb & mask;
             if(rouge == 0 && bleu == 0 && vert == 0)
                 ip.set(x, y, 0xff0000);
         }

     }

 }

 private ImagePlus imp;
}
