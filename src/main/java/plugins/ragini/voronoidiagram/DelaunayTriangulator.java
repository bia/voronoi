package plugins.ragini.voronoidiagram;

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3) 
//Source File Name:   DelaunayTriangulator.java

import java.io.PrintStream;
import java.util.*;

//Referenced classes of package delaunay:
//         DT, Face, QuadEdge, AFL, 
//         UniformGrid, Display, Vertex

public class DelaunayTriangulator
{

 public DelaunayTriangulator()
 {
     faces = new Vector();
     edges = new Vector();
     radiusMean = 0.0D;
     DT.counter1 = 0;
     DT.counter2 = 0;
     DT.counter3 = 0;
 }

 public void resetAll()
 {
     faces.clear();
     edges.clear();
     radiusMean = 0.0D;
     DT.counter1 = 0;
     DT.counter2 = 0;
     DT.counter3 = 0;
 }

 private void ajouterFace(Face f)
 {
     faces.addElement(f);
     DT.counter3++;
 }

 public Enumeration extractFaces()
 {
     return faces.elements();
 }

 public Vector getFaces()
 {
     return faces;
 }

 public void setMeasure()
 {
     Enumeration listing = extractFaces();
     Vector auxV = new Vector();
     double measureMean = 0.0D;
     int count = 0;
     while(listing.hasMoreElements()) 
     {
         Face f = (Face)listing.nextElement();
         double rad = f.getMeasure();
         if(rad < DT.MAX_VALUE)
         {
             count++;
             auxV.add(new Double(rad));
             measureMean += rad;
         }
     }
     double aux[] = new double[auxV.size()];
     for(int i = 0; i < count; i++)
         aux[i] = ((Double)auxV.get(i)).doubleValue();

     Arrays.sort(aux);
     measureMedian = aux[count / 2];
     this.measureMean = measureMean / (double)count;
 }

 public void setRadius()
 {
     Enumeration listing = extractFaces();
     Vector auxV = new Vector();
     double radiusMean = 0.0D;
     int count = 0;
     while(listing.hasMoreElements()) 
     {
         Face f = (Face)listing.nextElement();
         double rad = f.getInvRadius();
         if(rad < DT.MAX_VALUE)
         {
             count++;
             auxV.add(new Double(rad));
             radiusMean += rad;
         }
     }
     double aux[] = new double[auxV.size()];
     for(int i = 0; i < count; i++)
         aux[i] = ((Double)auxV.get(i)).doubleValue();

     Arrays.sort(aux);
     radiusMedian = aux[count / 2];
     this.radiusMean = radiusMean / (double)count;
 }

 public void setSize()
 {
     Enumeration listing = extractFaces();
     Vector auxV = new Vector();
     double sizeMean = 0.0D;
     int count = 0;
     while(listing.hasMoreElements()) 
     {
         Face f = (Face)listing.nextElement();
         double size = f.getInvSize();
         if(size < DT.MAX_VALUE)
         {
             count++;
             auxV.add(new Double(size));
             sizeMean += size;
         }
     }
     double aux[] = new double[auxV.size()];
     for(int i = 0; i < count; i++)
         aux[i] = ((Double)auxV.get(i)).doubleValue();

     Arrays.sort(aux);
     sizeMedian = aux[count / 2];
     this.sizeMean = sizeMean / (double)count;
 }

 public double getRadiusMean()
 {
     return radiusMean;
 }

 public double getRadiusMedian()
 {
     return radiusMedian;
 }

 public double getSizeMean()
 {
     return sizeMean;
 }

 public double getSizeMedian()
 {
     return sizeMedian;
 }

 public double getMeasureMean()
 {
     return measureMean;
 }

 public double getMeasureMedian()
 {
     return measureMedian;
 }

 public double getThresholdOpt()
 {
     return measureMedian * DT.densityParameter;
 }

 public Vector getEdges()
 {
     return edges;
 }

 public Enumeration extractEdges()
 {
     return edges.elements();
 }

 private void extractEdges(QuadEdge edge)
 {
     DT.counter1 = 0;
     extractEdges(edge, 0x7fffffff);
     edges.clear();
     Stack stk = new Stack();
     int counter = -1;
     stk.push(edge);
     while(!stk.isEmpty()) 
     {
         QuadEdge e = (QuadEdge)stk.pop();
         if(e.getCounter() != counter)
         {
             DT.counter1++;
             if(e.isCanonical())
                 edges.add(e);
             else
                 edges.add(e.sym());
         }
         e.setCounter(counter);
         e.sym().setCounter(counter);
         if(e.onext().getCounter() != counter)
             stk.push(e.onext());
         if(e.sym().onext().getCounter() != counter)
             stk.push(e.sym().onext());
     }
 }

 public Enumeration extractEdges(QuadEdge edge, int counter)
 {
     Vector edges = new Vector();
     Stack stk = new Stack();
     stk.push(edge);
     while(!stk.isEmpty()) 
     {
         QuadEdge e = (QuadEdge)stk.pop();
         if(e.getCounter() < counter)
             if(e.isCanonical())
                 edges.add(e);
             else
                 edges.add(e.sym());
         e.setCounter(counter);
         e.sym().setCounter(counter);
         if(e.onext().getCounter() < counter)
             stk.push(e.onext());
         if(e.sym().onext().getCounter() < counter)
             stk.push(e.sym().onext());
     }
     return edges.elements();
 }

 private QuadEdge putEdge(QuadEdge e, double med, int coord, AFL left, AFL center, AFL right)
 {
     int i = e.type(med, coord);
     QuadEdge qe;
     if(i < 0)
         qe = left.putTest(e);
     else
     if(i == 0)
         qe = center.putTest(e);
     else
         qe = right.putTest(e);
     return qe;
 }

 public QuadEdge inCoDeTotal(Display disp, Vertex sites[], Vertex sitesBin[])
 {
     QuadEdge tri = inCoDe(disp, sites);
     extractEdges(tri);
     constructDual();
     return tri;
 }

 public QuadEdge inCoDe(Display disp, Vertex sites[])
 {
     UniformGrid grid = new UniformGrid(sites, sites.length);
     AFL list = new AFL();
     AFL list_aux = new AFL();
     System.out.println("Debut inCode");
     disp.setQuadEdge(null);
     disp.setRange(0.0D, 0.0D, 1.0D, 1.0D);
     disp.setCenterAFL(list);
     disp.setTriangles(faces);
     disp.setEdges(edges);
     disp.breakBig();
     int count = 0;
     Vertex seed = new Vertex(grid.XMin() + grid.XSize() / 2D, grid.YMin() + grid.YSize() / 2D);
     Vertex A = grid.findNearestPoint(seed);
     Vertex B = grid.findNearestPoint(A);
     Vertex C = grid.findDelaunayPoint(A, B);
     if(C == null)
         C = grid.findDelaunayPoint(B, A);
     if(C == null)
         return QuadEdge.makeEdge(A, B);
     Vertex S = Vertex.findCenter(A, B, C);
     double rad = Vertex.distance(S, A);
     disp.setCircle(S, rad);
     disp.setTriangle(A, B, C);
     if(Vertex.ccw(A, B, C))
     {
         Vertex pom = B;
         B = C;
         C = pom;
     }
     QuadEdge a = QuadEdge.makeEdge(A, B);
     QuadEdge b = QuadEdge.makeEdge(B, C);
     a.sym().splice(b);
     QuadEdge c = QuadEdge.connect(b, a);
     QuadEdge tri = a;
     disp.setQuadEdge(tri);
     list.put(a);
     list.put(b);
     list.put(c);
     Face f = new Face(a, b, c);
     ajouterFace(f);
     a.setFace(f);
     b.setFace(f);
     c.setFace(f);
     disp.breakBig();
     for(; !list.isEmpty(); disp.breakBig())
     {
         count++;
         a = list.extract();
         QuadEdge olda = list_aux.putTest(a);
         if(olda != null)
             System.out.println("InCode deja traite");
         A = a.orig();
         B = a.dest();
         C = grid.findDelaunayPoint(A, B);
         if(C != null && olda == null)
         {
             S = Vertex.findCenter(A, B, C);
             rad = Vertex.distance(S, A);
             disp.setCircle(S, rad);
             disp.setTriangle(A, B, C);
             b = QuadEdge.makeEdge(A, C);
             c = QuadEdge.makeEdge(C, B);
             f = null;
             QuadEdge oldb = list.putTest(b);
             QuadEdge oldc = list.putTest(c);
             if(oldb == null && oldc == null)
             {
                 a.splice(b);
                 a.lnext().splice(c.sym());
                 b.sym().splice(c);
                 f = new Face(a.sym(), b, c);
                 b.setFace(f);
                 c.setFace(f);
             } else
             if(oldb == null)
             {
                 a.splice(b);
                 if(oldc.lnext() != b.sym())
                     oldc.lnext().splice(b.sym());
                 if(oldc.onext() != a.sym())
                 {
                     oldc.lnext().splice2(b.lnext());
                     oldc.splice2(a.sym());
                 }
                 f = new Face(a.sym(), b, oldc.sym());
                 b.setFace(f);
                 oldc.sym().setFace(f);
             } else
             if(oldc == null)
             {
                 if(a.lnext() != c.sym())
                     a.lnext().splice(c.sym());
                 oldb.splice(c);
                 if(a.onext() != oldb.sym())
                 {
                     oldb.lnext().splice2(a.sym().rprev());
                     a.splice2(oldb.sym());
                 }
                 f = new Face(a.sym(), oldb.sym(), c);
                 oldb.sym().setFace(f);
                 c.setFace(f);
             } else
             {
                 if(a.onext() != oldb.sym())
                     a.splice2(oldb.sym());
                 if(oldb.onext() != oldc.sym())
                 {
                     oldc.lnext().splice2(oldb.sym().rprev());
                     oldb.splice2(oldc.sym());
                 }
                 f = new Face(a.sym(), oldb.sym(), oldc.sym());
                 oldb.sym().setFace(f);
                 oldc.sym().setFace(f);
             }
             a.sym().setFace(f);
             ajouterFace(f);
         }
     }

     disp.setCircle(null, 0.0D);
     disp.setTriangle(null, null, null);
     System.out.println((new StringBuilder("Fin inCode : ")).append(count).append(" passages\n").toString());
     return tri;
 }

 private void constructDual()
 {
     for(Enumeration listing = extractEdges(); listing.hasMoreElements();)
     {
         QuadEdge e = (QuadEdge)listing.nextElement();
         QuadEdge r = e.rot();
         r.setData(e.rightFace().getBary());
         if(e.sym().rightFace() != null)
             r.sym().setData(e.sym().rightFace().getBary());
     }

 }

 private void setFacesV8_bin(Vertex sitesBin[])
 {
     Enumeration listing = extractFaces();
     if(sitesBin.length == 0)
     {
         double alpha = getThresholdOpt();
         while(listing.hasMoreElements()) 
         {
             Face f = (Face)listing.nextElement();
             if(f.getMeasure() > alpha)
                 f.setVal(1.0F);
             else
                 f.setVal(0.0F);
         }
     } else
     {
         while(listing.hasMoreElements()) 
         {
             Face f = (Face)listing.nextElement();
             f.setVal(0.0F);
             for(int i = 0; i < sitesBin.length; i++)
                 if(f.contains(sitesBin[i]) == 1)
                     f.setVal(1.0F);

         }
     }
 }

 private void setFacesV8_erode()
 {
     float e;
     Face f;
     for(Enumeration listing = extractFaces(); listing.hasMoreElements(); f.setValAux(e))
     {
         e = 1.0F;
         f = (Face)listing.nextElement();
         for(Iterator iter = f.extractFacesVoisines(); iter.hasNext();)
         {
             Face ff = (Face)iter.next();
             e = Math.min(e, ff.getVal());
         }

     }

     for(Enumeration listing = extractFaces(); listing.hasMoreElements(); f.setVal(f.getValAux()))
         f = (Face)listing.nextElement();

 }

 private void setFacesV8_dilate(double angle, double epsilon, int ordre)
 {
     float e;
     Face f;
     for(Enumeration listing = extractFaces(); listing.hasMoreElements(); f.setValAux(e))
     {
         e = 0.0F;
         f = (Face)listing.nextElement();
         for(Iterator iter = f.extractFacesVoisines(angle, epsilon); iter.hasNext();)
         {
             Face ff = (Face)iter.next();
             e = Math.max(e, Math.max(f.getVal(), (ff.getVal() + (1.0F - (float)ordre / (float)DT.step)) - 1.0F));
         }

     }

     for(Enumeration listing = extractFaces(); listing.hasMoreElements(); f.setVal(f.getValAux()))
         f = (Face)listing.nextElement();

 }

 private void setEdgesV8()
 {
     for(Enumeration listing = extractEdges(); listing.hasMoreElements();)
     {
         QuadEdge e = (QuadEdge)listing.nextElement();
         if(e.sym().rightFace() != null)
         {
             e.setLambda(Math.min(e.rightFace().getVal(), e.sym().rightFace().getVal()));
             e.setMu(Math.max(e.rightFace().getVal(), e.sym().rightFace().getVal()));
             e.sym().setLambda(Math.min(e.rightFace().getVal(), e.sym().rightFace().getVal()));
             e.sym().setMu(Math.max(e.rightFace().getVal(), e.sym().rightFace().getVal()));
         } else
         {
             e.setLambda(1.0F - e.rightFace().getVal());
             e.setMu(e.rightFace().getVal());
             e.sym().setLambda(1.0F - e.rightFace().getVal());
             e.sym().setMu(e.rightFace().getVal());
         }
     }

 }

 public void constructBin(Vertex sitesBin[])
 {
     setFacesV8_bin(sitesBin);
     setEdgesV8();
 }

 public void constructErode()
 {
     setFacesV8_erode();
     setEdgesV8();
 }

 public void constructDilate(double angle, double epsilon)
 {
     setFacesV8_dilate(angle, epsilon, 0);
     setEdgesV8();
 }

 public void constructFuzzyDilate(double angle, double epsilon)
 {
     for(int k = 0; k < DT.step + 1; k++)
         setFacesV8_dilate(angle, epsilon, k);

     setEdgesV8();
 }

 public void constructInter(DelaunayTriangulator dt1, DelaunayTriangulator dt2)
 {
     Enumeration listing = extractFaces();
     Enumeration listing1 = dt1.extractFaces();
     Face f;
     Face f1;
     Face f2;
     for(Enumeration listing2 = dt2.extractFaces(); listing.hasMoreElements() && listing1.hasMoreElements() && listing2.hasMoreElements(); f.setVal(Math.min(f1.getVal(), f2.getVal())))
     {
         f = (Face)listing.nextElement();
         f1 = (Face)listing1.nextElement();
         f2 = (Face)listing2.nextElement();
     }

     setEdgesV8();
 }

 public void constructUnion(DelaunayTriangulator dt1, DelaunayTriangulator dt2)
 {
     Enumeration listing = extractFaces();
     Enumeration listing1 = dt1.extractFaces();
     Face f;
     Face f1;
     Face f2;
     for(Enumeration listing2 = dt2.extractFaces(); listing.hasMoreElements() && listing1.hasMoreElements() && listing2.hasMoreElements(); f.setVal(Math.max(f1.getVal(), f2.getVal())))
     {
         f = (Face)listing.nextElement();
         f1 = (Face)listing1.nextElement();
         f2 = (Face)listing2.nextElement();
     }

     setEdgesV8();
 }

 public void constructComp()
 {
     Face f;
     for(Enumeration listing = extractFaces(); listing.hasMoreElements(); f.setVal(1.0F - f.getVal()))
         f = (Face)listing.nextElement();

     setEdgesV8();
 }

 public void expliciter()
 {
     Enumeration listing = extractEdges();
     System.out.println((new StringBuilder("Enumeration des ")).append(getEdges().size()).append(" edges ").toString());
     QuadEdge e;
     for(; listing.hasMoreElements(); e.expliciter())
     {
         e = (QuadEdge)listing.nextElement();
         System.out.println((new StringBuilder()).append(e).append(" Associ\357\277\275 aux triangles ").append(e.rightFace()).append(" ").append(e.sym().rightFace()).toString());
     }

     listing = extractFaces();
     System.out.println((new StringBuilder("Enumeration des ")).append(getFaces().size()).append(" faces").toString());
     Face f;
     for(; listing.hasMoreElements(); f.extractFacesVoisines())
     {
         f = (Face)listing.nextElement();
         System.out.println(f);
     }

 }

 private Vector faces;
 private Vector edges;
 private double radiusMean;
 private double radiusMedian;
 private double measureMean;
 private double measureMedian;
 private double sizeMean;
 private double sizeMedian;
}
