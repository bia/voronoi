package plugins.ragini.voronoidiagram;

public class Arcs {

	private int id;
	private float dist;
	private Arcs next;
	
	public void setID(int i) {
		id = i;
	}
	
	public void setDist(float d) {
		dist = d;
	}
	
	public void setNext(Arcs a) {
		next = a; 
	}
	
	public Arcs next() {
		return next; 
	}
	
	public float dist() {
		return dist; 
	}
	
	public int id() {
		return id; 
	}
	
}
