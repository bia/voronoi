 package plugins.ragini.voronoidiagram;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;


public class VRegion {
	
	private float pxmin = 10^10, pxmax = -1*(10^10), pymin = 10^10, pymax = -1 * (10^10); 
	
	private float[] xminymin, xminymax, xmaxymax, xmaxymin;
	
	private float Pxmin, Pxmax, Pymin, Pymax; 
	
	private VertTheta[] corner;
	
	private int numverts, numvedges, numtris;
	
	private int[] inverse;
	
	private VertTheta [] vtlist, slist;
	private int vtnum; 
	private Integer[][] adjlist; 
	
	private int MAXVERTS;
	private Constants constants; 
	private Vonoroi voronoi; 
	private ArrayList<Neighbor>[][] bins; 
	private VoronoiDiagram main; 

	private static final int SAFE = -900000;
	
	public VRegion(Constants _constants, VoronoiDiagram v) {
		xminymin = new float[3]; 
		xminymax = new float[3]; 
		xmaxymax = new float[3]; 
		xmaxymin = new float[3]; 
		vtlist = new VertTheta [1024];
		slist = new VertTheta [1024]; 
		adjlist = new Integer [MAXVERTS * 3][2]; 
		corner = new VertTheta[4]; 
		constants = _constants; 
		inverse = new int[constants.maxVerts()];
		main = v; 
	}
	
	public void setVoronoi(Vonoroi v) {
		voronoi = v; 
	}
	
	public void outSite(Site s) {
		
	}
	
	public void outBisector(Edge e) {
		//System.out.println("triangle line: " + "(" + e.reg()[0].getCoord().X() + "," + e.reg()[1].getCoord().Y() + ")" + " (" + e.reg()[0].getCoord().X() + "," + e.reg()[1].getCoord().Y() + ")");
	}
	
	public void clipLine(Edge e, int origin) {
		Site s1, s2;
		Site r1, r2;
		float x1, x2, y1, y2; 
		
		if (e.a() == 1 && e.b() >= 0) {
			s1 = e.ep()[1];
			//if (s1 == null) System.out.println("s1 null"); 
			//else if (s1.getCoord() == null) System.out.println("s1 coord null");
			//else System.out.println("s1: " + s1.getCoord().X() + " " + s1.getCoord().Y()); 
			s2 = e.ep()[0];
			//if (s2 == null) System.out.println("s2 null"); 
			//else if (s2.getCoord() == null) System.out.println("s2 coord null"); 
			//else System.out.println("s2: " + s2.getCoord().X() + " " + s1.getCoord().Y()); 
			r1 = e.reg()[1];
			r2 = e.reg()[0]; 
		}
		
		else {
			s1 = e.ep()[0];
			//if (s1 == null) System.out.println("s1 null"); 
			//else if (s1.getCoord() == null) System.out.println("s1 coord null"); 
			//else System.out.println("s1: " + s1.getCoord().X() + " " + s1.getCoord().Y()); 
			s2 = e.ep()[1];
			//if (s2 == null) System.out.println("s2 null"); 
			//else if (s2.getCoord() == null) System.out.println("s2 coord null"); 
			//else System.out.println("s2: " + s2.getCoord().X() + " " + s2.getCoord().Y()); 
			r1 = e.reg()[0];
			r2 = e.reg()[1]; 
		}
		
		if (s1 == null || s2 == null) {
			NVert vA, vB; 
			int A, B; 
			
			A = r1.sitenbr();
			B = r2.sitenbr();
			
			vA = constants.chverts()[A]; 
			vB = constants.chverts()[B]; 
			
			vA.setOnHull(1); 
			vB.setOnHull(1); 
			
			if (vA.nbr1() == 1) {
				vB.setNbr1(B); 
			}
			else if (vA.nbr2() == -1) {
				if(vA.nbr1() != B) {
					vA.setNbr2(B);
				}
			}
			
			if (vB.nbr1() == -1) {
				vB.setNbr1(A);
			}
			else if (vB.nbr2() == -1) {
				if (vB.nbr1() != A) {
					vB.setNbr2(A); 
				}
			}
			
		}
		
		if(e.a() == 1) {
			y1 = pymin; 
			if (s1 != null && s1.getCoord().Y() > pymin) {
				y1 = s1.getCoord().Y(); 
			}
			if (y1 > pymax) return;
			x1 = e.c() - e.b()*y1; 
			y2 = pymax;
			if (s2 != null && s2.getCoord().Y() < pymax) {
				y2 = s2.getCoord().Y();
			}
			if (y2 < pymin) return; 
			x2 = e.c() - e.b()*y2; 
			if ((x1 > pxmax & x2 > pxmax) | (x1 < pxmin & x2 <pxmin)) {
				return;
			}
			if (x1 > pxmax) {
				x1 = pxmax;
				y1 = (e.c()-x1)/e.b();
				if (e.b() == 0) System.out.println("dividing by zero"); 
				
			}
			if (x1 < pxmin) {
				x1 = pxmin;
				y1 = (e.c() - x1)/e.b();
				if (e.b() == 0) System.out.println("dividing by zero"); 
			}
			if (x2 > pxmax) {
				x2 = pxmax;
				y2 = (e.c() - x2)/e.b(); 
				if (e.b() == 0) System.out.println("dividing by zero"); 
			}
			if (x2  < pxmin) {
				x2 = pxmin;
				y2 = (e.c() - x2)/e.b(); 
				if (e.b() == 0) System.out.println("dividing by zero"); 
			}
		}
		else {
			x1 = pxmin;
			if (s1 != null && s1.getCoord().X() > pxmin) {
				x1 = s1.getCoord().X(); 
			}
			if (x1 > pxmax) return; 
			y1 = e.c() - e.a() * x1; 
			x2 = pxmax;
			if (s2 != null && s2.getCoord().X() < pxmax) {
				x2 = s2.getCoord().X();
			}
			if (x2 < pxmin) return; 
			y2 = e.c() - e.a() * x2;
			if ((y1 > pymax & y2 > pymax) | (y1 < pymin & y2 < pymin)) return;
			if (y1> pymax) {
				y1 = pymax;
				x1 = (e.c() - y1)/e.a();
				if(e.a() == 0) System.out.println("dividing by zero"); 
			}
			if(y1 < pymin) {
				y1 = pymin;
				x1 = (e.c() - y1) /e.a();
				if(e.a() == 0) System.out.println("dividing by zero"); 
			}
			if(y2 > pymax) {
				y2 = pymax;
				x2 = (e.c() - y2) /e.a();
				if(e.a() == 0) System.out.println("dividing by zero"); 
			}
			if(y2 < pymin) {
				y2 = pymin;
				x2 = (e.c() - y2) /e.a(); 
				if(e.a() == 0) System.out.println("dividing by zero"); 
			}
		}
		
		//draw line(x1,y1,x2,y2) 
		//System.out.println("line: " + "(" + x1 + "," + y1 + ")" + " (" + x2 + "," + y2 + ")"); 
		Point2D p1 = new Point2D.Double(x1, y1); 
		Point2D p2 = new Point2D.Double(x2, y2); 
		/*try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/ 
		constants.lines()[constants.lineindex()][0] =  p1; 
		constants.lines()[constants.lineindex()][1] =  p2;
		constants.setlineindex(constants.lineindex() + 1); 
		
		if (constants.triangulate() == 0) {
			constants.vedges()[numvedges] = new EdgeS();  
			constants.vedges()[numvedges].setx1(x1); 
			constants.vedges()[numvedges].sety1(y1);
			constants.vedges()[numvedges].setx2(x2);
			constants.vedges()[numvedges].sety2(y2);                          
			
			constants.vedges()[numvedges].setnbr1(r1 != null ? r1.sitenbr() : SAFE);
			constants.vedges()[numvedges].setnbr2(r2 != null ? r2.sitenbr() : SAFE-1); 
			
			if (r1 != null && r2 != null) {
				constants.vedges()[numvedges].setxm(avg(r1.getCoord().X(), r2.getCoord().X()));
				constants.vedges()[numvedges].setym(avg(r1.getCoord().Y(), r2.getCoord().Y())); 
			}
			
			if(constants.vrdebug() == 1) {
				///System.out.println...
			}
			
			if (numvedges < constants.maxEdges()) numvedges++; 
			else {
				System.out.println("edge list overflow!"); 
				System.exit(-1); 
			}
			
		}
		return;
		
	}
	
	public float avg(float a, float b) {
		return (a + b)/2; 
	}
	
	public void outEP(Edge e, int origin) {
		
		if(constants.triangulate() == 0) {
			clipLine(e, origin); 
		}
		
		if(constants.vrdebug() == 1) {
			///System.out.println...
		}
		
	}
	
	public void outVertex (Site v) {
		
		if(constants.triangulate() == 0) {
			constants.verts()[numverts] = new Vert(); 
			constants.verts()[numverts].setX(v.getCoord().X()); 
			constants.verts()[numverts].setY(v.getCoord().Y()); 
			
			if (numverts < constants.maxVerts()) numverts++; 
			else {
				System.out.println("nvert list overflow");
				System.exit(-1); //not sure if should use this 
			}
			
		}
		
		if(constants.vrdebug() == 1) {
			///System.out.println...
		}
		
	}
	
	public int CHS_LEFTOF(float cx, float cy, float ax, float ay, float bx, float by) {
		
		/* predicate: true iff point c leftof directed line ab */
		/* observation: we should use two shortest disp vectors */
		
		double dist_ab, dist_bc, dist_ca, max_dist;
		double det_b, det_a, det_c;
		double err_b, err_a, err_c; 
		
		if (ax == bx && ay == by) {
			System.out.println("indeterminate line");
			return 0; 
		}
		
		if ((cx == ax && cy == ay) ||(cx == bx && cy == by))
			return 0;
		
		dist_ab = Math.sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay));
		dist_bc = Math.sqrt((cx - bx)*(cx - bx) + (cy - by)*(cy - by));
		dist_ca = Math.sqrt((ax - cx)*(ax - cx) + (ay - cy)*(ay - cy)); 
		
		max_dist = max(dist_ab, max(dist_bc, dist_ca)); 
		
		if(max_dist == dist_bc) {
			/* use a as a pivot point [ie., form (c-a) x (b-a)] */
			det_a = (ax - cx) * (by - ay) - (ay - cy) * (bx - ax);
			if (det_a == 0) return 0; 
			err_a = (Math.abs((ax - cx) * (by - ay)) + Math.abs((ay - cy) * (bx - ax)))/det_a; 
			if ((Math.abs(det_a) > (10^-6)) || (err_a < (10^-7))) {
				return ((det_a > 0) ? 1 : 0); 
			}
			else return 0;
		}
		else if (max_dist == dist_ca) {
			/* use b as a pivot point [ie., form (a-b) x (c-b)] */
			det_b = (cx - bx) * (ay - by) - (cy - by) * (ax - bx);
			if (det_b == 0) return 0; 
			err_b = (Math.abs((cx - bx) * (ay - by)) + Math.abs((cy - by) * (ax - bx)))/det_b; 
			if ((Math.abs(det_b) > (10^-6)) || (err_b < (10^-7))) {
				return ((det_b > 0) ? 1 : 0); 
			}
			else return 0;
		}
		else if (max_dist == dist_ab) {
			/* use c as a pivot point [ie., form (b-c) x (a-c)] */
			det_c = (ax - cx) * (by - cy) - (ay - cy) * (bx - cx);
	        if (det_c == 0) {
	        	return (0);}
	        err_c = (Math.abs((cx - ax) * (by - cy)) + Math.abs((cy - ay) * (bx - cx))) / det_c;
		if ((Math.abs(det_c) > (10^-6)) || (err_c < (10^-7))) {
		    return ((det_c > 0)? 1 : 0); }
		else {
		    return (0); }
		}
		else {
			System.out.println("error max distance"); 
		}
		return -3;
	}
	
	public double max(double a, double b) {
		return (a > b? a : b); 
	}
	
	public float max(float a, float b) {
		return (a > b? a : b); 
	}
	
	public void outTriple(Neighbor s1, Neighbor s2, Neighbor s3) {
		
		Vert tmp;
		Tri tri = null; 
		
		if (constants.triangulate() == 1) {
			if (numtris < constants.maxTris()) {
				tri = new Tri(); 
			}
			else {
				System.out.println("triangle list overflow");
				System.exit(-1); 
			}
			
			tri.setV1(new Vert(constants.sites()[s1.sitenbr()].getCoord().X(), constants.sites()[s1.sitenbr()].getCoord().Y())); 
			tri.setV2(new Vert(constants.sites()[s2.sitenbr()].getCoord().X(), constants.sites()[s2.sitenbr()].getCoord().Y()));  
			tri.setV3(new Vert(constants.sites()[s3.sitenbr()].getCoord().X(), constants.sites()[s3.sitenbr()].getCoord().Y()));  
			
			if (CHS_LEFTOF(tri.v3().X(), tri.v3().Y(), tri.v1().X(), tri.v1().Y(), tri.v2().X(), tri.v2().Y()) == 0) {
				tmp = tri.v3();
				tri.setV3(tri.v2());
				tri.setV2(tmp); 
			}
			
			if(constants.rng() == 1) {
				ArrayList<EdgeRNG> lines = new ArrayList<EdgeRNG>(); 
				lines.add(new EdgeRNG(s1, s2)); 
				lines.add(new EdgeRNG(s2, s3));
				lines.add(new EdgeRNG(s1, s3));
				for (EdgeRNG e: lines) {
					boolean keep = true;
					int i = 0; 
					for(Neighbor n: e.endpoints()) {
						int thisbinx = (int) (Math.floor(n.getCoord().X()/constants.getedgex())) -1; 
						if (thisbinx < 0) thisbinx = 0; 
						int thisbiny = (int) (Math.floor(n.getCoord().Y()/constants.getedgey())) -1; 
						if (thisbiny < 0) thisbiny = 0; 
						int numbinsx = (int) Math.ceil(e.length()/constants.getedgex()) + 3; 
						int numbinsy = (int) Math.ceil(e.length()/constants.getedgey()) + 3; 
						int bstartx = thisbinx - numbinsx;
						if (bstartx < 0) bstartx = 0; 
						int bendx = thisbinx + numbinsx; 
						if (bendx > constants.maxbinx()) bendx = constants.maxbinx(); 
						int bstarty = thisbiny - numbinsy; 
						if (bstarty < 0) bstarty = 0; 
						int bendy = thisbiny + numbinsy; 
						if (bendy> constants.maxbiny()) bendy = constants.maxbiny(); 
						for (int bx = bstartx; bx < bendx; bx++) {
							for (int by = bstarty; by < bendy; by++) {
								ArrayList<Neighbor> test = bins[bx][by]; 
								if (test != null) {
									for (Neighbor t : test) {
										if (t != e.endpoints().get(0)
												&& t != e.endpoints().get(1)) {
											double dist = distance(n, t);
											if (dist < e.length()) {
												int otherend = (i == 0) ? 1: 0; 
												double dist2 = distance(e.endpoints().get(otherend), t);
												if (dist2 < e.length()) keep = false; 
											}
										}
									}
								}
							}
						}
						i++; 
					}
					if (keep == true) constants.relativeneighbors().add(e); 
				}
			}
		
			constants.tris()[numtris] = tri; 
			numtris++; 
			
			if (!s1.neighbors().contains(s2))
				s1.addNeighbor(s2); 
			if (!s1.neighbors().contains(s3))
				s1.addNeighbor(s3);  
			if (!s2.neighbors().contains(s1))
				s2.addNeighbor(s1); 
			if (!s2.neighbors().contains(s3))
				s2.addNeighbor(s3); 
			if (!s3.neighbors().contains(s1))
				s3.addNeighbor(s1); 
			if (!s3.neighbors().contains(s2))
				s3.addNeighbor(s2);   		
			
		}
	}
	
	private double distance(Neighbor g1, Neighbor g2) {
		return distance(g1.getCoord().X(), g1.getCoord().Y(), g2.getCoord().X(), g2.getCoord().Y()); 
	}
	
	private double distance(double ax, double ay, double bx, double by) {
		return (Math.sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay)));
	}
	
	public void vdInit() {
		numtris = 0;
		numvedges = 0;
		numverts = 0; 
	}
	
	public void emitSites(Site[] vsites, int nsites) {
		//Printing some stuff...
		//opening files 
	}
	
	private class scomp implements Comparator<Neighbor> {

		@Override
		public int compare(Neighbor s1, Neighbor s2) {
			if(s1.getCoord().Y() < s2.getCoord().Y()) return -1;
			else if (s1.getCoord().Y() > s2.getCoord().Y()) return 1;
			else if (s1.getCoord().X() < s2.getCoord().X()) return -1;
			else if (s1.getCoord().X() > s2.getCoord().X()) return 1;
			else return 0; 
		}
		
	}
	
	public void bBoxInit() {
		int k;
		float dx, dy, x, y; 
		
		constants.setvxmin(10^10);
		constants.setvxmax(-1*(10^10));
		constants.setvymin(10^10);
		constants.setvymax(-1*(10^10)); 
		
		Neighbor [] sites = new Neighbor[constants.nsites()]; 
		
		//System.out.println("numsites: " + constants.nsites()); 
		for (k = 0; k < constants.nsites(); k++) {
			//set sites all the same as GLsites (right now, the data read directly from the image) 
			
			Point c = new Point(); 
			x = constants.getGLSites()[k].X(); //System.out.println("glsitex: " + x); 
			y = constants.getGLSites()[k].Y(); //System.out.println("glsitey: " + y); 
			c.setX(x);
			c.setY(y);
			Neighbor s = new Neighbor(); 
			s.setCoord(c);
			s.setSiteNbr(k); 
			sites[k] = s; 
			
			//System.out.println("setting site: " + s.getCoord().X() + " " + s.getCoord().Y()); 
		
			//find the current bounds 
			
			if(x < constants.vxmin()) {
				constants.setvxmin(x);
			}
			
			if (y < constants.vymin()) {
				constants.setvymin(y); 
			}
			
			if (x > constants.vxmax()) {
				constants.setvxmax(x);
			}
			
			if (y > constants.vymax()) {
				constants.setvymax(y);  
			}
			
		}
		
		if(constants.rng() == 1) {
			constants.setrelativeneighbors(new ArrayList<EdgeRNG>(constants.nsites() * 2)); 
			int n = (int) Math.floor(Math.sqrt(constants.nsites())); 
			constants.setmaxbinx(n);
			constants.setmaxbiny(n); 
			bins = new ArrayList[n][n]; 
			int edgex = (int) Math.floor((constants.vxmax()-constants.vxmin())/n);
			constants.setedgex(edgex);
			int edgey = (int) Math.floor((constants.vymax()-constants.vymin())/n); 
			constants.setedgey(edgey); 
			int binx;
			int biny; 
			for(Neighbor j: sites) {
				binx = (int) (Math.floor(j.getCoord().X()/edgex)) -1; 
				if (binx < 0) binx = 0; 
				biny = (int) (Math.floor(j.getCoord().Y()/edgey)) -1; 
				if (biny < 0) biny = 0; 
				if (bins[binx][biny] == null) {
					bins[binx][biny] = new ArrayList<Neighbor>(); 
				}
				bins[binx][biny].add(j); 
			}
		}
		
		scomp comp = new scomp(); 
		
		Arrays.sort(sites, comp);
		
		constants.setSites(sites); 
		
		if (constants.vrdebug() == 1) {
			emitSites(constants.sites(), constants.nsites()); //print some stuff...
		}
		
		float epsilon = 1; 
		
		dx = constants.vxmax() - constants.vxmin();
		dx = max(dx, epsilon); 
		
		dy = constants.vymax() - constants.vymin();
		dy = max(dy, epsilon); 
		
		pxmin = (float) (constants.vxmin() - (dx*0.25));
		pymin = (float) (constants.vymin() - (dy*0.25));
		pxmax = (float) (constants.vxmax() + (dx*0.25));
		pymax = (float) (constants.vymax() + (dy*0.25));
		
		//Some more printing...
		
	}
	
	/* load_vsites(): 
    accept the n voronoi sites (x_n, y_n)
    calculate and store the voronoi diagram over the n sites,
    clipping all infinite edges to bbox: [xmin, ymin, xmax, ymax].
    
    note: if (xmin,ymin,xmax,ymax are all == 0.0), OR
	if these do not enclose the data, a bounding box
	 will be computed over the input.

    returns:
	-1 if error
	 0 otherwise
*/
	
	//usites = data from image 
	//n = number of data points? 
	
	public int loadVSites (int n, float[][] usites, float uxmin, float uymin, float uxmax, float uymax) {
		int k, compute_bbox, sid; //tid;
		float dx, dy; //x, y; 
		
		if (n >= constants.maxSites()) {
			System.out.println("error too many sites"); 
			return -1; 
		}
		
		compute_bbox = ((uxmin == 0) && (uymin == 0) && (uxmax == 0) && (uymax == 0)) ? 1 : 0; 
		
		Vert[] USites = new Vert[n];
		Vert[] GLSites = new Vert[n]; 
		//NVert[] chverts = new NVert[constants.maxVerts()];
		for (k = 0; k < n; k++) {
			Vert v = new Vert();
			v.setX(usites[k][0]);
			//System.out.println("getting usites: " + usites[k][0] + " " + usites[k][1]); 
			v.setY(usites[k][1]);
			USites[k] = v; 
			//System.out.println("set v: " + v.X()  + " " + v.Y()); 
			Vert r = new Vert();
			r.setX(usites[k][0]);
			r.setY(usites[k][1]);
			GLSites[k] = r; 
			
			//System.out.println("setting glsite: " + r.X() + " " + r.Y()); 
			
			//here, GLsites is the same as USites, which is just data read directly from the image 
			constants.chverts()[k] = new NVert(); 
			constants.chverts()[k].setNbr1(-1); 
			constants.chverts()[k].setNbr2(-1); //what is the purpose of this? 
			constants.chverts()[k].setOnHull(0); 
		}
		
		constants.setUSites(USites);
		constants.setGLsites(GLSites); 
		
		constants.setNSites(n); 
		if (constants.nsites() <=2) {
			for(k = 0; k < constants.nsites(); k++) {
				constants.uchverts()[k].setVPid(k);
				constants.uchverts()[k].setNbr1((k-1+constants.nsites()) % constants.nsites()); //what is the purpose of this? 
				constants.uchverts()[k].setNbr2((k+1+constants.nsites()) % constants.nsites()); 
			}
			return -3; 	
		}
		
		voronoi.sortGLSites(); //sort by data points and remove coincident sites 
		
		bBoxInit(); //set some bounds 
		
		/* now, if user punted on bbox calculation, OR if user bbox does not truly enclose
	       user data, we use our bbox (computed in initbbox).  otherwise we take the user's. */
		
		if (!(compute_bbox == 1 || uxmin > constants.vxmin() || uymin > constants.vymin() || uxmax < constants.vxmax() || uymax < constants.vymax())) {
			//if we did not want to compute bbox or what the user gave (?) is larger than what was computed? 
			pxmin = uxmin;
			pymin = uymin;
			pxmax = uxmax;
			pymax = uymax; 
		}
		
		xminymax[0] = pxmin;
		xminymin[0] = pxmin;
		xminymin[1] = pymin;
		xmaxymin[1] = pymin;
		
		xmaxymax[0] = pxmax;
		xmaxymin[0] = pxmax;
		xmaxymax[1] = pymax;
		xminymax[1] = pymax; 
		
		corner[0] = new VertTheta(xminymin[0], xminymin[1]);
		corner[1] = new VertTheta(xmaxymin[0], xmaxymin[1]);
		corner[2] = new VertTheta(xmaxymax[0], xmaxymax[1]);
		corner[3] = new VertTheta(xminymax[0], xminymax[1]); 
		
		/* now: set the floating point tolerance values P*** to be 1 or 2 significant bits inside the p*** */
	    /* be careful to use RELATIVE error; that is, to scale the tolerance to the bbox values themselves */
	    /* now, if some user puts points way out in left field, our technique handles the ranges correctly */
		
		dx = (pxmax - pxmin) * (1/4096);
		dy = (pymax - pymin) * (1/4096); 
		
		//cut a little off the edges? 
		
		Pxmin = pxmin + dx;
		Pxmax = pxmax - dx;
		Pymin = pymin + dy;
		Pymax = pymax - dy;    
		
		/* compute inverse of external->internal sid mapping */
	    /* given an internal site number, returns user index */
		
		for (sid = 0; sid < constants.nsites(); sid++) {
			inverse[sid] = constants.getGLSites()[sid].vpid(); //when did vpid get set and what is it? 
		}
		
		
		//creating an inverse of some sort...? 
		/*for (sid = 0; sid < constants.nsites(); sid ++) {
			if (constants.sites()[sid].getCoord().X() != constants.USites()[inverse[sid]].X()) 
				System.out.println("inverse error");
			if (constants.sites()[sid].getCoord().Y() != constants.USites()[inverse[sid]].Y()) 
				System.out.println("inverse error"); 
		}
		*/
		
		vdInit(); //initialize counting numbers 
		
		/* run the voronoi code, no triangulate */
		//constants.setTriangulate(0); 
		//voronoi.voronoi();
		
		//bBoxInit();
		//constants.setTriangulate(1); 
		//System.out.println("triangulate: " + constants.triangulate()); 
		voronoi.voronoi(); 
		
		for (sid = 0; sid < constants.nsites(); sid++) {
			constants.sites()[sid].setSiteNbr(constants.getGLSites()[sid].vpid());
			constants.sites()[sid].getCoord().setX(constants.getGLSites()[sid].X());
			constants.sites()[sid].getCoord().setY(constants.getGLSites()[sid].Y()); 
		}
		
		return 0; 
		
	}
	
	private class vtcomp implements Comparator<VertTheta> {

		@Override
		public int compare(VertTheta v1, VertTheta v2) {
			if (v1.theta() < v2.theta()) {
				return -1;
			}
			else if (v1.theta() > v2.theta()) {
				return 1;
			}
			else return 0; 
		}
		 
	}
	
	/*
    find_vregion(sid, plen, pverts)
	given a site id 'sid' from 0..nsites-1 inclusive, 
	returns the voronoi polygon associated with that site
	in the array 'pverts', and its length on call stack.

	the vertices are returned in counterclockwise order.

    returns:
	-1 if error condition
	plen > 2 [i.e., the # of verts in the voronoi polygon] otherwise
*/
	
	public int findVRegion (int vsid, float[][] pverts) {
		
		//given a site id, returns the voronoi polygon associated with that site, with the vertices returned in counterclockwise order 
		
		int sid, b, k, vnum, bnd1, bnd2, bdiff, sleft, lag, lead, p1a, p1b, p2a, p2b;
		float x, y, x1, y1, x2, y2, theta1, theta2, lasttheta, dtheta, h; 
		
		//ifdef statement here - but the identifier is never actually defined in the code, so perhaps it is not needed? 
		
		if (vsid < 0 || vsid >= constants.nsites()){
			System.out.println("error site bounds");
			return -1; 
		}
		
		for (sid = 0; sid < constants.nsites(); sid++) {
			if (constants.getGLSites()[sid].vpid() == vsid)
				break;
		}
		
		if (sid == constants.nsites()) {
			System.out.println("can't find requested site");
			return -1; 
		}
		
		for (k = 0; k < 4; k++) {
			corner[k].setTheta((float) Math.atan2((double) (corner[k].Y() - constants.getGLSites()[sid].Y()), (double) (corner[k].X() - constants.getGLSites()[sid].X()))); 
		}
		
		vtnum = 0;
		for (k = 0; k < numvedges; k++) {
			if (constants.vedges()[k].nbr1() == sid) {
				slist[vtnum].setE1(k);
				slist[vtnum].setE2(k); 
				slist[vtnum].setTheta((float) Math.atan2((double) (constants.vedges()[k].y1() - constants.getGLSites()[sid].Y()), (double) (constants.vedges()[k].x1() - constants.getGLSites()[sid].X()))); 
				slist[vtnum].setX(constants.vedges()[k].x1());
				slist[vtnum].setY(constants.vedges()[k].y1()); 
				
				vtnum ++;
				
				slist[vtnum].setE1(k);
				slist[vtnum].setE2(k); 
				slist[vtnum].setTheta((float) Math.atan2((double) (constants.vedges()[k].y2() - constants.getGLSites()[sid].Y()), (double) (constants.vedges()[k].x2() - constants.getGLSites()[sid].X()))); 
				slist[vtnum].setX(constants.vedges()[k].x2());
				slist[vtnum].setY(constants.vedges()[k].y2());
				
			}
		}
		
		vtcomp comp = new vtcomp(); 
		
		Arrays.sort(slist, comp);  
		
		lag = 0;
		lead = 0;
		
		vtlist[lag] = slist[lead]; 
		lasttheta = -10; 
		
		while (lead < vtnum) {
			if (Math.abs((slist[lead].theta()- lasttheta)) > (10^-4)) {
				lasttheta = slist[lead].theta();
				vtlist[lag] = slist[lead];
				lag++;
				lead++; 
			}
			else {
				vtlist[lag-1].setE2(slist[lead].e1());
				lead++; 
			}
		}
		
		vtnum = lag; 
		
		vnum = 0; 
		for(k = 0; k < vtnum; k++) {
			if (Math.abs(vtlist[(k + vtnum - 1) % vtnum].theta() - vtlist[k].theta()) < (10^-4)){
				System.out.println("region identical"); 
				return -1; 
			}
			
			x1 = vtlist[(k + vtnum - 1) % vtnum].X();
			y1 = vtlist[(k + vtnum - 1) % vtnum].Y();
			p1a = vtlist[(k + vtnum - 1) % vtnum].e1();
			p1b = vtlist[(k + vtnum - 1) % vtnum].e2(); 
			x2 = vtlist[k].X();
			y2 = vtlist[k].Y();
			p2a = vtlist[k].e1(); 
			p2b = vtlist[k].e2(); 
			
			if (((p1a == p2a) || (p1a == p2b) || (p1b == p2b)) && (vtnum > 2)) {
				sid = PUTV(vtlist[k], sid, pverts); 
			}
			else {
			bnd1 = NBOUND(x1, y1); 
			bnd2 = NBOUND(x2, y2);
			
			if (bnd1 >= 0 && bnd2 >=0) 
				bdiff = ((bnd2 - bnd1) + 4) % 4;
			else 
				bdiff = 0; 
			
			if (bdiff == 0) {
				sid = PUTV(vtlist[k], sid, pverts);
			}
			else {
			if (vtnum == 2) {
				theta1 = vtlist[0].theta();
				theta2 = vtlist[1].theta();
				dtheta = theta2 - theta1; 
				
				for(b = 0; b < 4; b++){
					if ( /*dtheta >= M_PI*/ (corner[b].theta() > theta1) && (corner[b].theta() < theta2)) { //theta is the angle measure? 
						vtlist[vtnum++] = corner[b]; 
					}
					else if ( /*dtheta < M_PI*/ ((corner[b].theta() < theta1) || corner[b].theta() > theta2)) {
						vtlist[vtnum++] = corner[b]; 
					}
				}
				
				Arrays.sort(vtlist, comp); 
				
				for(b = 0; b < vtnum; b++) {
					sid = PUTV(vtlist[b], sid, pverts); 
				}
				
				break; 
				
			}
			
			for (b = 0; b < bdiff; b++) {
				sid = PUTV(corner[(bnd1 + b) % 4], sid, pverts);
			}
			
			sid = PUTV(vtlist[k], sid, pverts); 
			}
		}
		}
		
		return vnum; 
		
	}
	
	private int NBOUND(float x, float y) {
		return (x >= Pxmax ? constants.rbnd() : (x <= Pxmin ? constants.lbnd() : (y >= Pymax ? constants.tbnd() : (y <= Pymin ? constants.bbnd() : -1))));
	}
	
	private int PUTV(VertTheta v, int vid, float[][] pverts) {//////////////////
		pverts[vid][0] = v.X();
		pverts[vid][1] = v.Y(); 
		return (vid + 1); 
	}
	
	public int findDTriangles () {///////////////////
		if (numtris <= 0){
			constants.setdtris(null); 
			return -1;
		}
		else {
			Tri[] dtris = new Tri[constants.maxTris()];
			int i = 0; 
			for (Tri t: constants.tris()) {
				dtris[i] = t; 
				i++; 
			}
			constants.setdtris(dtris);  
			return numtris; 
		}
	}
	
	private boolean EMATCH(Vert Ap, Vert Aq, Vert Bp, Vert Bq) {
		return (Ap == Bp && Aq == Bq); 
	}
	
	public int findDTAdjacencies (IntArraySetter dtadjset) {
		int nadj = 0, j, k, nematched;
		Tri trij, trik; 
		
		if (numtris <= 0) {
			dtadjset.set(null); 
			return -1;
		}
		else {
			for (j = 0; j < numtris; j++) {
				trij = constants.tris()[j];
				nematched = 0; 
				for (k = j + 1; k < numtris; k++) {
					trik = constants.tris()[k]; 
					if (EMATCH(trij.v1(), trij.v2(), trik.v3(), trik.v2()) || EMATCH(trij.v1(), trij.v2(), trik.v2(), trik.v1()) || EMATCH(trij.v1(), trij.v2(), trik.v1(), trik.v3())) {
						adjlist[nadj][0] = j;
						adjlist[nadj++][1] = k;
						nematched++; 
					}
					else if (EMATCH(trij.v2(), trij.v3(), trik.v3(), trik.v2()) || EMATCH(trij.v1(), trij.v3(), trij.v2(), trik.v1()) || EMATCH(trij.v2(), trij.v3(), trik.v1(), trik.v3())) {
						adjlist[nadj][0] = j;
						adjlist[nadj++][1] = k; //what does the ++ denote? 
						nematched++;
					}
					else if (EMATCH(trij.v3(), trij.v1(), trik.v3(), trik.v2()) || EMATCH(trij.v3(), trij.v1(), trik.v2(), trik.v1()) || EMATCH(trij.v3(), trij.v1(), trik.v1(), trik.v3())) {
						adjlist[nadj][0] = j;
						adjlist[nadj++][1] = k;
						nematched++; 
					}
					if (nematched == 3)
						break; 
				}
			}
			dtadjset.set(adjlist); //this should maybe be the other way around? 
			return nadj; 
		}
		
	}
	
	public int fident(float a, float b) {
		float sum;
		
		sum = Math.abs(a) + Math.abs(b); 
		if (sum == 0) 
			return 1; 
		else {
			sum = (a-b)/sum;
			return ((sum > (2*(10^-7)) ? 1 : 0)); 
		}
			 
	}
	
	public int xycollinear (float ax, float ay, float bx, float by, float cx, float cy) {
		float AX, AY, BX, BY; 
		
		AX = bx - ax;
		AY = by - ay;
		BX = cx - bx;
		BY = cy - by;
		
		return fident(AX*BY, BX*AY); 
		
	}
	
	public int findConvexHull(ArraySetter chvertset) {  //type of chvertices??
		int k, nchverts, nbr1, nbr2; 
		NVert v1 = null, v2, T;
		int start, lag, lead;
		
		if (constants.nsites() <= 2) {
			chvertset.set(constants.uchverts()); 
			return constants.nsites(); 
		}
		
		for(k = 0; k < constants.nsites(); k++) {
			v1 = constants.chverts()[k];
			if (v1.onhull() == 1 && v1.nbr1() != -1 && v1.nbr2() != -1) {
				break; 
			}
		}
			if (k == constants.nsites()) {
				System.out.println("no hull edge found");
				chvertset.set(null);  
				return -1; 
			}
			
			start = v1.vpid();
			lag = v1.vpid();
			lead = v1.nbr2();
			
			k = 0;
			
			while(lag != start) {
				if (k>0) 
					constants.uchverts()[k].setNbr1(constants.uchverts()[k-1].vpid()); 
				constants.uchverts()[k].setVPid(inverse[lag]); 
				constants.uchverts()[k++].setNbr2(inverse[lead]); 
				
				if (lag == constants.chverts()[lead].nbr1()) {
					lag = lead;
					lead = constants.chverts()[lead].nbr2(); 
				}
				else if (lag == constants.chverts()[lead].nbr2()) {
					lag = lead;
					lead = constants.chverts()[lead].nbr1(); 
				}
				else {
					System.out.println("inconsistent convex hull");
					return 0;
				}
				
			}
			
			constants.uchverts()[0].setNbr1(constants.uchverts()[k-1].vpid()); 
			nchverts = k; 
		
		lag = 0; 
		for (k = 0; k < nchverts; k++) {
			if ((fident(UX(k), UX((k + 1) % nchverts)) != 1 && fident(UY(k), UY((k + 1) % nchverts)) == 1)) {
				constants.uchverts()[lag++] = constants.uchverts()[k]; 
			}
			if (lag != nchverts) {
				System.out.println("excised coincident convex hull vertices");
				k = lag;
				nchverts = lag;
			}
		}
		
		if (nchverts > 2) {
			lag = 0; 
			for (k = 0; k < nchverts; k++) {
				if (xycollinear(UX(k), UY(k), UX((k + 1) % nchverts), UY((k + 1) % nchverts), UX((k + 2) % nchverts), UY((k + 2) % nchverts)) == 0) {
					constants.uchverts()[lag++] = constants.uchverts()[k]; 
				}
			}
			if (lag != nchverts) {
				System.out.println ("excised dgenerate convex hull edges");
				k = lag;
				nchverts = lag; 
			}
		}
			
		chvertset.set(constants.uchverts()); 
		return k; 
		
	}
	
	public float UX(int k) {
		return constants.USites()[constants.uchverts()[k].vpid()].X(); 
	}
	
	public float UY(int k) {
		return constants.USites()[constants.uchverts()[k].vpid()].Y(); 
	}

}
