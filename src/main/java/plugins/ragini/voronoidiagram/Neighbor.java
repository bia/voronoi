package plugins.ragini.voronoidiagram;

import java.util.ArrayList;

public class Neighbor extends Site implements PointType {
	
	private double dist; 
	private Site site; 
	private ArrayList<Neighbor> samedist; 
	
	public Neighbor() {
		super(); 
		samedist = new ArrayList<Neighbor>(); 
	}
	
	public Site getSite() {
		return site; 
	}
	
	public void addNeighbor(Neighbor n) {
		neighbors.add(n); 
	}
	
	public void same(Neighbor n) {
		samedist.add(n);
	}
	
	public ArrayList<Neighbor> samedistpoints() {
		return samedist; 
	}
	
}
