package plugins.ragini.voronoidiagram;

import icy.canvas.IcyCanvas;
import icy.image.IcyBufferedImage;
import icy.painter.AbstractPainter;
import icy.painter.Painter;
import icy.painter.PainterListener;
import icy.roi.BooleanMask2D;
import icy.roi.ROI2DEllipse;
import icy.roi.ROI2DLine;
import icy.roi.ROI2DPoint;
import icy.roi.ROI2DPolygon;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzButton;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVar;
import plugins.adufour.ezplug.EzVarEnum;
import plugins.adufour.ezplug.EzVarFile;
import plugins.adufour.ezplug.EzVarInteger;
import plugins.adufour.ezplug.EzVarListener;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.vars.lang.VarSequence;

public class VoronoiDiagram extends EzPlug implements Block {
	
	private enum MethodType
	{
		DELAUNAY("Calculate Delaunay Triangulation"), VORONOI("Calculate Voronoi Diagram"), RNG("Calculate Relative Neighborhood Graph");
		
		private final String	name;
		
		private MethodType(String string)
		{
			this.name = string;
		}
		
		public String toString()
		{
			return this.name;
		}
	}
	
	private Constants constants; 
	private EzVarFile input = new EzVarFile("Input", "/Users"); 
	private EzVarSequence img = new EzVarSequence("Image"); 
	private EzVarInteger numPoints = new EzVarInteger("Display Closest Points", 2, 1, 7, 1); 
	private EzButton morphbutton = new EzButton("Calculate Morphological Operators", new buttonlist()); 
	
	private class buttonlist implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			a = new Alpha_Morph(constants.sites(), constants.vxmin(), constants.vymin(), constants.vxmax(), constants.vymax());
		}
		
	}
	
	float[][] points; 
	VarSequence out = new VarSequence("Out", null);
	Point2D[] dpoints; 
	private EzVarEnum<MethodType> method = new EzVarEnum<MethodType>("Method:", MethodType.values()); ;

	private ArrayList<ROI2DSite> imgpoints = new ArrayList<ROI2DSite>(); 
	private ArrayList<ROI2DSite> allpoints = new ArrayList<ROI2DSite>(); 
	private ArrayList<ROI2DLine> alltris = new ArrayList<ROI2DLine>(); 
	private ArrayList<ROI2DLine> imgedges = new ArrayList<ROI2DLine>(); 
	private Sequence seqOut; 
	private Alpha_Morph a; 
	
	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void execute() {
		 String data = input.getValue().getPath(); 
		 MethodType _method = method.getValue();
		 Sequence seqIn = img.getValue(); 
		 final int numpoints = numPoints.getValue(); 
		 IcyBufferedImage imgIn = null; 
		 if (seqIn != null) {
			 imgIn = seqIn.getImage(0, 0, 0); }
		 System.out.println(_method.toString()); 
		 points = new float[5000][2];
		 dpoints = new Point2D[5000]; 
		 double xmin = 0;
		 double xmax = 0;
		 double ymin = 0;
		 double ymax = 0; 
		 int x = 0;
		 try {
			Workbook wb = Workbook.getWorkbook(new File(data)); 
			Sheet s = wb.getSheet(0);
			NumberCell c = (NumberCell) s.getCell(0,0);  
			while (x < s.getRows()) {
				c = (NumberCell) s.getCell(0,x);
				points[x][0] = (float) c.getValue();
				c = (NumberCell) s.getCell(1,x);
				points[x][1] = (float) c.getValue();
				dpoints[x] = new Point2D.Double(points[x][0], points[x][1]);
				//System.out.println("x: " + points[x][0] + "y: " + points[x][1]); 
				if (x == 0 ){
					xmin = points[x][0]; 
					xmax = points[x][0]; 
					ymin = points[x][1];
					ymax = points[x][1]; 
				}
				else {
					if (points[x][0] > xmax)
						xmax = points[x][0];
					if (points[x][0] < xmin)
						xmin = points[x][0];
					if (points[x][1] > ymax) 
						ymax = points[x][1];
					if (points[x][1] < ymin) 
						ymin = points[x][1]; 
				}
				x++; 
			}
			//System.out.println("done reading data points"); 
		} catch (BiffException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		 
		constants= new Constants(); 
	if(_method.toString() == "Calculate Delaunay Triangulation") {
		constants.setTriangulate(1);
	}
	else if(_method.toString() == "Calculate Relative Neighborhood Graph") {
		constants.setTriangulate(1);
		constants.setrng(1); 
	}
			VRegion vregion = new VRegion(constants, this);
			EdgeList EL = new EdgeList(constants, vregion); 
			Geometry events = new Geometry(constants); 
			Vonoroi voronoi = new Vonoroi(EL, constants, vregion, events); 
			vregion.setVoronoi(voronoi); 
			seqOut = new Sequence();
			if (seqIn == null) {
				int width = (int) Math.ceil(xmax - xmin + 1); 
				int height = (int) Math.ceil(ymax - ymin + 1);
				IcyBufferedImage imgOut = new IcyBufferedImage(width,height, 1, DataType.UBYTE ); 
				seqOut.addImage(imgOut); 
				double[] _out = Array1DUtil.arrayToDoubleArray(imgOut.getDataXY(0), imgOut.isSignedDataType());
				for (int p = 0; p < width; p++) {
					for (int q = 0; q < height; q++) {
						_out[p + q*width] = 255; 
					}
				}
				imgOut.setDataXY(0,Array1DUtil.doubleArrayToArray(_out, imgOut.getDataXY(0))); 
				seqOut.setImage(0, 0, imgOut);}
				else {seqOut.setImage(0, 0, imgIn);seqOut.dataChanged(); }
for(int k= 0; k < x; k++) {
				
				Point2D dp = new Point2D.Double(points[k][0], points[k][1]); 
				ROI2DSite point = new ROI2DSite(dp); 
				allpoints.add(point);
				point.setEditable(false);  
				point.setColor(Color.ORANGE); 
				point.attachTo(seqOut); 
				
			} 
			vregion.loadVSites(x, points, 0, 0, 0, 0);
			
			
			//System.out.println("done running"); 
			
			
			
			int i = 0; 
			Point2D p1; 
			while((p1 = constants.lines()[i][0]) != null) {
				Point2D p2 = constants.lines()[i][1];  
				ROI2DLine line = new ROI2DLine(p1, p2); 
				line.setEditable(false); 
				line.setColor(Color.CYAN);
				line.attachTo(seqIn); 
				i++; 
			}
			
			i= 0;  
			Vert v; 
			while ((v = constants.verts()[i]) != null) {
				Point2D p = new Point2D.Double(v.X(), v.Y()); 
				ROI2DPoint point = new ROI2DPoint(p); 
				point.setEditable(false); 
				point.setColor(Color.RED); 
				point.attachTo(seqOut);
				i++; 
			}
			
			long dist[]=new long[1000]; //VMY
			
			if (constants.triangulate() == 1 && constants.rng() != 1) {
				alltris.clear(); 
				i = 0;
				Tri t;
				while ((t = constants.tris()[i]) != null) {
					Point2D t1 = new Point2D.Double(t.v1().X(), t.v1().Y());
					Point2D t2 = new Point2D.Double(t.v2().X(), t.v2().Y());
					Point2D t3 = new Point2D.Double(t.v3().X(), t.v3().Y());

					ROI2DLine l1 = new ROI2DLine(t1, t2);
					ROI2DLine l2 = new ROI2DLine(t2, t3);
					ROI2DLine l3 = new ROI2DLine(t1, t3);

					l1.setEditable(false);
					l1.setColor(Color.GREEN);
					l1.attachTo(seqIn);
					l2.setEditable(false);
					l2.setColor(Color.GREEN);
					l2.attachTo(seqIn);
					l3.setEditable(false);
					l3.setColor(Color.GREEN);
					l3.attachTo(seqIn);

					double d1 = t1.distance(t2);
					int d = (int) (Math.round(Math.floor(d1)));
				//	System.out.println(" "+d1+" d:"+d+" t1x:"+t1.getX()+" t1y:"+t1.getY());
				//	dist[d]++;
					double d2 = t1.distance(t3);
					 d = (int) (Math.round(Math.floor(d2))); // d1 ??
					//	System.out.println(" "+d2+" d:"+d +" t2x:"+t2.getX()+" t2y:"+t2.getY());
			//		dist[d]++;
					double d3 = t2.distance(t3);
					 d = (int) (Math.round(Math.floor(d3))); // d3?
					//	System.out.println(" "+d3+" d:"+d+" t3x:"+t3.getX()+" t3y:"+t3.getY());
			//		dist[d]++;
						
						//MYV
						Polygon polygon = new Polygon();
						polygon.addPoint( 
								(int) t1.getX(),
								(int) t1.getY()
						);
						polygon.addPoint( 
								(int) t2.getX(),
								(int) t2.getY()
						);
						polygon.addPoint( 
								(int) t3.getX(),
								(int) t3.getY()
						);
						
						
						ROI2DPolygon poly = new ROI2DPolygon();
						poly.setPolygon(polygon);
						
						BooleanMask2D roiMask = poly.getAsBooleanMask();
						
						int minx = Math.max(0, roiMask.bounds.x);
						int maxx = Math.min(4000, roiMask.bounds.x+roiMask.bounds.width);
						int miny = Math.max(0, roiMask.bounds.y);
						int maxy = Math.min(4000, roiMask.bounds.y+roiMask.bounds.height);
						double intensitySpot=0.;
						int areaC=0;//size Cell
						for (int ix = minx; ix < maxx;ix++)
							for (int y = miny; y < maxy;y++){
								if(roiMask.contains(ix, y)) {
									areaC++;
								}			
								
							}
						System.out.println(" "+areaC); // triangle Size
						
						
						
						
					alltris.add(l1);
					alltris.add(l2);
					alltris.add(l3);
					
					/*Circ c = makeCirc(t1, t2, t3);
					ROI2DEllipse circ = new ROI2DEllipse(c.rectangle()); 
					circ.setEditable(false);
					circ.setColor(Color.ORANGE);
					circ.attachTo(seqOut);*/ 

					//System.out.println("tri: " + "(" + t1.getX() + "," + t1.getY() + ") (" + t2.getX() + "," + t2.getY() + ") (" + t3.getX() + "," + t3.getY() + ")"); 
					/*ArrayList<Point2D> plist = new ArrayList<Point2D>();
					plist.add(t1);
					plist.add(t2);
					plist.add(t3); 
					ROI2DPolygon tri = new ROI2DPolygon(); 
					alltris.add(tri); 
					tri.setPoints(plist); 
					tri.setColor(Color.GREEN);
					tri.setEditable(false); 
					tri.attachTo(seqOut); */
					/*Circ c = makeCirc(t1, t2, t3); 
					Point2D ref; 
					if (t1.getX() > t2.getX()) {
						ref = t1; 
					}
					else ref = t2; 
					if (t3.getX() > ref.getX()) ref = t3; 
					Ellipse2D e = new Ellipse2D.Double(ref.getX(), ref.getY(), c.r()*2, c.r()*2); 
					ROI2DEllipse re = new ROI2DEllipse(e.getFrame()); 
					re.setColor(Color.BLUE); 
					re.setEditable(false); 
					re.attachTo(seqOut); */
					i++;
				}
			}
			
			else if (constants.rng() == 1) {
				i = 0; 
				for (EdgeRNG e: constants.relativeneighbors()) {
					ROI2DEdge edge = new ROI2DEdge(e); 
					edge.setEditable(false);
					edge.setColor(Color.YELLOW);
					edge.attachTo(seqOut); 
				}
				
			}
			allpoints.clear(); 
			for(Neighbor site : constants.sites()) {
				
				Point2D dp = new Point2D.Double(site.getCoord().X(), site.getCoord().Y()); 
				ROI2DSite point = new ROI2DSite(dp);
				point.setSite(site); 
				site.setPoint(point); 
				allpoints.add(point);
				point.setEditable(false);  
				point.setColor(Color.ORANGE); 
				point.attachTo(seqOut); 
			}
			
			
			
			if (_method.toString() == "Calculate Delaunay Triangulation") {
				Painter painter = new AbstractPainter() {
				@Override
				public void mousePressed(MouseEvent e, Point2D imagePoint,
						IcyCanvas canvas) {
					//super.mouseClick(e, imagePoint, canvas);
					//System.out.println("mouse pressed"); 
					for(ROI2DSite p : allpoints) {
						if ((p.getPoint().getX() <= imagePoint.getX() + 3 && p.getPoint().getX() >= imagePoint.getX() -3) && ((p.getPoint().getY() <= imagePoint.getY() + 3) && p.getPoint().getY() >= imagePoint.getY() -3)) {
							//System.out.println("on site"); 
							p.setColor(Color.RED);  
							Neighbor sp = (Neighbor) p.getSite(); 
							int size = sp.neighbors().size(); 
							//System.out.println(size); 
							int count = 0; 
							Neighbor cur = null; 
							ArrayList<Neighbor> temp = new ArrayList<Neighbor>(); 
							//System.out.println("size: " + size); 
							//System.out.println("numpoints: " + numpoints); 
							while (count < numpoints && count < size) {
								cur = sp.neighbors().remove(); 
								temp.add(cur); 
								ROI2DSite curpoint = cur.getPoint(); 
								curpoint.setColor(Color.YELLOW); 
								imgpoints.add(curpoint); 
								count++; 
								if (count == 1) {
									for (Neighbor s : cur.samedistpoints()) {
										if (sp.neighbors().remove(s)) {
											s.getPoint().setColor(Color.YELLOW);
											imgpoints.add(s.getPoint());
											temp.add(s);
											count++;
										}
									}
								}
							}
							for (Neighbor t : temp) {
								sp.addNeighbor(t);  
							}
							temp.clear(); 
							boolean incident = false; 
							boolean keep = false; 
							for(ROI2DLine edge: alltris) {
								Point2D tp = edge.getLine().getP1();
									if (tp.getX() == p.getPoint().getX() && tp.getY() == p.getPoint().getY()){
										incident = true; //System.out.println("incident"); 
									}
									
								tp = edge.getLine().getP2();
									if (tp.getX() == p.getPoint().getX() && tp.getY() == p.getPoint().getY()){
										incident = true; //System.out.println("incident"); 
									}
								
								for (ROI2DSite n: imgpoints) {
									tp = edge.getLine().getP1(); 
										
											if (tp.getX() == n.getPoint().getX() && tp.getY() == n.getPoint().getY()) {
												if (incident == true) {keep = true; }
											}
										
									 
										tp = edge.getLine().getP2(); 
						
											if (tp.getX() == n.getPoint().getX() && tp.getY() == n.getPoint().getY()) {
												if (incident == true) {keep = true;}
											}
										
										
									
								}
								for (Neighbor t : temp) {
									sp.addNeighbor(t);  
								}
								temp.clear(); 
								//System.out.println("keep: " + keep); 
								if (keep == false) {
									edge.detachFrom(seqOut); 
									imgedges.add(edge);
								}
								keep = false; 
								incident = false; 
							}

							imgpoints.add(p);
						}
					}
					
					
				}
				
				@Override
					public void mouseReleased(MouseEvent e, Point2D imagePoint,
							IcyCanvas canvas) {
							for(ROI2DSite rp : imgpoints) {
								rp.setColor(Color.BLACK); 
							}
							imgpoints.clear(); 
							for(ROI2DLine edge: imgedges) {
								edge.attachTo(seqOut); 
							}
							imgedges.clear(); 
						
	
					}
				
				}; 
				
				
				seqOut.addPainter(painter); 
				seqOut.painterChanged(painter); 
			}
			
			if (isHeadLess())  {
				out.setValue(seqOut);
			}
			
			else {addSequence(seqOut);} 
			
	}
	
	private Circ makeCirc(Point2D p1, Point2D p2, Point2D p3) {
		double x = (p3.getX()*p3.getX() * (p1.getY() - p2.getY()) 
			    + (p1.getX()*p1.getX() + (p1.getY() - p2.getY())*(p1.getY() - p3.getY())) 
			    * (p2.getY() - p3.getY()) + p2.getX()*p2.getX() * (-p1.getY() + p3.getY())) 
		    / (2 * (p3.getX() * (p1.getY() - p2.getY()) + p1.getX() * (p2.getY() - p3.getY()) + p2.getX() 
			    * (-p1.getY() + p3.getY())));

		double y = (p2.getY() + p3.getY())/2 - (p3.getX() - p2.getX())/(p3.getY() - p2.getY()) 
		    * (x - (p2.getX() + p3.getX())/2);

		Point2D c = new Point2D.Double(x,y); 
		double r = distance(c.getX(), c.getY(), p1.getX(), p1.getY());

		return (new Circ(c, r)); 

	}
	
	private double distance(double ax, double ay, double bx, double by) {
		return (Math.sqrt((bx - ax)*(bx - ax) + (by - ay)*(by - ay)));
	}

	@Override
	protected void initialize() {
		
		super.addEzComponent(input);
		super.addEzComponent(img);
		super.addEzComponent(method);
		super.addEzComponent(numPoints); 
		super.addEzComponent(morphbutton); 
		method.addVarChangeListener(new EzVarListener<MethodType>()
				{
					@Override
					public void variableChanged(EzVar<MethodType> source, MethodType newValue)
					{
						updateDefaultParams();
					}
					
				});
		 
		
	}

	@Override
	public void declareInput(VarList inputMap) {
		inputMap.add(input.getVariable()); 
		inputMap.add(method.getVariable()); 
		inputMap.add(img.getVariable()); 
		inputMap.add(numPoints.getVariable()); 
	}

	@Override
	public void declareOutput(VarList outputMap) {
		outputMap.add(out);
	}
	
	private void updateDefaultParams()
	{
		if (true)
		{
			/*if (input.getValue(false) != null)
		{*/
			MethodType _method = method.getValue();
			switch(_method)
			{
			case DELAUNAY:
				
				break;
			case VORONOI:
				
				break;						
			default:
				break;
			}

			//}
	}
	}

}
